<?php

use yii\db\Schema;

class m140921_125851_news extends \yii\db\Migration
{
    public function up()
    {
        // Настройки MySql таблицы
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        // Таблица постов
        $this->createTable('{{%news}}', [
            'news_id' => Schema::TYPE_PK,
            'author_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'title' => Schema::TYPE_STRING . '(100) NOT NULL',
            'slug' => Schema::TYPE_STRING . '(100) NOT NULL',
            'snippet' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
            'content' => 'longtext NOT NULL',
            'image' => Schema::TYPE_INTEGER . ' NULL DEFAULT NULL',
            'status' => 'tinyint(2) NOT NULL DEFAULT 1',
            'views' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'create_time' => Schema::TYPE_DATETIME . ' NOT NULL',
            'update_time' => Schema::TYPE_DATETIME . ' NOT NULL',
        ], $tableOptions);

        // Индексы
        $this->createIndex('slug', '{{%news}}', 'slug');
        $this->createIndex('author_id', '{{%news}}', 'author_id');
        $this->createIndex('status', '{{%news}}', 'status');
        $this->createIndex('views', '{{%news}}', 'views');
        $this->createIndex('create_time', '{{%news}}', 'create_time');
        $this->createIndex('update_time', '{{%news}}', 'update_time');
    }

    public function down()
    {
        $this->dropTable('{{%news}}');
    }
}
