<?php
use yii\db\Schema;
use yii\db\Migration;
use yii\base\Security;

/**
 * Миграция создаёт все таблицы БД модуля [[Users]]
 * Создаются 2 таблицы:
 * - {{%users}} => в которой хранится основная информация о пользователе.
 * - {{%user_email}} => в которой хранятся временные E-mail записи. Таблица используется для смены E-mail адреса.
 */
class m130704_174427_create_users_tbl extends Migration
{
	public function safeUp()
	{
		// Настройки MySql таблицы
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Таблица пользователей
		$this->createTable('{{%users}}', [
			'id' => Schema::TYPE_PK,
			'username' => Schema::TYPE_STRING . '(30) NOT NULL',
			'email' => Schema::TYPE_STRING . '(100) NOT NULL',
			'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
			'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
			'name' => Schema::TYPE_STRING . '(50) NOT NULL',
			'surname' => Schema::TYPE_STRING . '(50) NOT NULL',
			'avatar' => Schema::TYPE_STRING . '(64) NOT NULL',
			'role' => 'tinyint NOT NULL DEFAULT 0',
			'status' => 'tinyint(4) NOT NULL DEFAULT 0',
			'create_time' => Schema::TYPE_DATETIME . ' NOT NULL',
			'update_time' => Schema::TYPE_DATETIME . ' NOT NULL'
		], $tableOptions);

		// Индексы
		$this->createIndex('username', '{{%users}}', 'username', true);
		$this->createIndex('email', '{{%users}}', 'email', true);
		$this->createIndex('role', '{{%users}}', 'role');
		$this->createIndex('status', '{{%users}}', 'status');
		$this->createIndex('create_time', '{{%users}}', 'create_time');

		// Добавляем администратора
		$this->execute($this->getSql());
	}

	public function safeDown()
	{
		$this->dropTable('{{%users}}');
	}

	private function getSql()
	{
		$time = date("Y-m-d H:i:s");
		$password_hash =  Yii::$app->getSecurity()->generatePasswordHash('admin12345');
		$auth_key = Security::generateRandomKey();
		return "INSERT INTO {{%users}} (
		    `username`, `email`, `name`, `surname`, `avatar`, `password_hash`, `auth_key`, `role`, `status`, `create_time`, `update_time`
		) VALUES (
		    'admin', 'admin@demo.com', 'Администрация', 'Сайта', '', '$password_hash', '$auth_key', 60, 1, '$time', '$time'
		)";
	}
}