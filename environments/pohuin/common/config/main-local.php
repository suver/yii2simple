<?php

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=pohuin',
            'username' => 'pohuin',
            'password' => 'sdjf24pndlDhf73Sd',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'suver\mailer\Mailer',
            'user'  => [
                'class'  => 'common\modules\user\models\User',
                'fields' => [
                    'id'    =>'id',
                    'email' => 'email',
                    'phone' => 'phone',
                    'fullname' => 'fullname',
                ],
            ],
            'methods'  => [
                'default' => [
                    'method' => 'suver\mailer\method\SwiftMailer',
                    'sender' => 'noreply@pohu.in',
                    'viewPath' => '@common/mail',
                    'layout' => 'html',
                    'transport' => [
                        'class' => 'Swift_SmtpTransport',
                        'host'  => 'smtp.yandex.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                        'username' => 'mail@farpse.com',
                        'password' => 'gpejjli4',
                        'port'  => '465', // Port 25 is a very common port too
                        'encryption' => 'ssl', // It is often used, check your provider or mail server specs

                    ],
                ],
                'smpt' => [
                    'method' => 'suver\mailer\method\SwiftMailer',
                    'sender' => 'mail@farpse.com',
                    'transport' => [
                        'class' => 'Swift_SmtpTransport',
                        'host'  => 'smtp.yandex.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                        'username' => 'noreply@pohu.in',
                        'password' => 'gpejjli4',
                        'port'  => '465', // Port 25 is a very common port too
                        'encryption' => 'ssl', // It is often used, check your provider or mail server specs

                    ],
                ],
                'mail' => [
                    'method' => 'suver\mailer\method\SwiftMailer',
                    'sender' => 'noreply@pohu.in',
                    'transport' => [
                        'class' => 'Swift_MailTransport',
                    ],
                ],
                'sendmail' => [
                    'method' => 'suver\mailer\method\SwiftMailer',
                    'sender' => 'noreply@pohu.in',
                    'transport' => [
                        'class' => 'Swift_SendmailTransport',
                        'command' => '/usr/sbin/exim -bs',
                    ],
                ],
                'sms' => [
                    'class' => 'suver\mailer\method\Sms',
                    'useFileTransport' => false,
                    'transport' => [

                    ],
                ],
            ],
        ],
    ],
];
