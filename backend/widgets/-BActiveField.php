<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\widgets;

use Yii;
use yii\base\Component;
use yii\base\ErrorHandler;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\Model;
use yii\web\JsExpression;
use yii\widgets\ActiveField;

/**
 * ActiveField represents a form input field within an [[ActiveForm]].
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BActiveField extends ActiveField
{

    public $template = "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>";

    /**
     * Renders the whole field.
     * This method will generate the label, error tag, input tag and hint tag (if any), and
     * assemble them into HTML according to [[template]].
     * @param string|callable $content the content within the field container.
     * If null (not set), the default methods will be called to generate the label, error tag and input tag,
     * and use them as the content.
     * If a callable, it will be called to generate the content. The signature of the callable should be:
     *
     * ~~~
     * function ($field) {
     *     return $html;
     * }
     * ~~~
     *
     * @return string the rendering result
     */
    public function render($content = null)
    {
        if ($content === null) {

            Html::addCssClass($this->labelOptions, 'col-sm-2');
            $this->parts['{label}'] = Html::activeLabel($this->model, $this->attribute, $this->labelOptions);

            if (!isset($this->parts['{error}'])) {
                $this->parts['{error}'] = Html::tag('label', Html::error($this->model, $this->attribute), $this->errorOptions);
            }

            $this->parts['{input}'] = Html::activeTextInput($this->model, $this->attribute, $this->inputOptions);

            if (!isset($this->parts['{hint}'])) {
                $this->parts['{hint}'] = '';
            }

            $content = strtr($this->template, $this->parts);
        } elseif (!is_string($content)) {
            $content = call_user_func($content, $this);
        }

        return $this->begin() . "\n" . $content . "\n" . $this->end();
    }

}
