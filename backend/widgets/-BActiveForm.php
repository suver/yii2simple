<?php
/**
 * Created by PhpStorm.
 * User: suver
 * Date: 19.08.14
 * Time: 1:17
 */

namespace backend\widgets;

use yii\widgets\ActiveForm;

use Yii;
use yii\base\Widget;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;


class BActiveForm extends ActiveForm
{


    /**
     * Initializes the widget.
     * This renders the form open tag.
     */
    public function init()
    {
        if (!isset($this->fieldConfig['class'])) {
            $this->fieldConfig['class'] = BActiveField::className();
        }
        parent::init();
    }

    /**
     * Generates a form field.
     * A form field is associated with a model and an attribute. It contains a label, an input and an error message
     * and use them to interact with end users to collect their inputs for the attribute.
     * @param Model $model the data model
     * @param string $attribute the attribute name or expression. See [[Html::getAttributeName()]] for the format
     * about attribute expression.
     * @param array $options the additional configurations for the field object
     * @return ActiveField the created ActiveField object
     * @see fieldConfig
     */
    public function field($model, $attribute, $options = [])
    {
        return Yii::createObject(array_merge($this->fieldConfig, $options, [
            'model' => $model,
            'attribute' => $attribute,
            'form' => $this,
        ]));
    }

}