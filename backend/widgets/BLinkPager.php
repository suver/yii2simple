<?php
namespace backend\widgets;

use yii\widgets\LinkPager;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class BLinkPager extends LinkPager
{

    /**
     * @var array HTML attributes for the pager container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'dataTables_paginate paging_full_numbers'];

    /**
     * @var string the CSS class for the "first" page button.
     */
    public $firstPageCssClass = 'first paginate_button';
    /**
     * @var string the CSS class for the "last" page button.
     */
    public $lastPageCssClass = 'last paginate_button';
    /**
     * @var string the CSS class for the "previous" page button.
     */
    public $prevPageCssClass = 'previous paginate_button';
    /**
     * @var string the CSS class for the "next" page button.
     */
    public $nextPageCssClass = 'next paginate_button';
    /**
     * @var string the CSS class for the active (currently selected) page button.
     */
    public $activePageCssClass = 'paginate_active paginate_button';
    /**
     * @var string the CSS class for the disabled page buttons.
     */
    public $disabledPageCssClass = 'paginate_button_disabled paginate_button';

    /**
     * @var array HTML attributes for the link in a pager container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $linkOptions = ['class'=>'paginate_button'];

    /**
     * @var string the label for the "next" page button. Note that this will NOT be HTML-encoded.
     * If this property is null, the "next" page button will not be displayed.
     */
    public $nextPageLabel = '>';
    /**
     * @var string the text label for the previous page button. Note that this will NOT be HTML-encoded.
     * If this property is null, the "previous" page button will not be displayed.
     */
    public $prevPageLabel = '<';
    /**
     * @var string the text label for the "first" page button. Note that this will NOT be HTML-encoded.
     * If this property is null, the "first" page button will not be displayed.
     */
    public $firstPageLabel = "Первый";
    /**
     * @var string the text label for the "last" page button. Note that this will NOT be HTML-encoded.
     * If this property is null, the "last" page button will not be displayed.
     */
    public $lastPageLabel = "Последний";

    /**
     * Renders the page buttons.
     * @return string the rendering result
     */
    protected function renderPageButtons()
    {
        $buttons = [];

        $pageCount = $this->pagination->getPageCount();
        $currentPage = $this->pagination->getPage();

        // first page
        if ($this->firstPageLabel !== null) {
            $buttons[] = $this->renderPageButton($this->firstPageLabel, 0, $this->firstPageCssClass, $currentPage <= 0, false);
        }

        // prev page
        if ($this->prevPageLabel !== null) {
            if (($page = $currentPage - 1) < 0) {
                $page = 0;
            }
            $buttons[] = $this->renderPageButton($this->prevPageLabel, $page, $this->prevPageCssClass, $currentPage <= 0, false);
        }

        // internal pages
        list($beginPage, $endPage) = $this->getPageRange();
        for ($i = $beginPage; $i <= $endPage; ++$i) {
            $buttons[] = $this->renderPageButton($i + 1, $i, null, false, $i == $currentPage);
        }

        // next page
        if ($this->nextPageLabel !== null) {
            if (($page = $currentPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }
            $buttons[] = $this->renderPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);
        }

        // last page
        if ($this->lastPageLabel !== null) {
            $buttons[] = $this->renderPageButton($this->lastPageLabel, $pageCount - 1, $this->lastPageCssClass, $currentPage >= $pageCount - 1, false);
        }

        return Html::tag('div', implode("\n", $buttons), $this->options);
    }




    /**
     * Renders a page button.
     * You may override this method to customize the generation of page buttons.
     * @param string $label the text label for the button
     * @param integer $page the page number
     * @param string $class the CSS class for the page button.
     * @param boolean $disabled whether this page button is disabled
     * @param boolean $active whether this page button is active
     * @return string the rendering result
     */
    protected function renderPageButton($label, $page, $class, $disabled, $active)
    {
        $linkOptions = $this->linkOptions;

        $options = ['class' => $class === '' ? null : $class];
        if ($active) {
            Html::addCssClass($linkOptions, $this->activePageCssClass);
        }
        if ($disabled) {
            Html::addCssClass($linkOptions, $this->disabledPageCssClass);

            return Html::tag('a', Html::tag('span', $label), $linkOptions);
        }

        $linkOptions['data-page'] = $page;

        return Html::tag('span', Html::a($label, $this->pagination->createUrl($page), $linkOptions), $options);
    }

}