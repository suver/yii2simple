<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\data\Sort;
use yii\helpers\Html;
use yii\widgets\LinkSorter;
use yii\helpers\Inflector;

/**
 * LinkSorter renders a list of sort links for the given sort definition.
 *
 * LinkSorter will generate a hyperlink for every attribute declared in [[sort]].
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BLinkSorter extends LinkSorter
{
    /**
     * @var Sort the sort definition
     */
    public $sort;
    /**
     * @var array list of the attributes that support sorting. If not set, it will be determined
     * using [[Sort::attributes]].
     */
    public $attributes;
    /**
     * @var array HTML attributes for the sorter container tag.
     * @see \yii\helpers\Html::ul() for special attributes.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'sorter'];

    public $field_name = 'sort';
    public $field_id = 'sort';

    /**
     * Initializes the sorter.
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Executes the widget.
     * This method renders the sort links.
     */
    public function run()
    {
        echo $this->renderSortBox();
    }

    /**
     * Renders the sort links.
     * @return string the rendering result
     */
    protected function renderSortBox()
    {
        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
        $links = [];
        $button_url = false;
        //$this->sort->attributes;

        foreach($attributes as $name)
        {
            if(($direction = $this->sort->getAttributeOrder($name)) !== null)
            {
                $button_url = $this->sort->createUrl($name);
            }

            if (isset($this->sort->attributes[$name]['label'])) {
                $label = $this->sort->attributes[$name]['label'];
            } else {
                $label = Inflector::camel2words($name);
            }
            $links[$this->sort->createUrl($name)] = $label;
        }

        $sortVal = Yii::$app->getRequest()->get($this->sort->sortParam, null);

        $content = '';
        $content .= Html::label(Yii::t('app', 'Сортировать'), $this->field_name);
        $content .= Html::dropDownList($this->field_name, $button_url, $links,array('class'=>'select', 'id'=>$this->field_id));
        $content .= Html::a('<i class="icon-sort"></i>', $button_url, array('class'=>"btn btn-primary btn-icon btn-loading", 'data-loading-text'=>"<i class='icon-spinner7 spin'></i>"));

        return Html::tag('div', $content, array_merge($this->options, ['encode' => false]));
    }
}
