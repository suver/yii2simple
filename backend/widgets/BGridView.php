<?php

namespace backend\widgets;

use yii\grid\GridView;
use backend\widgets\BLinkPager;
use yii\widgets\LinkPager;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use Closure;
use yii\base\Formatter;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;


class BGridView extends GridView
{
    /**
     * @var string the layout that determines how different sections of the list view should be organized.
     * The following tokens will be replaced with the corresponding section contents:
     *
     * - `{summary}`: the summary section. See [[renderSummary()]].
     * - `{errors}`: the filter model error summary. See [[renderErrors()]].
     * - `{items}`: the list items. See [[renderItems()]].
     * - `{sorter}`: the sorter. See [[renderSorter()]].
     * - `{pager}`: the pager. See [[renderPager()]].
     */
    public $layout = "{items}\n{summary}\n{pager}";

    public $name = 'grid';

    public $summaryOptions = ['class' => 'dataTables_info'];

    public $widgetHeaderOptions = ['class'=>'datatable-header'];

    public $widgetHeaderFilterOptions = ['class' => 'dataTables_filter'];

    public $widgetHeaderTableLengthOptions = ['class' => 'dataTables_length'];

    public $widgetHeaderShowOptions = array(
        10=>10,
        20=>20,
        30=>30,
        40=>40
    );
    public $widgetHeaderShowOptionsDefault = 10;

    public $widgetHeader = true;

    /**
     * @var string the route of the controller action for displaying the sorted contents.
     * If not set, it means using the currently requested route.
     */
    public $route;
    /**
     * @var string the character used to separate different attributes that need to be sorted by.
     */
    public $separator = ',';
    /**
     * @var array parameters (name => value) that should be used to obtain the current sort directions
     * and to create new sort URLs. If not set, $_GET will be used instead.
     *
     * In order to add hash to all links use `array_merge($_GET, ['#' => 'my-hash'])`.
     *
     * The array element indexed by [[sortParam]] is considered to be the current sort directions.
     * If the element does not exist, the [[defaultOrder|default order]] will be used.
     *
     * @see sortParam
     * @see defaultOrder
     */
    public $params;
    /**
     * @var \yii\web\UrlManager the URL manager used for creating sort URLs. If not set,
     * the "urlManager" application component will be used.
     */
    public $urlManager;


    public function init()
    {
        parent::init();
        $pagination = $this->dataProvider->getPagination();
        $this->widgetHeaderShowOptionsDefault = $pagination->getPageSize();
    }



    /**
     * Runs the widget.
     */
    public function run()
    {
        $pagination = $this->dataProvider->getPagination();

        Yii::$app->request->setScriptFile( '/js/plugins/forms/select2.min.js' );
        $view = $this->getView();
        $view->registerJs( '$("#'.$this->name.'_length").change(function() {
            var self = this;
            href=$("#'.$this->name.'_length :selected").val();
            var pjax_id = $(self).closest(".pjax-wraper").attr("id");
            $.pjax({url: href, container: "#" + pjax_id});
        });');
        $view->registerJs( '$("#'.$this->name.'_filter").keydown(function (e) {
            if (e.keyCode == 13) {
                var self = this;
                val = "'.Url::to(['', 'filter'=>'PPppPPppfilter']).'";
                href=val.replace(/PPppPPppfilter/,  $("#'.$this->name.'_filter").val());
                var pjax_id = $(self).closest(".pjax-wraper").attr("id");
                $.pjax({url: href, container: "#" + pjax_id});
            }
        });');
        $view->registerJs( '$("#'.$this->name.'_length").select2({minimumResultsForSearch: "-1"});' );
        parent::run();
    }

    /**
     * Renders the data models for the grid view.
     */
    public function renderItems()
    {
        $content = array_filter([
            $this->widgetHeader ? $this->renderWidgetHeader() : false,
            $this->renderCaption(),
            $this->renderColumnGroup(),
            $this->showHeader ? $this->renderTableHeader() : false,
            $this->showFooter ? $this->renderTableFooter() : false,
            $this->renderTableBody(),
        ]);

        return Html::tag('table', implode("\n", $content), $this->tableOptions);
    }

    public function renderWidgetHeader()
    {
        $pagination = $this->dataProvider->getPagination();
        $content = array();

        $filter_value = isset($_GET["filter"]) ? $_GET["filter"] : null;
        $filter_value = isset($_POST["filter"]) ? $_POST["filter"] : $filter_value;

        $select_value = $length_value = isset($_GET[$pagination->pageSizeParam]) ? $_GET[$pagination->pageSizeParam] : $this->widgetHeaderShowOptionsDefault;

        $content[] = Html::tag('div', Html::tag('label',
            Html::tag('span', Yii::t('app','Фильтр: ')) .
            Html::textInput("{$this->name}_filter", $filter_value, array(
                'placeholder' => Yii::t('app','Фильтра по ...'),
                'id'=>"{$this->name}_filter",
            ))
        ), $this->widgetHeaderFilterOptions);

        foreach($this->widgetHeaderShowOptions as $length=>$value)
        {
            if($length_value == $value)
            {
                $select_value = $this->createUrl([$pagination->pageSizeParam=>$value]);
            }
            $widgetHeaderShowOptions[$this->createUrl([$pagination->pageSizeParam=>$value])] = $value;
        }

        $content[] = Html::tag('div', Html::tag('label',
            Html::tag('span', Yii::t('app','Показать: ')) .
            Html::tag('select', Html::renderSelectOptions($select_value, $widgetHeaderShowOptions), array(
                'name'=>"{$this->name}_length",
                'id'=>"{$this->name}_length",
            ))
        ), $this->widgetHeaderTableLengthOptions);


        return Html::tag('div', implode("\n", $content), $this->widgetHeaderOptions);
    }

    private function createUrl($attribute, $absolute = false)
    {
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        $params = array_merge($params, $attribute);
        $params[0] = $this->route === null ? Yii::$app->controller->getRoute() : $this->route;
        $urlManager = $this->urlManager === null ? Yii::$app->getUrlManager() : $this->urlManager;
        if ($absolute) {
            return $urlManager->createAbsoluteUrl($params);
        } else {
            return $urlManager->createUrl($params);
        }
    }

    /**
     * Renders the pager.
     * @return string the rendering result
     */
    public function renderPager()
    {
        $pagination = $this->dataProvider->getPagination();
        if ($pagination === false || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        /** @var LinkPager $class */
        $class = ArrayHelper::remove($this->pager, 'class', BLinkPager::className());
        $pager = $this->pager;
        $pager['pagination'] = $pagination;
        $pager['view'] = $this->getView();

        return '<div class="datatable-footer">' . $class::widget($pager) . '</div>';
    }

    /**
     * Renders the summary text.
     */
    public function renderSummary()
    {
        $count = $this->dataProvider->getCount();
        if ($count <= 0) {
            return '';
        }
        $tag = ArrayHelper::remove($this->summaryOptions, 'tag', 'div');
        if (($pagination = $this->dataProvider->getPagination()) !== false) {
            $totalCount = $this->dataProvider->getTotalCount();
            $begin = $pagination->getPage() * $pagination->pageSize + 1;
            $end = $begin + $count - 1;
            if ($begin > $end) {
                $begin = $end;
            }
            $page = $pagination->getPage() + 1;
            $pageCount = $pagination->pageCount;
            if (($summaryContent = $this->summary) === null) {
                return Html::tag($tag, Yii::t('yii', 'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.', [
                    'begin' => $begin,
                    'end' => $end,
                    'count' => $count,
                    'totalCount' => $totalCount,
                    'page' => $page,
                    'pageCount' => $pageCount,
                ]), $this->summaryOptions);
            }
        } else {
            $begin = $page = $pageCount = 1;
            $end = $totalCount = $count;
            if (($summaryContent = $this->summary) === null) {
                return Html::tag($tag, Yii::t('yii', 'Total <b>{count, number}</b> {count, plural, one{item} other{items}}.', [
                    'begin' => $begin,
                    'end' => $end,
                    'count' => $count,
                    'totalCount' => $totalCount,
                    'page' => $page,
                    'pageCount' => $pageCount,
                ]), $this->summaryOptions);
            }
        }

        return Yii::$app->getI18n()->format($summaryContent, [
            'begin' => $begin,
            'end' => $end,
            'count' => $count,
            'totalCount' => $totalCount,
            'page' => $page,
            'pageCount' => $pageCount,
        ], Yii::$app->language);
    }


    /**
     * Renders the sorter.
     * @return string the rendering result
     */
    public function renderSorter()
    {
        $sort = $this->dataProvider->getSort();
        if ($sort === false || empty($sort->attributes) || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        /** @var LinkSorter $class */
        $class = ArrayHelper::remove($this->sorter, 'class', LinkSorter::className());
        $sorter = $this->sorter;
        $sorter['sort'] = $sort;
        $sorter['view'] = $this->getView();

        return $class::widget($sorter);
    }
}
