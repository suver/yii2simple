<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\web\Request;

/**
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BListView extends ListView
{

    public $name = 'list';

    public $showOnEmpty = true;

    public $widgetHeaderShowOptions = array(
        10=>10,
        20=>20,
        30=>30,
        40=>40
    );
    public $widgetHeaderShowOptionsDefault = 10;

    /**
     * @var string the route of the controller action for displaying the sorted contents.
     * If not set, it means using the currently requested route.
     */
    public $route;
    /**
     * @var string the character used to separate different attributes that need to be sorted by.
     */
    public $separator = ',';
    /**
     * @var array parameters (name => value) that should be used to obtain the current sort directions
     * and to create new sort URLs. If not set, $_GET will be used instead.
     *
     * In order to add hash to all links use `array_merge($_GET, ['#' => 'my-hash'])`.
     *
     * The array element indexed by [[sortParam]] is considered to be the current sort directions.
     * If the element does not exist, the [[defaultOrder|default order]] will be used.
     *
     * @see sortParam
     * @see defaultOrder
     */
    public $params;
    /**
     * @var \yii\web\UrlManager the URL manager used for creating sort URLs. If not set,
     * the "urlManager" application component will be used.
     */
    public $urlManager;

    /**
     * Runs the widget.
     */
    public function run()
    {
        $pagination = $this->dataProvider->getPagination();

        Yii::$app->request->setScriptFile( '/js/plugins/forms/select2.min.js' );
        $view = $this->getView();
        $view->registerJs( '$("#'.$this->name.'_sort").change(function() {
            var self = this;
            href=$("#'.$this->name.'_sort :selected").val();
            var pjax_id = $(self).closest(".pjax-wraper").attr("id");
            $.pjax({url: href, container: "#" + pjax_id});
        });');

        $view->registerJs( '$("#'.$this->name.'_length").change(function() {
            var self = this;
            href=$("#'.$this->name.'_length :selected").val();
            var pjax_id = $(self).closest(".pjax-wraper").attr("id");
            $.pjax({url: href, container: "#" + pjax_id});
        });');

        $view->registerJs( '$("#'.$this->name.'_filter").keydown(function (e) {
            if (e.keyCode == 13) {
                '.$this->name.'_filter(this);
            }
        });

        $("#'.$this->name.'_filter_button").click(function (e) {
            '.$this->name.'_filter(this);
        });

        function '.$this->name.'_filter(self)
        {
            val = "'.Url::to(['', 'filter'=>'PPppPPppfilter']).'";
            href=val.replace(/PPppPPppfilter/,  $("#'.$this->name.'_filter").val());
            var pjax_id = $(self).closest(".pjax-wraper").attr("id");
            $.pjax({url: href, container: "#" + pjax_id});
        }
        ');
        $view->registerJs( '$("#'.$this->name.'_length").select2({minimumResultsForSearch: "-1"});' );
        $view->registerJs( '$("#'.$this->name.'_sort").select2({minimumResultsForSearch: "-1", width: 200});' );


        parent::run();
    }


    /**
     * Renders a section of the specified name.
     * If the named section is not supported, false will be returned.
     * @param string $name the section name, e.g., `{summary}`, `{items}`.
     * @return string|boolean the rendering result of the section, or false if the named section is not supported.
     */
    public function renderSection($name)
    {
        switch ($name) {
            case '{summary}':
                return $this->renderSummary();
            case '{items}':
                return $this->renderItems();
            case '{pager}':
                return $this->renderPager();
            case '{filter}':
                return $this->renderFilter();
            case '{sorter}':
                return $this->renderSorter();
            default:
                return false;
        }
    }

    /**
     * Renders the sorter.
     * @return string the rendering result
     */
    public function renderSorter()
    {
        $pagination = $this->dataProvider->getPagination();
        $sort = $this->dataProvider->getSort();
        if ($sort === false || empty($sort->attributes))
        {
            return '';
        }

        /** @var LinkSorter $class */
        $class = ArrayHelper::remove($this->sorter, 'class', BLinkSorter::className());

        $sorter = $this->sorter;
        $sorter['sort'] = $sort;
        $sorter['field_name'] = $this->name.'_sort';
        $sorter['field_id'] = $this->name.'_sort';
        $sorter['view'] = $this->getView();

        $select_value = $length_value = isset($_GET[$pagination->pageSizeParam]) ? $_GET[$pagination->pageSizeParam] : $this->widgetHeaderShowOptionsDefault;

        $filter_value = isset($_GET["filter"]) ? $_GET["filter"] : null;
        $filter_value = isset($_POST["filter"]) ? $_POST["filter"] : $filter_value;

        $content[] = Html::tag('div', Html::tag('form',
            Html::tag('label', Yii::t('app','Фильтр: ')) .
            Html::textInput("{$this->name}_filter", $filter_value, array(
                'placeholder' => Yii::t('app','Фильтра по ...'),
                'id'=>"{$this->name}_filter",
                'class' => 'form-control',
            )) .
            Html::button('<i class="icon-search3"></i>', ['class'=>"btn btn-primary btn-icon btn-loading", 'data-loading-text'=>"<i class='icon-spinner7 spin'></i>", 'id'=>"{$this->name}_filter_button"])
        ), array('class'=>'bar-left'));

        foreach($this->widgetHeaderShowOptions as $length=>$value)
        {
            if($length_value == $value)
            {
                $select_value = $this->createUrl([$pagination->pageSizeParam=>$value]);
            }
            $widgetHeaderShowOptions[$this->createUrl([$pagination->pageSizeParam=>$value])] = $value;
        }

        $content[] = Html::tag('div',
            //Html::tag('label',Html::tag('span', Yii::t('app','Показать: '))) .
            Html::tag('select', Html::renderSelectOptions($select_value, $widgetHeaderShowOptions), array(
                'name'=>"{$this->name}_length",
                'id'=>"{$this->name}_length",
            )), array('class'=>'bar-right'));

        $content[] = Html::tag('div', $class::widget($sorter), array('class'=>'bar-right'));

        return Html::tag('div', implode("\n", $content), ['class'=>'bar block clearfix']);
    }

    private function createUrl($attribute, $absolute = false)
    {
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        $params = array_merge($params, $attribute);
        $params[0] = $this->route === null ? Yii::$app->controller->getRoute() : $this->route;
        $urlManager = $this->urlManager === null ? Yii::$app->getUrlManager() : $this->urlManager;
        if ($absolute) {
            return $urlManager->createAbsoluteUrl($params);
        } else {
            return $urlManager->createUrl($params);
        }
    }

    /**
     * Renders the sorter.
     * @return string the rendering result
     */
    /*public function renderSorter()
    {
        var_dump(parent::renderSorter());

        return parent::renderSorter();
    }*/

    /**
     * Renders the pager.
     * @return string the rendering result
     */
    public function renderPager()
    {
        return Html::tag('div', parent::renderPager(), array('class' => 'text-center block'));
    }

    /**
     * Renders all data models.
     * @return string the rendering result
     */
    public function renderItems()
    {
        $models = $this->dataProvider->getModels();
        $keys = $this->dataProvider->getKeys();
        $rows = [];
        foreach (array_values($models) as $index => $model)
        {
            $rows[] = $this->renderItem($model, $keys[$index], $index);
        }

        return Html::tag('div', implode($this->separator, $rows), array('class' => 'row'));
    }

    /**
     * Renders a single data model.
     * @param mixed $model the data model to be rendered
     * @param mixed $key the key value associated with the data model
     * @param integer $index the zero-based index of the data model in the model array returned by [[dataProvider]].
     * @return string the rendering result
     */
    public function renderItem($model, $key, $index)
    {
        if ($this->itemView === null) {
            $content = $key;
        } elseif (is_string($this->itemView)) {
            $content = $this->getView()->render($this->itemView, array_merge([
                'model' => $model,
                'key' => $key,
                'index' => $index,
                'widget' => $this,
            ], $this->viewParams));
        } else {
            $content = call_user_func($this->itemView, $model, $key, $index, $this);
        }
        $options = $this->itemOptions;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        if ($tag !== false) {
            $options['data-key'] = is_array($key) ? json_encode($key) : (string) $key;

            return Html::tag($tag, $content, $options);
        } else {
            return $content;
        }
    }
}
