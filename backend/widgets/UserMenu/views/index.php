<?php

/**
 * @var yii\base\View $this View
 */

use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

?>


<li class="user dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->context->title ?><i class="caret"></i></a>
    <ul class="dropdown-menu dropdown-menu-right icons-right">
        <?php foreach($this->context->items as $item) {?>
            <?php if($item['visible']) { ?>
                <li><a href="<?php echo Url::to($item['url']) ?>"><?php echo $item['label'] ?></a></li>
            <?php } ?>
        <?php } ?>
    </ul>
</li>