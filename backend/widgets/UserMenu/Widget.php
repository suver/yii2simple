<?php

namespace backend\widgets\UserMenu;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\web\YiiAsset;

/**
 * Control widget
 */
class Widget extends \yii\base\Widget
{

    /**
     * @var array Items array
     */
    public $title = '';

    /**
     * @var array Items array
     */
    public $items = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->registerTranslations();
        $this->initDefaultItems();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('index');
    }

    /**
     * Set default control items.
     */
    public function initDefaultItems()
    {
        $items = [];

        $this->items = ArrayHelper::merge($items, $this->items);
    }

    /**
     * Register widget translations.
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['UserMenu'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@backend/widgets/UserMenu/messages',
            'forceTranslation' => true
        ];
    }
}
