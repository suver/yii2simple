<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 *
 * ~~~
 * echo BMenu::widget([
 *     'items' => [
 *         // Important: you need to specify url as 'controller/action',
 *         // not just as 'controller' even if default action is used.
 *         ['label' => 'Home', 'url' => ['site/index']],
 *         // 'Products' menu item will be selected as long as the route is 'product/index'
 *         ['label' => 'Products', 'url' => ['product/index'], 'items' => [
 *             ['label' => 'New Arrivals', 'url' => ['product/index', 'tag' => 'new']],
 *             ['label' => 'Most Popular', 'url' => ['product/index', 'tag' => 'popular']],
 *         ]],
 *         ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
 *     ],
 * ]);
 * ~~~
 */
class BMenu extends \yii\widgets\Menu
{

    public $items = [];

    public $itemOptions = [];

    public $linkTemplate = '<a href="{url}" {linkHtmlOption}><span>{label}</span> {icon}</a>';

    public $linkHtmlOption = [];

    public $labelTemplate = '{label}';

    public $submenuTemplate = "\n<ul>\n{items}\n</ul>\n";

    public $encodeLabels = true;

    public $activeCssClass = 'active';

    public $expandCssClass = 'expand';

    public $activateItems = true;

    public $activateParents = true;

    public $hideEmptyItems = true;

    public $options = [];

    public $firstItemCssClass;

    public $lastItemCssClass;

    public $route;

    public $params;



    public function run()
    {
        if ($this->route === null && Yii::$app->controller !== null) {
            $this->route = Yii::$app->controller->getRoute();
        }
        if ($this->params === null) {
            $this->params = Yii::$app->request->getQueryParams();
        }
        $items = $this->normalizeItems($this->items, $hasActiveChild);
        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'ul');
        echo Html::tag($tag, $this->renderItems($items), $options);
    }


    protected function renderItems($items, $level=0)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];
            if ($item['active']) {
                $class[] = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }

            $menu = $this->renderItem($item, $level);
            if (!empty($item['items'])) {
                $level++;
                $item['level'] = $level;
                $menu .= strtr($this->submenuTemplate, [
                    '{items}' => $this->renderItems($item['items'], $level),
                ]);
                $level--;
            }
            $lines[] = Html::tag($tag, $menu, $options);
        }

        return implode("\n", $lines);
    }


    protected function renderItem($item, $level=0)
    {
        if (isset($item['url'])) {

            if(!empty($item['items']))
            {
                if (empty($item['linkHtmlOption']['class'])) {
                    $item['linkHtmlOption']['class'] = $this->expandCssClass;
                } else {
                    $item['linkHtmlOption']['class'] .= ' ' . $this->expandCssClass;
                }
            }

            if(!empty($item['active']))
            {
                if (empty($item['linkHtmlOption']['id'])) {
                    $item['linkHtmlOption']['id'] = "second-level";
                } else {
                    $item['linkHtmlOption']['id'] .= ' second-level';
                }
            }

            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

            $strLinkHtmlOption = '';
            if(isset($item['linkHtmlOption']))
            {
                foreach($item['linkHtmlOption'] as $key=>$val)
                {
                    $strLinkHtmlOption .= " {$key}=\"{$val}\"";
                }
            }

            $icon = '';
            if(!empty($item['icon']))
            {
                $icon = '<i class="'.$item['icon'].'"></i>';
            }

            return strtr($template, [
                '{url}' => Url::to($item['url']),
                '{label}' => $item['label'],
                '{linkHtmlOption}' => $strLinkHtmlOption,
                '{icon}' => $icon,

            ]);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{label}' => $item['label'],
            ]);
        }
    }


    protected function normalizeItems($items, &$active)
    {
        foreach ($items as $i => $item)
        {
            if (isset($item['visible']) && !$item['visible']) {
                unset($items[$i]);
                continue;
            }

            if (!isset($item['label'])) {
                $item['label'] = '';
            }

            if ($this->encodeLabels) {
                $items[$i]['label'] = Html::encode($item['label']);
            }

            $hasActiveChild = false;
            if (isset($item['items'])) {

                $items[$i]['items'] = $this->normalizeItems($item['items'], $hasActiveChild);

                if (empty($items[$i]['items']) && $this->hideEmptyItems) {
                    unset($items[$i]['items']);
                    if (!isset($item['url'])) {
                        unset($items[$i]);
                        continue;
                    }
                }
            }

            if (!isset($item['active'])) {
                if ($this->activateParents && $hasActiveChild || $this->activateItems && $this->isItemActive($item)) {
                    $active = $items[$i]['active'] = true;
                } else {
                    $items[$i]['active'] = false;
                }
            } elseif ($item['active']) {
                $active = true;
            }
        }

        return array_values($items);
    }


    protected function isItemActive($item)
    {
        if (isset($item['url']) && is_array($item['url']) && isset($item['url'][0])) {
            $route = $item['url'][0];

            if (isset($route[0]) AND $route[0] !== '/' && Yii::$app->controller) {
                $route = Yii::$app->controller->module->getUniqueId() . '/' . $route;
            }
            if (ltrim($route, '/') !== $this->route) {
                return false;
            }
            unset($item['url']['#']);
            if (count($item['url']) > 1) {
                foreach (array_splice($item['url'], 1) as $name => $value) {
                    if ($value !== null && (!isset($this->params[$name]) || $this->params[$name] != $value)) {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }
}
