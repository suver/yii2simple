<?php
namespace backend\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class jGrowl extends Widget
{
    public $ignoreFlashType = [];

    /**
     * Initializes the FragmentCache object.
     */
    public function init()
    {
        parent::init();
        ob_start();
        ob_implicit_flush(false);
    }

    /**
     * Renders the page buttons.
     * @return string the rendering result
     */
    public function run()
    {
        $content = ob_get_clean();
        $view = $this->getView();
        if(Yii::$app->session->getAllFlashes()) {
            foreach(Yii::$app->session->getAllFlashes() as $key=>$value) {
                $flash = Yii::$app->session->getFlash($key, null, true);
                if(!in_array($key, $this->ignoreFlashType))
                {
                    if(is_string($flash))
                    {
                        $view->registerJs("$.jGrowl('" . $flash . "', { sticky: true, theme: 'growl-" . $key . "' });");
                    }
                    elseif(is_array($flash))
                    {
                        if(isset($flash['message']) AND isset($flash['header']))
                        {
                            $view->registerJs("$.jGrowl('" . $flash['message'] . "', { sticky: true, theme: 'growl-" . $key . "' , header: '" . $flash['header'] . "' });");
                        }
                        elseif(isset($flash['0']) AND isset($flash['1']))
                        {
                            $view->registerJs("$.jGrowl('" . $flash['1'] . "', { sticky: true, theme: 'growl-" . $key . "' , header: '" . $flash['0'] . "' });");
                        }
                    }
                    //Yii::$app->session->removeFlash($key);
                }
                else {

                    $_content = $content;
                    $_content = str_replace("{type}", $key, $_content);

                    if(is_string($flash))
                    {
                        $_content = str_replace("{header}", '', $_content);
                        $_content = str_replace("{message}", $flash, $_content);
                    }
                    elseif(is_array($flash))
                    {
                        if(isset($flash['message']) AND isset($flash['header']))
                        {
                            $_content = str_replace("{header}", $flash['header'], $_content);
                            $_content = str_replace("{message}", $flash['message'], $_content);
                        }
                        elseif(isset($flash['0']) AND isset($flash['1']))
                        {
                            $_content = str_replace("{header}", $flash['0'], $_content);
                            $_content = str_replace("{message}", $flash['1'], $_content);
                        }
                    }


                    echo $_content;
                }
            }
        }
    }

}