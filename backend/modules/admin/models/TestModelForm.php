<?php
namespace backend\modules\admin\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class TestModelForm extends Model
{
    public $testString;
    public $testStringWithTooltip;
    public $testStringWithInfo;
    public $testStringWithWarning;
    public $testStringWithHint;
    public $testStringWithAfterIcon;
    public $testStringWithLeftIcon;
    public $testNumeric = 1;
    public $testMultiSelect_1;
    public $testSingleSelect_1;
    public $testSelect2_1;
    public $testSelect2Disabled_1;
    public $testMultiSelect2_1;
    public $testSwitch_1;
    public $testSwitch_2;
    public $testEmail;
    public $testPassword;
    public $testElasticTextarea;
    public $testElasticWithLimitTextarea;
    public $testTextarea;
    public $testFile;
    public $testMask;
    public $testDropDownList;
    public $testTags;
    public $testChackbox;
    public $testRadio;
    public $testChackboxList;
    public $testChackboxListWithColored;
    public $testRadioList;
    public $testRadioListWithColored;
    public $testListBox;
    public $testMultipliListBox;
    public $testOnliValue = 'value';
    public $testDatetimeLocal;
    public $testHtml5DatetimeLocal;
    public $testHtml5Time;
    public $testHtml5Date;
    public $testHtml5Month;
    public $testHtml5Url;
    public $testHtml5Email;
    public $testHtml5Number;
    public $testHtml5Week;
    public $testHtml5Tel;
    public $testHtml5Search;
    public $testHtml5Datetime;

    public $testSimpleWysiwyg;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['testEmail', 'testPassword'], 'required'],
            // rememberMe must be a boolean value
            ['testInt', 'boolean'],
            // password is validated by validatePassword()
            ['testPassword', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->testPassword)) {
                $this->addError('testPassword', 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function getAutocomliteForTags()
    {
        return '[
                { "id": "Netta rufina", "label": "Red-crested Pochard", "value": "Red-crested Pochard" },
                { "id": "Sterna sandvicensis", "label": "Sandwich Tern", "value": "Sandwich Tern" },
                { "id": "Saxicola rubetra", "label": "Whinchat", "value": "Whinchat" },
                { "id": "Saxicola rubicola", "label": "European Stonechat", "value": "European Stonechat" },
                { "id": "Lanius senator", "label": "Woodchat Shrike", "value": "Woodchat Shrike" },
                { "id": "Coccothraustes coccothraustes", "label": "Hawfinch", "value": "Hawfinch" },
                { "id": "Ficedula hypoleuca", "label": "Eurasian Pied Flycatcher", "value": "Eurasian Pied Flycatcher" },
                { "id": "Sitta europaea", "label": "Eurasian Nuthatch", "value": "Eurasian Nuthatch" },
                { "id": "Pyrrhula pyrrhula", "label": "Eurasian Bullfinch", "value": "Eurasian Bullfinch" },
                { "id": "Muscicapa striata", "label": "Spotted Flycatcher", "value": "Spotted Flycatcher" },
                { "id": "Carduelis chloris", "label": "European Greenfinch", "value": "European Greenfinch" },
                { "id": "Carduelis carduelis", "label": "European Goldfinch", "value": "European Goldfinch" }
            ]';
    }

}
