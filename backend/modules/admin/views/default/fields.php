<?php

use yii\helpers\Html;
use suver\widgets\ActiveForm;
use common\modules\user\models\User;
use common\modules\news\models\News;

/**
 * @var yii\web\View $this
 * @var common\modules\news\models\News $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            //'template' => '{label}<div class="col-sm-10">{input}{error}</div>',
            //'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ]
    ]); ?>

    <?php echo $form->field($modelForm, 'testFile')->fileInput() ?>

    <?php echo $form->field($modelForm, 'testPassword')->passwordInput(['helper'=>true]) ?>

    <?php echo $form->field($modelForm, 'testEmail')->textInput() ?>

    <?php echo $form->field($modelForm, 'testString')->textInput(['maxlength' => 100, 'before'=>'<i class="icon-grid"></i>', 'after'=>'11']) ?>

    <?php echo $form->field($modelForm, 'testNumeric')->numericInput(['step' => 10]) ?>

    <?php echo $form->field($modelForm, 'testSingleSelect_1')->singleSelect(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3']) ?>

    <?php echo $form->field($modelForm, 'testMultiSelect_1')->multiSelect(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3']) ?>

    <?php echo $form->field($modelForm, 'testSelect2_1')->select2(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3']) ?>

    <?php echo $form->field($modelForm, 'testSelect2Disabled_1')->select2(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3'], ['disabled'=>true]) ?>

    <?php echo $form->field($modelForm, 'testMultiSelect2_1')->multiSelect2(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3'], ['empty'=>Yii::t('news', 'Выберите')]) ?>

    <?php echo $form->field($modelForm, 'testSwitch_1')->switchButton() ?>

    <?php echo $form->field($modelForm, 'testSwitch_2')->switchButton(['on'=>'Да', 'off'=>'<i class="icon-apple"></i>', 'on-class'=>'success', 'off-class'=>'warning' ]) ?>

    <?php echo $form->field($modelForm, 'testStringWithTooltip')->textInput(['tooltip'=>Yii::t('news','Допускаются латинские буквы, цифры, знак дефис и знак подчеркивания - Aa-Zz, 0-9, -, _')])?>

    <?php echo $form->field($modelForm, 'testStringWithInfo')->textInput()->info(Yii::t('news','Учавствует в генерации ссылки на новость')) ?>

    <?php echo $form->field($modelForm, 'testStringWithWarning')->textInput()->warning(Yii::t('news','Учавствует в генерации ссылки на новость')) ?>

    <?php echo $form->field($modelForm, 'testStringWithHint')->textInput()->hint(Yii::t('news','Учавствует в генерации ссылки на новость')) ?>

    <?php echo $form->field($modelForm, 'testStringWithAfterIcon')->input('text',['icon'=>'icon-warning']) ?>

    <?php echo $form->field($modelForm, 'testStringWithLeftIcon')->input('text',['left-icon'=>'icon-warning']) ?>

    <?php echo $form->field($modelForm, 'testMask')->input('text',['mask'=>'99999']) ?>

    <?php echo $form->field($modelForm, 'testElasticTextarea')->elasticTextarea(['rows' => 3]) ?>

    <?php echo $form->field($modelForm, 'testElasticWithLimitTextarea')->elasticTextarea(['rows' => 3, 'limit' => 255]) ?>

    <?php echo $form->field($modelForm, 'testTextarea')->textarea(['rows' => 3]) ?>

    <?php echo $form->field($modelForm, 'testDropDownList')->dropDownList(['Val 1', 'Val 2', 'Val 3']) ?>

    <?php echo $form->field($modelForm, 'testTags')->tags(['','tags_search'=>1], ['value'=>'test1,test2,test3']) ?>

    <?php echo $form->field($modelForm, 'testChackbox')->checkbox() ?>

    <?php echo $form->field($modelForm, 'testRadio')->radio() ?>

    <?php echo $form->field($modelForm, 'testChackboxList')->checkboxList(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3']) ?>

    <?php echo $form->field($modelForm, 'testChackboxListWithColored')->checkboxList(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3'], ['labelClass'=>'checkbox-success']) ?>

    <?php echo $form->field($modelForm, 'testRadioList')->radioList(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3']) ?>

    <?php echo $form->field($modelForm, 'testRadioListWithColored')->radioList(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3'], ['labelClass'=>'radio-primary']) ?>

    <?php echo $form->field($modelForm, 'testListBox')->listBox(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3']) ?>

    <?php echo $form->field($modelForm, 'testMultipliListBox')->multyListBox(['Key 1'=>'Val 1', 'Key 2'=>'Val 2', 'Key 3'=>'Val 3']) ?>

    <?php echo $form->field($modelForm, 'testSimpleWysiwyg')->simpleWysiwyg() ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title"><i class="icon-html5"></i> Advanced inputs</h6>
        </div>
        <div class="panel-body">

            <?php echo $form->field($modelForm, 'testHtml5DatetimeLocal')->html5DatetimeLocal() ?>

            <?php echo $form->field($modelForm, 'testHtml5Time')->html5Time() ?>

            <?php echo $form->field($modelForm, 'testHtml5Date')->html5Date() ?>

            <?php echo $form->field($modelForm, 'testHtml5Month')->html5Month() ?>

            <?php echo $form->field($modelForm, 'testHtml5Url')->html5Url() ?>

            <?php echo $form->field($modelForm, 'testHtml5Email')->html5Email() ?>

            <?php echo $form->field($modelForm, 'testHtml5Number')->html5Number() ?>

            <?php echo $form->field($modelForm, 'testHtml5Week')->html5Week() ?>

            <?php echo $form->field($modelForm, 'testHtml5Tel')->html5Tel() ?>

            <?php echo $form->field($modelForm, 'testHtml5Search')->html5Search() ?>

            <?php echo $form->field($modelForm, 'testHtml5Datetime')->html5Datetime() ?>

        </div>
    </div>





    <div class="form-group field-news-update_time">
        <label class="col-sm-2 control-label" for="news-update_time">Attribue / Key / Name</label>
        <div class="col-sm-10">
            <p class="form-control-static"><?php echo Html::encode($modelForm->testOnliValue) ?></p>
        </div>
    </div>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('news', 'Обновить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
