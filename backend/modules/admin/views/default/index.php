<?php
/**
 * @var yii\web\View $this
 */
$this->title = Yii::t('app','Доска');
?>

<div class="row">
    <div class="col-sm-6">
        <?php echo backend\modules\user\widgets\LastUsersActivity\Widget::widget(['title'=>Yii::t('app','Активность пользователей')]); ?>
    </div>
    <div class="col-sm-6">
        <?php echo backend\modules\user\widgets\MyActivity\Widget::widget(['title'=>Yii::t('app','Моя Активность')]); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php echo backend\modules\user\widgets\LastUsers\Widget::widget(['title'=>Yii::t('app','Последние активные пользователи')]); ?>
    </div>
</div>
