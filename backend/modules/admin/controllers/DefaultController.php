<?php

namespace backend\modules\admin\controllers;

use backend\controllers\AbstractBackendController;
use backend\modules\admin\models\TestModelForm;

class DefaultController extends AbstractBackendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionIcons()
    {
        return $this->render('icons');
    }

    public function actionFields()
    {
        $modelForm = new TestModelForm;
        if(isset($_GET['tags_search']))
        {
            return $modelForm->getAutocomliteForTags();
        }

        return $this->render('fields', ['modelForm'=>$modelForm]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
