<?php

/**
 * @var yii\base\View $this View
 */

use yii\helpers\Url;
use yii\helpers\Html;

?>
<h6><i class="icon-people"></i> <?php echo $title ?></h6>
<div class="row">
    <?php foreach($Users as $User) { ?>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="block">
            <div class="thumbnail">
                <a href="<?php echo $User->avatar->getImage("500x500") ?>" class="thumb-zoom lightbox" title="<?php echo $User->name . " " . $User->surname ?>">
                    <img src="<?php echo $User->avatar->getImage("300x300") ?>"   data-avatar-user="<?php echo ($User->id == Yii::$app->user->identity->id) ? "me" : $User->id ?>" <?php echo ($User->id == Yii::$app->user->identity->id) ? 'data-avatar-size="300x300"' : '' ?>  data-avatar-user="<?php echo $User->id ?>">
                </a>
                <div class="caption text-center">
                    <h6><?php echo $User->name . " " . $User->surname ?> <small><?php echo $User->username ?></small></h6>
                    <div class="icons-group">
                        <a href="#" title="" class="tip" data-original-title="Google Drive"><i class="icon-google-drive"></i></a>
                        <a href="#" title="" class="tip" data-original-title="Twitter"><i class="icon-twitter"></i></a>
                        <a href="#" title="" class="tip" data-original-title="Github"><i class="icon-github3"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>