<?php

namespace backend\modules\user\widgets\LastUsers;

use Yii;
use yii\helpers\ArrayHelper;
use common\modules\user\models\User;

/**
 * Control widget
 */
class Widget extends \yii\base\Widget
{

    /**
     * @var array Items array
     */
    public $title = '';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $view = $this->getView();
        $view->registerJsFile('/js/plugins/interface/fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()], 'position' => \yii\web\View::POS_HEAD]);

        $this->registerTranslations();
        $this->initDefaultItems();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $Users = User::find()->orderBy('create_time DESC')->limit(4)->all();
        return $this->render('index', array(
            'Users' => $Users,
            'title' => $this->title,
        ));
    }

    /**
     * Set default control items.
     */
    public function initDefaultItems()
    {

        $items = [];
    }

    /**
     * Register widget translations.
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['UserActivity'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@backend/modules/user/widget/LastUsers',
            'forceTranslation' => true
        ];
    }
}
