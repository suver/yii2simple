<?php

/**
 * @var yii\base\View $this View
 */

use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="block">
    <h6 class="heading-hr"><i class="icon-file"></i> <?php echo $title ?></h6>
    <ul class="media-list">
        <?php foreach($Activitys as $Activity) { ?>
            <?php if($MasterUser = $Activity->getMasterUser()) { ?>
                <li class="media">
                    <a class="pull-left thumb-zoom lightbox" href="<?php echo $MasterUser->avatar->getImage("500x500") ?>" title="<?php echo $MasterUser->name . " " . $MasterUser->surname ?>">
                        <img class="media-object" src="<?php echo $MasterUser->avatar->getImage("44x44") ?>"  data-avatar-user="<?php echo ($MasterUser->id == Yii::$app->user->identity->id) ? "me" : $MasterUser->id ?>" <?php echo ($MasterUser->id == Yii::$app->user->identity->id) ? 'data-avatar-size="200x200"' : '' ?>  data-avatar-user="<?php echo $MasterUser->id ?>">
                    </a>

                    <?php if($SlaveUser = $Activity->getSlaveUser()) { ?>
                    <a class="pull-left thumb-zoom lightbox" href="<?php echo $SlaveUser->avatar->getImage("500x500") ?>" title="<?php echo $SlaveUser->name . " " . $SlaveUser->surname ?>">
                        <img class="media-object" src="<?php echo $SlaveUser->avatar->getImage("44x44") ?>"  data-avatar-user="<?php echo ($SlaveUser->id == Yii::$app->user->identity->id) ? "me" : $SlaveUser->id ?>" <?php echo ($SlaveUser->id == Yii::$app->user->identity->id) ? 'data-avatar-size="200x200"' : '' ?>  data-avatar-user="<?php echo $SlaveUser->id ?>">
                    </a>
                    <?php } ?>

                    <div class="media-body">
                        <div class="clearfix">
                            <a href="<?php echo Url::toRoute(['/user/edit/index', 'id' => $MasterUser->id]) ?>" class="media-heading"><?php echo $MasterUser->name . ' ' . $MasterUser->surname ?></a>
                            <span class="media-notice"><?php echo $Activity->time->format('d M Y H:i') ?> / <?php echo $Activity->time->ago() ?></span>
                        </div>
                        <?php echo $Activity->getMessage() ?>
                    </div>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
</div>