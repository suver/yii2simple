<?php

namespace backend\modules\user\widgets\LastUsersActivity;

use Yii;
use yii\helpers\ArrayHelper;
use common\modules\user\models\User;

/**
 * Control widget
 */
class Widget extends \yii\base\Widget
{

    /**
     * @var array Items array
     */
    public $title = '';
    public $limit = 10;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $view = $this->getView();
        $view->registerJsFile('/js/plugins/interface/fancybox.min.js', ['depends' => [\yii\web\JqueryAsset::className()],'position' => \yii\web\View::POS_HEAD]);

        $this->registerTranslations();
        $this->initDefaultItems();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $Activitys = Yii::$app->activity->get()->orderBy('time DESC')->limit($this->limit)->all();
        return $this->render('index', array(
            'Activitys' => $Activitys,
            'title' => $this->title,
        ));
    }

    /**
     * Set default control items.
     */
    public function initDefaultItems()
    {

        $items = [];
    }

    /**
     * Register widget translations.
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['UserActivity'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@backend/modules/user/widget/LastUsersActivity',
            'forceTranslation' => true
        ];
    }
}
