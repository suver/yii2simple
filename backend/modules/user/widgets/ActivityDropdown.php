<?php
namespace backend\modules\user\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class ActivityDropdown extends Widget
{
    public $userModel = null;

    /**
     * Renders the page buttons.
     * @return string the rendering result
     */
    public function run()
    {
        ?>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown"><i class="icon-people"></i><span class="label label-default"><?php echo Yii::$app->activity->getForUser(Yii::$app->user->id)->count() ?></span></a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="popup-header">
                    <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
                    <span><?php echo Yii::t('user', 'Активность') ?></span>
                    <a href="#" class="pull-right"><i class="icon-paragraph-justify"></i></a>
                </div>
                <ul class="activity">

                    <?php
                    foreach(Yii::$app->activity->getForUser(Yii::$app->user->id)->limit(10)->orderBy(['id' => SORT_DESC])->all() as $activity) {
                        ?>
                        <li>
                            <i class="pull-left glyphicon <?php echo $activity->getIcon() ?> text-info"></i>
                            <div> <?php echo $activity->getMessage() ?> <span><?php echo $activity->time->format("d M Y H:i") ?></span> </div>
                        </li>
                    <?php
                    }
                    ?>

                </ul>
            </div>
        </li>
    <?php
    }

}