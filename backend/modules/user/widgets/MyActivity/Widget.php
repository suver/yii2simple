<?php

namespace backend\modules\user\widgets\MyActivity;

use Yii;
use yii\helpers\ArrayHelper;
use common\modules\user\models\User;

/**
 * Control widget
 */
class Widget extends \yii\base\Widget
{

    /**
     * @var array Items array
     */
    public $title = '';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->registerTranslations();
        $this->initDefaultItems();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $User = User::findById(Yii::$app->user->identity->id);
        return $this->render('index', array(
            'User' => $User,
            'title' => $this->title,
        ));
    }

    /**
     * Set default control items.
     */
    public function initDefaultItems()
    {
        $items = [];
    }

    /**
     * Register widget translations.
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['UserActivity'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@backend/modules/user/widget/MyActivity',
            'forceTranslation' => true
        ];
    }
}
