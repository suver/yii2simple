<?php

namespace backend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use common\modules\user\models\User;
use yii\data\Pagination;
use yii\helpers\Url;
use backend\controllers\AbstractBackendController;

class ListController extends AbstractBackendController
{
    public function actionIndex()
    {

        $User = new User;

        $dataProvider =  $User->search($_GET, true);

        return $this->render('index', array(
            'dataProvider'=>$dataProvider,
        ));
    }

    public function actionGrid()
    {

        //$query = User::find()->where(['status'=>1]);
        /*$query = User::find();

        $countQuery = clone $query;

        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
        ]);

        // Колличество записей на страницу
        $pages->setPageSize(20);

        $Users = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
*/
        $Users = new User;

        $dataProvider =  $Users->search($_GET, true);

        return $this->render('grid', array(
            //'pages' => $pages,
            'Users' => $Users,
            'dataProvider' => $dataProvider,
        ));
    }
}
