<?php

namespace backend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use common\modules\user\models\User;
use yii\data\Pagination;
use yii\helpers\Url;
use backend\controllers\AbstractBackendController;
use yii\gii\generators\extension;
use yii\helpers\Security;
use yii\helpers\Json;

class ProfileController extends AbstractBackendController
{
    public function actionMe()
    {

        if(!Yii::$app->user->isGuest AND Yii::$app->user->id)
        {
            $isActiveTab = !empty($_COOKIE['ActiveTabInMeProfile']) ? $_COOKIE['ActiveTabInMeProfile'] : 'profile-profile';

            $ChangeEmailForm = new \common\modules\user\models\ChangeEmailForm();
            $ChangeEmailForm->scenario = 'start';
            if(Yii::$app->request->post('ChangeEmailForm', false))
            {
                setcookie('ActiveTabInMeProfile', 'profile-security', time()+36000, '/');
                if ($ChangeEmailForm->load(Yii::$app->request->post()) && $ChangeEmailForm->sendChangeLinkToEmail(Yii::$app->user->id))
                {
                    Yii::$app->activity->setMe('sendChangeLinkToEmail', Yii::t('user', '{master_user} запрсил(a) смену email'));

                    Yii::$app->session->setFlash('success', Yii::t('user', 'Письмо с инструкциями отправленно на Email'));
                    return $this->refresh();
                }
            }

            $ChangePasswordForm = new \common\modules\user\models\ChangePasswordForm();
            if(Yii::$app->request->post('ChangePasswordForm', false))
            {
                setcookie('ActiveTabInMeProfile', 'profile-security', time()+36000, '/');
                if($ChangePasswordForm->load(Yii::$app->request->post()) AND $ChangePasswordForm->changePassword())
                {
                    Yii::$app->activity->setMe('changeMyPassword', Yii::t('user', '{master_user} сменил(a) пароль'));

                    Yii::$app->session->setFlash('success', Yii::t('user', 'Пароль успешно изменен'));
                    return $this->refresh();
                }
            }

            $User = User::findById(Yii::$app->user->id);
            if(!$User)
            {
                throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
            }
            $User->scenario = 'admin-update';

            if($User->load(Yii::$app->request->post()))
            {
                if($User->save())
                {
                    // Указываем activity
                    Yii::$app->activity->setMe('changeMyInfo', Yii::t('user', '{master_user} отредактировал(a) данные'));

                    Yii::$app->session->setFlash('success', Yii::t('user', 'Данные изменены'));
                    return $this->refresh();
                }
                else
                {
                    var_dump($User->getErrors());
                }
            }

            $ChangeEmailForm->email = $User->email;

            return $this->render('index', array(
                'isActiveTab'     => $isActiveTab,
                'User'            => $User,
                'ChangeEmailForm' => $ChangeEmailForm,
                'ChangePasswordForm' => $ChangePasswordForm,
            ));
        }
        else
        {
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }
    }

    public function actionAvatarupload()
    {
        if(Yii::$app->user->id) {

            $User = User::findById(Yii::$app->user->id);
            $User->scenario = 'update-avatar';

            if($User->save())
            {
                // Указываем activity
                Yii::$app->activity->setMe('changeMyAvatar', Yii::t('user', '{master_user} загрузил(a) новый аватар'));

                $sizes = Yii::$app->request->post('sizes', false);
                $sizes = explode(",", $sizes);
                foreach($sizes as $size)
                {
                    $_sizes[$size] = $User->avatar->getImage($size);
                }

                return Json::encode(['status'=>'success', 'image'=>$User->avatar->getImage('300x300'), 'sizes'=>$_sizes]);
            }
            else
            {
                return Json::encode(['status'=>'error', 'message'=>Yii::t('user', 'System Error'), 'errors'=>$User->getErrors()]);
            }
        }
        else
        {
            return Json::encode(['status'=>'error']);
        }
    }

    public function actionAvatarremove()
    {
        if(Yii::$app->user->id) {

            $User = User::findById(Yii::$app->user->id);
            $User->scenario = 'update-avatar';

            if($User->removeAvatar())
            {
                // Указываем activity
                Yii::$app->activity->setMe('deleteMyAvatar', Yii::t('user', '{master_user} удалил(a) аватар'));

                $_sizes = array();
                $sizes = Yii::$app->request->post('sizes', array());
                foreach($sizes as $size)
                {
                    $_sizes[$size] = $User->avatar->getImage($size);
                }

                return Json::encode(['status'=>'success', 'image'=>$User->avatar->getImage('300x300'), 'sizes'=>$_sizes]);
            }
            else
            {
                return Json::encode(['status'=>'error', 'message'=>Yii::t('user', 'System Error'), 'errors'=>$User->getErrors()]);
            }
        }
        else
        {
            return Json::encode(['status'=>'error']);
        }
    }
}
