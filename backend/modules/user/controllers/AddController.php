<?php

namespace backend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use common\modules\user\models\User;
use yii\data\Pagination;
use yii\helpers\Url;
use backend\controllers\AbstractBackendController;
use yii\gii\generators\extension;
use yii\helpers\Security;
use yii\helpers\Json;

class AddController extends AbstractBackendController
{
    public function actionIndex()
    {
        $User = new User;
        $User->scenario = 'admin-update';

        if($User->load(Yii::$app->request->post()))
        {
            if($User->save())
            {
                Yii::$app->session->setFlash('success', Yii::t('user', 'Пользователь добавлен'));
                return $this->refresh();
            }
            else
            {
                var_dump($User->getErrors());
            }
        }

        return $this->render('index', array(
            'User'=>$User,
        ));
    }

    public function actionAvatarupload()
    {
        if(Yii::$app->request->get('id', false)) {

            $User = User::findById(Yii::$app->request->get('id', false));
            $User->scenario = 'update-avatar';

            if($User->save())
            {

                $sizes = Yii::$app->request->post('sizes', false);
                $sizes = explode(",", $sizes);
                foreach($sizes as $size)
                {
                    $_sizes[$size] = $User->avatar->getImage($size);
                }

                return Json::encode(['status'=>'success', 'image'=>$User->avatar->getImage('300x300'), 'sizes'=>$_sizes]);
            }
            else
            {
                return Json::encode(['status'=>'error', 'message'=>Yii::t('user', 'System Error'), 'errors'=>$User->getErrors()]);
            }
        }
        else
        {
            return Json::encode(['status'=>'error']);
        }
    }

    public function actionAvatarremove()
    {
        if(Yii::$app->request->get('id', false)) {

            $User = User::findById(Yii::$app->request->get('id', false));
            $User->scenario = 'update-avatar';

            if($User->removeAvatar())
            {
                $sizes = Yii::$app->request->post('sizes', array());
                foreach($sizes as $size)
                {
                    $_sizes[$size] = $User->avatar->getImage($size);
                }

                return Json::encode(['status'=>'success', 'image'=>$User->avatar->getImage('300x300'), 'sizes'=>$_sizes]);
            }
            else
            {
                return Json::encode(['status'=>'error', 'message'=>Yii::t('user', 'System Error'), 'errors'=>$User->getErrors()]);
            }
        }
        else
        {
            return Json::encode(['status'=>'error']);
        }
    }
}
