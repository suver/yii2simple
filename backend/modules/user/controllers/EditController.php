<?php

namespace backend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use common\modules\user\models\User;
use yii\data\Pagination;
use yii\helpers\Url;
use backend\controllers\AbstractBackendController;
use yii\gii\generators\extension;
use yii\helpers\Security;
use yii\helpers\Json;

class EditController extends AbstractBackendController
{
    public function actionIndex()
    {
        if(Yii::$app->request->get('id', false))
        {
            if(Yii::$app->request->get('id', false) == Yii::$app->user->id)
            {
                throw new \yii\web\HttpException(403, 'Вы не можете редактировать свой профиль в этом разделе');
            }

            $User = User::findById(Yii::$app->request->get('id', false));
            if(!$User)
            {
                throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
            }
            $User->scenario = 'admin-update';

            if($User->load(Yii::$app->request->post()))
            {
                if($User->save())
                {
                    // Указываем activity
                    Yii::$app->activity->set('changeUserInfo', Yii::t('user', '{master_user} отредактировал данные пользователя {slave_user}'), [], Yii::$app->user->id, $User->id);

                    Yii::$app->session->setFlash('success', Yii::t('user', 'Данные пользователя изменены'));
                    return $this->refresh();
                }
                else
                {
                    var_dump($User->getErrors());
                }
            }

            return $this->render('index', array(
                'User'=>$User,
            ));
        }
        else
        {
            throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
        }
    }

    public function actionAvatarupload()
    {
        if(Yii::$app->request->get('id', false))
        {
            if(Yii::$app->request->get('id', false) == Yii::$app->user->id)
            {
                throw new \yii\web\HttpException(403, 'Вы не можете редактировать свой профиль в этом разделе');
            }

            $User = User::findById(Yii::$app->request->get('id', false));
            $User->scenario = 'update-avatar';

            if($User->save())
            {
                // Указываем activity
                Yii::$app->activity->set('changeMyAvatar', Yii::t('user', '{master_user} изменил аватар пользователя {slave_user}'), [], Yii::$app->user->id, $User->id);

                $sizes = Yii::$app->request->post('sizes', false);
                $sizes = explode(",", $sizes);
                foreach($sizes as $size)
                {
                    $_sizes[$size] = $User->avatar->getImage($size);
                }

                return Json::encode(['status'=>'success', 'image'=>$User->avatar->getImage('300x300'), 'sizes'=>$_sizes]);
            }
            else
            {
                return Json::encode(['status'=>'error', 'message'=>Yii::t('user', 'System Error'), 'errors'=>$User->getErrors()]);
            }
        }
        else
        {
            return Json::encode(['status'=>'error']);
        }
    }

    public function actionAvatarremove()
    {
        if(Yii::$app->request->get('id', false))
        {
            if(Yii::$app->request->get('id', false) == Yii::$app->user->id)
            {
                throw new \yii\web\HttpException(403, 'Вы не можете редактировать свой профиль в этом разделе');
            }

            $User = User::findById(Yii::$app->request->get('id', false));
            $User->scenario = 'update-avatar';

            if($User->removeAvatar())
            {
                // Указываем activity
                Yii::$app->activity->set('deleteMyAvatar', Yii::t('user', '{master_user} удалил(a) аватар пользователя {slave_user}'), [], Yii::$app->user->id, $User->id);

                $_sizes = array();
                $sizes = Yii::$app->request->post('sizes', array());
                foreach($sizes as $size)
                {
                    $_sizes[$size] = $User->avatar->getImage($size);
                }

                return Json::encode(['status'=>'success', 'image'=>$User->avatar->getImage('300x300'), 'sizes'=>$_sizes]);
            }
            else
            {
                return Json::encode(['status'=>'error', 'message'=>Yii::t('user', 'System Error'), 'errors'=>$User->getErrors()]);
            }
        }
        else
        {
            return Json::encode(['status'=>'error']);
        }
    }
}
