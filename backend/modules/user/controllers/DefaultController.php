<?php

namespace backend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use backend\controllers\AbstractBackendController;
use yii\filters\AccessControl;
use common\modules\user\models\LoginForm;
use yii\helpers\Url;
use common\modules\user\models\User;

class DefaultController extends AbstractBackendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true, // Do not have access
                        'actions' => ['login','recovery','signup','resendactivatekey','activate'],
                        'roles'=>['?'], // Guests '?'
                    ],
                    [
                        'allow' => true, // Do not have access
                        'actions' => ['changemail'],
                        'roles'=>['?','@'], // Guests '?'
                    ],
                    [
                        'allow' => true, // Do not have access
                        'actions' => ['logout'],
                        'roles'=>['@'], // Guests '?'
                    ],
                ]
            ]
        ];
    }

    public $layout = '@backend/views/layouts/login';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        //Yii::$app->session->setFlash('success', Yii::t('user', 'Ссылка для восстановления пароля, была отправлена на указанный вами электронный адрес.'));
        if (!\Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            return $this->goBack();
        }
        else
        {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $User = new User();

        $User->scenario = 'register';

        if($User->load(Yii::$app->request->post()))
        {
            $User->on(User::EVENT_AFTER_REGISTER_SUCCESS, function($event){
                $isSuccess = Yii::$app->sender->email([
                    'to' => $event->sender,
                    'subject' => Yii::t("user", "Вы зарегистрировались."),
                    'message' => Yii::t("user", "Для активации акаунта перейдите по ссылке: {link}", [
                            'link'=>Url::toRoute(['/user/default/activate', 'key'=>$event->sender->auth_key], true)
                        ]),
                ]);
                if($isSuccess)
                {
                    // Указываем activity
                    Yii::$app->user->activity(\common\modules\user\models\Activity::ICON_GLYPHICON_PLUS, Yii::t('user', 'Пользователь зарегистрирован'));
                    Yii::$app->session->setFlash('info', [Yii::t('user', 'Пользователь зарегистрирован'),Yii::t('user', 'Для активации пользователя необходимо подтвердить email. Вам наравлено письмо с дополнительными инсрукциями.')]);
                }
                else
                {
                    Yii::$app->session->setFlash('error', [Yii::t('user','Внимание!'),Yii::t('user','Не удалось отправить письмо с сслыкой для активации. Свяжитесь с администратором или попробуйте позже.')]);
                }
            });

            if($User->save())
            {
                return $this->refresh();
            }
            else
            {
                var_dump($User->getErrors());
            }
        }

        return $this->render('signup', [
            'User' => $User,
        ]);
    }

    public function actionResendactivatekey()
    {
        if(Yii::$app->request->isPost)
        {
            $post = Yii::$app->request->post('User', false);
            if($post && $User = User::find()->where("email=:email", [":email"=>$post['email']])->one())
            {
                if($User->status != User::STATUS_INACTIVE)
                {
                    Yii::$app->session->setFlash('error', [Yii::t('user','Внимание!'),Yii::t('user','Вы уже авктивировали свой акаунт или пользователь удален. Свяжитесь с администратором сайта.')]);
                    return $this->redirect(['/user/default/login']);
                }
                $User->scenario = 'activate';
                $User->createNewAuthKey();
                $isSuccess = Yii::$app->sender->email([
                    'to' => $User,
                    'subject' => Yii::t("user", "Новый ключь активации"),
                    'message' => Yii::t("user", "Перейдите по ссылке для активации {link}", [
                            "link" => Url::toRoute(['/user/default/activate', 'key'=>$User->auth_key], true)
                        ]),
                ]);
                if($isSuccess)
                {
                    // Указываем activity
                    Yii::$app->user->activity(\common\modules\user\models\Activity::ICON_GLYPHICON_PLUS, Yii::t('user', 'Для пользователя сгененрирован новый ключь активации'));
                    Yii::$app->session->setFlash('info', [Yii::t('user', 'Новый ключ активации отправлен на ваш почтовый ящик'),Yii::t('user', 'Для активации акаунта воспользуйтесь инструкциями отправленными вам на почту')]);
                }
                else
                {
                    Yii::$app->session->setFlash('error', [Yii::t('user','Внимание!'),Yii::t('user','Не удалось активировать пользоватля. Возможно ключь активации устарел. Попробуйте позже, обратитесь к администратору сайта или попробуйте перезапросить активационый ключь.')]);
                }
                return $this->refresh();
            }
        }

        $User = new User();
        $User->scenario = 'activate';
        return $this->render('ResendActivateKey', [
            'User' => $User,
        ]);
    }

    public function actionActivate($key)
    {
        if($key && $User = User::find()->where("auth_key=:auth_key",[":auth_key"=>$key])->one())
        {
            $User->scenario = 'activate';
            $User->on(User::EVENT_AFTER_ACTIVATE_SUCCESS, function($event){
                $isSuccess = Yii::$app->sender->email([
                    'to' => $event->sender,
                    'subject' => Yii::t("user", "Вы зарегистрировались."),
                    'message' => Yii::t("user", "Спасибо что вы с нами!"),
                ]);
            });

            if($User->activate())
            {
                // Указываем activity
                Yii::$app->user->activity(\common\modules\user\models\Activity::ICON_GLYPHICON_PLUS, Yii::t('user', 'Пользователь подтвердил свою почту'));
                Yii::$app->session->setFlash('info', [Yii::t('user', 'Вы завершили регистрацию'),Yii::t('user', 'Можете авторизоватся и пользоватся сайтом.')]);
            }
            else
            {
                Yii::$app->session->setFlash('error', [Yii::t('user','Внимание!'),Yii::t('user','Не удалось активировать пользоватля. Возможно ключь активации устарел. Попробуйте позже, обратитесь к администратору сайта или попробуйте перезапросить активационый ключь.')]);
            }
        }
        $this->redirect(['/user/default/login']);
    }

    /**
     * Восстановливаем пароль.
     * @param string $email E-mail адрес для которого нужно восстановить пароль.
     * @param string $key Ключь подтверждения.
     */
    public function actionRecovery($email = false, $key = false)
    {
        // В случае когда $email и $key заданы, прорабатывается сценарий подтверждения восстановления пароля.
        if ($email && $key)
        {
            $ResetPasswordForm = new \common\modules\user\models\ResetPasswordForm($email, $key);
            if(Yii::$app->request->isPost)
            {
                if($ResetPasswordForm->isCurrentPasrams())
                {
                    // Добавляем обработчик события который отправляет сообщение с новым паролем на e-mail адрес пользователя.
                    $ResetPasswordForm->on(\common\modules\user\models\ResetPasswordForm::EVENT_RESET_PASSWORD_SUCCESS, function($event){
                        $isSuccess = Yii::$app->sender->email([
                            'to' => $event->sender,
                            'subject' => Yii::t("user", "Recovery password: You new password"),
                            'message' => Yii::t("user", "You new password: {password}", array('password'=>$event->sender->password)),
                        ]);
                        if($isSuccess)
                        {
                            Yii::$app->session->setFlash('success', [Yii::t('user','Поздравляем!'),Yii::t('user','На ваш почтовый ящик отправленно письмо с вашим новым паролем. Спасибо что остаетесь с нами!')]);
                        }
                        else
                        {
                            Yii::$app->session->setFlash('error', [Yii::t('user','Внимание!'),Yii::t('user','Не удалось отправить письмо с новым паролем. Свяжитесь с администратором или попробуйте позже.')]);
                        }
                        // Указываем activity
                        Yii::$app->user->activity(\common\modules\user\models\Activity::ICON_GLYPHICON_USER, Yii::t('user', 'Пользователь изменил пароль по процедуре востановления пароля'));
                    });

                    // Проверяем сущесвтования пользователя с переданым e-mail адресом, и ключом восстановления.
                    if ($ResetPasswordForm->load(Yii::$app->request->post()) && $ResetPasswordForm->changePassword())
                    {
                        // В случае успешного восстановления пароля, перенаправляем пользователя на главную страницу, и оповещаем пользователя об успешном завершении процесса восстановления.
                        //Yii::$app->session->setFlash('success', Yii::t('user', 'Пароль был успешно восстановлен и отправлен на указанный электронный адрес. Проверьте пожалуйста почту!'));
                        // Перенаправляем пользователя на главную страницу сайта.
                        return $this->redirect(['/user/default/login']);
                    } else {
                        // В случае когда пользователь с передаными аргументами не существует в базе данных, оповещаем пользователя об ошибке.
                        Yii::$app->session->setFlash('error', Yii::t('user', 'Неправильный запрос подтверждения смены пароля. Пожалуйста попробуйте ещё раз!'));
                    }
                    return $this->refresh();
                }
                else
                {
                    Yii::$app->session->setFlash('error', Yii::t('user', 'Некорретная или устаревшая ссылка востановления пароля!'));
                    return $this->redirect(['/user/default/login']);
                }
            }
            else
            {
                return $this->render('recoveryPassword', [
                    'ResetPasswordForm' => $ResetPasswordForm,
                ]);
            }

            // В случае когда $email и $key не заданы, прорабатывается сценарий непосредственного запроса восстановления пароля.
        } else {
            $model = new User(['scenario' => 'recovery']);
            // Добавляем обработчик события который отправляет сообщение с ключом подтверждения смены пароля на e-mail адрес пользователя.
            $model->on(User::EVENT_AFTER_VALIDATE_SUCCESS, function($event){
                $isSuccess = Yii::$app->sender->email([
                    'to' => $event->sender,
                    'subject' => Yii::t("user", "Recovery password: You recovery password link"),
                    'message' => Yii::t("user", "You recovery password link: {link}", [
                            'link'=>Url::toRoute(['/user/default/recovery', 'email'=>$event->sender->email,'key'=>$event->sender->auth_key], true)
                        ]),
                ]);
                if($isSuccess)
                {
                    Yii::$app->session->setFlash('info', [Yii::t('user','Информация!'),Yii::t('user','На ваш почтовый ящик отправленно письмо с инструкциями по востановлению пароля.')]);
                }
                else
                {
                    Yii::$app->session->setFlash('error', [Yii::t('user','Внимание!'),Yii::t('user','Не удалось отправить письмо с сслыкой для востановления. Свяжитесь с администратором или попробуйте позже.')]);
                }
                // Указываем activity
                Yii::$app->user->activity(\common\modules\user\models\Activity::ICON_GLYPHICON_EDIT, Yii::t('user', 'Пользователь запросил востановление пароля'));
            });

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                // Перенаправляем пользователя на главную страницу, и оповещаем его об успешном завершении запроса восставновления пароля.
                //Yii::$app->session->setFlash('success', Yii::t('user', 'Ссылка для восстановления пароля, была отправлена на указанный вами электронный адрес.'));
                return $this->redirect(['/user/default/login']);
            }
            // Рендерим представление.
            return $this->render('recovery', [
                'model' => $model
            ]);
        }
    }

    /**
     *
     */
    public function actionChangemail()
    {
        $ChangeEmailForm = new \common\modules\user\models\ChangeEmailForm();
        $ChangeEmailForm->scenario = 'confirm';
        $ChangeEmailForm->key = Yii::$app->request->get('key');
        $ChangeEmailForm->user = Yii::$app->request->get('user');
        if($ChangeEmailForm->changeUserEmail())
        {
            Yii::$app->activity->set('changeMyEmail', Yii::t('user', '{master_user} сменил(a) email', [], Yii::$app->request->get('user')));

            Yii::$app->session->setFlash('success', Yii::t('user', 'Вы успешно сменили свой email Email'));
        }
        else {
            $errors = $ChangeEmailForm->getFirstErrors();
            if(empty($errors))
            {
                Yii::$app->session->setFlash('error', implode("; ", $errors));
            }
            else {
                Yii::$app->session->setFlash('error', Yii::t('user',"Не удалось изменить email"));
            }
        }


        if(Yii::$app->user->isGuest)
        {
            return $this->redirect('/');
        }
        else
        {
            return $this->redirect('/user/profile/me');
        }
    }
}
