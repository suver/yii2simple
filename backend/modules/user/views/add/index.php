<?php
use yii\helpers\Html;
use suver\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\FileInput;
use common\modules\user\models\User;


$this->title = Yii::t('user', 'Создание пользователя');

$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('user', 'Создание пользователя')], 'meta-description');

// http://antelle.github.io/passfield/api.html
$this->registerJsFile('/js/passfield/js/passfield.min.js');
$this->registerCssFile('/js/passfield/css/passfield.min.css');
$this->registerJs('$("#user-password").passField({ /*options*/ });');


$this->params['breadcrumbs'] = [
    ['label' => Yii::t('user', 'Пользователи'), 'url' => !empty(Url::previous()) ? [Url::previous()] : ['/user/list/index']],
    Yii::t('user', 'Создание пользователя'),
];

?>


<script type="text/javascript">
$(document).ready(function(){
    $("[data-change-avatar]").click(function(){
        $("[data-field-change-avatar]").click();
    });
});
</script>

<!-- Profile information -->
<?php $form = ActiveForm::begin(array(
    'options' => [
        'class' => 'form-horizontal',
        'role' => 'form',
        'enctype' => 'multipart/form-data',
    ],
)); ?>
<!-- Profile grid -->
<div class="row">
    <div class="col-lg-2">
        <!-- Profile links -->
        <div class="block">
            <div class="block">
                <div class="thumbnail">

                    <div class="thumb">
                        <img src="<?php echo $User->avatar->getImage('300x300') ?>" data-avatar-image="300x300">
                    </div>
                    <div class="caption text-center">
                        <h6>
                            <?php echo $User->name?> <?php echo $User->surname?>
                            <small><?php echo $User->username ?></small>
                        </h6>
                    </div>
                </div>
            </div>
            <ul class="nav nav-list">
                <li class="nav-header"><?php echo Yii::t('user', 'Профиль') ?> <i class="icon-accessibility"></i></li>
                <li><?php echo Html::activeDropDownList($User, 'role', User::getRoles(), array('class' => 'form-control')); ?></li>
                <li><?php echo Html::activeDropDownList($User, 'status', User::getStatuses(), array('class' => 'form-control')); ?></li>
            </ul>
        </div>
        <!-- /profile links -->
    </div>
    <div class="col-lg-10">
        <!-- Page tabs -->
        <div class="tabbable page-tabs">

            <div class="tab-content">
                <!-- Settings tab -->
                <div class="tab-pane active fade in" id="settings">

                        <h6 class="heading-hr"><i class="icon-user"></i> <?php echo Yii::t('user', 'Данные пользователя') ?>:</h6>

                        <div class="block-inner">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo $form->field($User, 'username')->textInput(array('class' => 'form-control')); ?>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo $form->field($User, 'name')->textInput(array('class' => 'form-control')); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $form->field($User, 'surname')->textInput(array('class' => 'form-control')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h6 class="heading-hr"><i class="icon-lock"></i> <?php echo Yii::t('user', 'Настройки безопасности') ?>:</h6>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $form->field($User, 'email')->textInput(array('class' => 'form-control')); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo $form->field($User, 'password')->passwordInput() ?>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <?php echo Html::resetButton(Yii::t('app', 'Сбросить'), array('class' => 'btn btn-default')); ?>
                            <?php echo Html::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn btn-success')); ?>
                        </div>

                    <!-- /profile information -->
                </div>
                <!-- /Settings tab -->
        </div>
    </div>
    <!-- /page tabs -->
</div>
</div>
<!-- /profile grid -->
<?php ActiveForm::end(); ?>