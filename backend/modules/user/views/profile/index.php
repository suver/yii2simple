<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\FileInput;

$this->registerJsFile('/js/uploadavatar.js');

$this->title = Yii::t('user', 'Редактирование профиля');

$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('user', 'Редактирование профиля')], 'meta-description');

$this->params['breadcrumbs'] = [
    Yii::t('user', 'Редактирование профиля'),
];

$this->sidebarLite = true;

?>


<script type="text/javascript">
$(document).ready(function(){
    $("[data-change-avatar]").click(function(){
        $("[data-field-change-avatar]").click();
    });
});
</script>

<!-- Profile grid -->
<div class="row">
    <div class="col-lg-2">
        <!-- Profile links -->
        <div class="block">
            <div class="block">
                <div class="thumbnail">

                    <div class="thumb">
                        <input type="hidden" name="<?php echo Yii::$app->request->csrfParam ?>" value="<?php echo Yii::$app->request->getCsrfToken() ?>" data-csr-tocken="me">
                        <img src="<?php echo $User->avatar->getImage('300x300') ?>" data-avatar-size="300x300" data-avatar-user="me">
                        <div class="thumb-loader-progress-container" data-avatar-loader-progress>
                            <div class="progress progress-container progress-striped active" data-avatar-progress>
                                <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;"><span class="sr-only">85%</span></div>
                            </div>
                        </div>
                        <div class="thumb-options">
                            <span>
                                <a href="#" class="btn btn-icon btn-success" data-change-me-avatar="me"><i class="icon-pencil"></i></a>
                                <a href="#" class="btn btn-icon btn-success" data-remove-me-avatar="me"><i class="icon-remove"></i></a>
                            </span>
                        </div>
                    </div>
                    <div class="caption text-center">
                        <h6>
                            <?php echo $User->name?> <?php echo $User->surname?>
                            <small><?php echo $User->username ?></small>
                        </h6>
                    </div>
                </div>
            </div>

            <ul class="nav nav-list">
                <li class="nav-header"><?php echo Yii::t('user', 'Профиль') ?> <i class="icon-accessibility"></i></li>
                <li><a href="#"><?php echo Yii::t('user', 'Аналитика') ?></a></li>
            </ul>

        </div>
        <!-- /profile links -->
    </div>
    <div class="col-lg-10">
        <!-- Page tabs -->
        <div class="tabbable page-tabs">
            <ul class="nav nav-pills nav-justified">
                <li<?php echo $isActiveTab == 'profile-activity' ? ' class="active"' : '' ?>>
                    <a href="#profile-activity" data-toggle="tab"><i class="icon-bubbles3"></i> <?php echo Yii::t('user', 'Активность') ?> <span class="label label-danger"><?php echo Yii::$app->activity->getForUser(Yii::$app->user->id)->count() ?></span></a>
                </li>
                <li<?php echo $isActiveTab == 'profile-messages' ? ' class="active"' : '' ?>>
                    <a href="#profile-messages" data-toggle="tab"><i class="icon-bubbles3"></i> <?php echo Yii::t('user', 'Сообщения') ?> <span class="label label-danger">12</span></a>
                </li>
                <li<?php echo $isActiveTab == 'profile-profile' ? ' class="active"' : '' ?>>
                    <a href="#profile-profile" data-toggle="tab"><i class="icon-user3"></i> <?php echo Yii::t('user', 'Настройки') ?></a>
                </li>
                <li<?php echo $isActiveTab == 'profile-security' ? ' class="active"' : '' ?>>
                    <a href="#profile-security" data-toggle="tab"><i class="icon-key"></i> <?php echo Yii::t('user', 'Настройки безопасности') ?></a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- Activity tab -->
                <div class="tab-pane fade <?php echo $isActiveTab == 'profile-activity' ? 'active' : '' ?>" id="profile-activity">
                    <?php echo $this->render('_profile_activity', ['User'=>$User]); ?>
                </div>
                <!-- /Activity tab -->

                <!-- Messages tab -->
                <div class="tab-pane fade <?php echo $isActiveTab == 'profile-messages' ? 'active' : '' ?>" id="profile-messages">
                    <?php echo $this->render('_profile_messages', ['User'=>$User]); ?>
                </div>
                <!-- /Messages tab -->

                <!-- Profile tab -->
                <div class="tab-pane fade in <?php echo $isActiveTab == 'profile-profile' ? 'active' : '' ?>" id="profile-profile">
                    <?php echo $this->render('_profile_profile', ['User'=>$User]); ?>
                </div>
                <!-- /Settings tab -->

                <!-- Security tab -->
                <div class="tab-pane fade in <?php echo $isActiveTab == 'profile-security' ? 'active' : '' ?>" id="profile-security">
                    <?php echo $this->render('_profile_security', ['User'=>$User, 'ChangeEmailForm'=>$ChangeEmailForm, 'ChangePasswordForm'=>$ChangePasswordForm]); ?>
                </div>
                <!-- /Security tab -->
        </div>
    </div>
    <!-- /page tabs -->
</div>
</div>
<!-- /profile grid -->
