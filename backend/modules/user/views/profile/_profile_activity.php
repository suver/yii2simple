<div class="block activity-list">
    <h6 class="heading-hr"><i class="icon-people"></i> <?php echo Yii::t('user','Активность пользователя') ?></h6>
    <ul class="media-list">
        <?php foreach(Yii::$app->activity->getForUser(Yii::$app->user->id)->limit(10)->orderBy(['id'=>SORT_DESC])->all() as $activity) { ?>
            <li class="media">
                <span class="pull-left glyphicon <?php echo $activity->getIcon() ?> text-info"></span>
                <div class="media-body">
                    <div class="clearfix"><span class="media-heading"><?php echo $activity->getMessage() ?></span><span class="media-notice"><?php echo $activity->time->format('d M Y H:i') ?></span></div>
                    <?php echo $activity->time->ago() ?>
                </div>
            </li>
        <?php } ?>
    </ul>
</div>