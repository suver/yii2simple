<?php
use yii\helpers\Html;
use suver\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\FileInput;

?>

<!-- Profile information -->
<h6 class="heading-hr"><i class="icon-user"></i> <?php echo Yii::t('user', 'Сменить Email') ?>:</h6>
<?php $form = ActiveForm::begin(array(
    'options' => [
        'class' => 'form-horizontal',
        'role' => 'form',
        'enctype' => 'multipart/form-data',
    ],
)); ?>
<div class="block-inner">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <?php echo $form->field($ChangeEmailForm, 'password')->passwordInput() ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->field($ChangeEmailForm, 'email')->textInput(array('class' => 'form-control')); ?>
            </div>
        </div>
    </div>
</div>
<div class="text-right">
    <?php echo Html::resetButton(Yii::t('app', 'Сбросить'), array('class' => 'btn btn-default')); ?>
    <?php echo Html::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn btn-success')); ?>
</div>
<?php ActiveForm::end(); ?>


<h6 class="heading-hr"><i class="icon-user"></i> <?php echo Yii::t('user', 'Сменить пароль') ?>:</h6>
<?php $form = ActiveForm::begin(array(
    'options' => [
        'class' => 'form-horizontal',
        'role' => 'form',
        'enctype' => 'multipart/form-data',
    ],
)); ?>
<div class="block-inner">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <?php echo $form->field($ChangePasswordForm, 'password')->passwordInput() ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->field($ChangePasswordForm, 'new_password')->passwordInput(['helper'=>true]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <?php echo $form->field($ChangePasswordForm, 'new_repassword')->passwordInput() ?>
            </div>
        </div>
    </div>
</div>

<div class="text-right">
    <?php echo Html::resetButton(Yii::t('app', 'Сбросить'), array('class' => 'btn btn-default')); ?>
    <?php echo Html::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn btn-success')); ?>
</div>
<?php ActiveForm::end(); ?>
<!-- /profile information -->