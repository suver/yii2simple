<?php
use yii\helpers\Html;
use suver\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\FileInput;

?>

<!-- Profile information -->
<?php $form = ActiveForm::begin(array(
    'options' => [
        'class' => 'form-horizontal',
        'role' => 'form',
        'enctype' => 'multipart/form-data',
    ],
)); ?>
<h6 class="heading-hr"><i class="icon-user"></i> <?php echo Yii::t('user', 'Данные пользователя') ?>:</h6>

<div class="block-inner">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <?php echo $form->field($User, 'username')->textInput(array('class' => 'form-control')); ?>
            </div>
            <div class="col-md-6">

            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <?php echo $form->field($User, 'name')->textInput(array('class' => 'form-control')); ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->field($User, 'surname')->textInput(array('class' => 'form-control')); ?>
            </div>
        </div>
    </div>
</div>

<h6 class="heading-hr"><i class="icon-lock"></i> <?php echo Yii::t('user', 'Настройки безопасности') ?>:</h6>

<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($User, 'email')->textInput(array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->field($User, 'password')->passwordInput(['helper'=>true]) ?>
        </div>
    </div>
</div>

<h6 class="heading-hr"><i class="icon-watch"></i> <?php echo Yii::t('user', 'Время') ?>:</h6>

<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-2 control-label text-right"><?php echo Yii::t('user', 'Создан') ?>:</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $User->create_time->format('d.m.Y H:i'); ?> (<?php echo $User->create_time->ago(); ?>)</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label text-right"><?php echo Yii::t('user', 'Обновлен') ?>:</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $User->update_time->format('d.m.Y H:i'); ?> (<?php echo $User->update_time->ago(); ?>)</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>
</div>

<div class="text-right">
    <?php echo Html::resetButton(Yii::t('app', 'Сбросить'), array('class' => 'btn btn-default')); ?>
    <?php echo Html::submitButton(Yii::t('app', 'Сохранить'), array('class' => 'btn btn-success')); ?>
</div>
<?php ActiveForm::end(); ?>
<!-- /profile information -->