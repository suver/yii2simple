<?php
use backend\widgets\BGridView;
use backend\widgets\BLinkPager;
use yii\helpers\Url;
use common\modules\user\models\User;

//$this->title = [Yii::t('user','Список пользователей'), Yii::t('user','в виде Table Grid')];

$this->title = Yii::t('user','Список пользователей');
$this->pageInfo = Yii::t('user','в виде Table Grid');

$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('user','Список пользователей')], 'meta-description');

$this->params['breadcrumbs'] = [
    Yii::t('user','Список пользователей'),
];

Url::remember();
?>


<!-- Page tabs -->
<div class="tabbable page-tabs">
    <ul class="nav nav-tabs">
        <li><a href="<?php echo Url::toRoute('grid')?>"><i class="icon-checkbox-unchecked"></i> <?php echo Yii::t('user','Список пользователей') ?></a></li>
        <li class="active"><a><i class="icon-list2"></i> <?php echo Yii::t('user','Таблица пользователей') ?></a></li>
    </ul>
    <div class="content">
        <!-- Third tab -->
        <div class="tab-pane">
            <!-- Table view -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title"><i class="icon-people"></i> <?php echo Yii::t('user','Пользователи') ?></h5>
                    <span class="label label-danger pull-right">+<?php echo User::getCount() ?></span> </div>
                <div class="datatable-media">
                    <?php
                    \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
                    echo BGridView::widget([
                        'dataProvider' => $dataProvider,
                        'name' => 'user-grid',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'label' => Yii::t('user', 'Avatar'),
                                'class' => 'yii\grid\DataColumn',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return '<img src="' . $data->avatar->getImage('50x50') . '" ' . (($data->id == Yii::$app->user->identity->id) ? ' data-avatar-size="50x50"' : '') . ' data-avatar-user="' . $data->id . '">';
                                },
                            ],
                            'id',
                            'username',
                            'email',
                            [
                                'label' => Yii::t('user', 'Full Name'),
                                'class' => 'yii\grid\DataColumn',
                                'value' => function ($data) {
                                    return $data->name . ' ' . $data->surname;
                                },
                            ],
                            [
                                'label' => Yii::t('user', 'Role'),
                                'class' => 'yii\grid\DataColumn',
                                'value' => function ($data) {
                                    return $data->getRole();
                                },
                            ],
                            [
                                'label' => Yii::t('user', 'Status'),
                                'class' => 'yii\grid\DataColumn',
                                'value' => function ($data) {
                                    return $data->getStatus();
                                },
                            ],
                            [
                                'label' => Yii::t('user', 'Create Time'),
                                'class' => 'yii\grid\DataColumn',
                                'format' => ['date', 'd.m.Y H:m'],
                                'attribute' => 'create_time',
                            ],
                            [
                                'label' => Yii::t('user', 'Update Time'),
                                'class' => 'yii\grid\DataColumn',
                                'format' => ['date', 'd.m.Y H:m'],
                                'attribute' => 'update_time',
                            ],
                            [
                                'label' => '',
                                'class' => 'yii\grid\DataColumn',
                                'format' => 'html',
                                'value' => function ($data) {
                                    $edit = '<a class="btn btn-default" href="' . Url::toRoute(['/user/edit/index', 'id' => $data->id]) . '">' . Yii::t('user', 'Ред.') . '</a>';
                                    return $edit;
                                },
                            ],
                        ],
                    ]);
                    \yii\widgets\Pjax::end();
                    ?>
                </div>
            </div>
            <!-- /table view -->
        </div>
        <!-- /third tab -->
    </div>
</div>
<!-- page tabs -->