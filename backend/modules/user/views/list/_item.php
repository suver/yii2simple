<?php

use yii\helpers\Url;
?>
<div class="block user-grid-block col-lg-3 col-md-6 col-sm-6">
    <div class="thumbnail">
        <a href="<?php echo $model->avatar->getImage("500x500") ?>" class="thumb-zoom lightbox" title="<?php echo $model->name . " " . $model->surname ?>">
            <img src="<?php echo $model->avatar->getImage("300x300", 'crop') ?>" <?php echo ($model->id == Yii::$app->user->identity->id) ? 'data-avatar-size="200x200"' : '' ?>  data-avatar-user="<?php echo $model->id ?>">
        </a>
        <div class="caption text-center">
            <h6><a href="<?php echo Url::toRoute(['/user/edit/index', 'id' => $model->id]) ?>"><?php echo $model->name . " " . $model->surname ?> <small><?php echo $model->username ?></small></a></h6>
            <!--div class="icons-group">
                <a href="#" title="Google Drive" class="tip"><i class="icon-google-drive"></i></a>
                <a href="#" title="Twitter" class="tip"><i class="icon-twitter"></i></a>
                <a href="#" title="Github" class="tip"><i class="icon-github3"></i></a>
            </div-->
        </div>
    </div>
</div>