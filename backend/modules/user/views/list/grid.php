<?php
use backend\widgets\BGridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;

//$this->title = [Yii::t('user','Список пользователей'), Yii::t('user','в виде Data Grid')];

$this->title = Yii::t('user','Список пользователей');
$this->pageInfo = Yii::t('user','в виде Data Grid');

$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('user','Список пользователей')], 'meta-description');

$this->registerJsFile('/js/plugins/interface/fancybox.min.js', ['depends' => [yii\web\JqueryAsset::className()], 'position' => \yii\web\View::POS_HEAD]);

$this->params['breadcrumbs'] = [
    Yii::t('user','Список пользователей'),
];

Url::remember();
?>


<!-- Page tabs -->
<div class="tabbable page-tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a><i class="icon-checkbox-unchecked"></i> <?php echo Yii::t('user','Список пользователей') ?></a></li>
        <li><a href="<?php echo Url::toRoute('index')?>"><i class="icon-list2"></i><?php echo Yii::t('user','Таблица пользователей') ?></a></li>
    </ul>
    <div class="content">
        <!-- First tab -->
        <div class="tab-pane">
            <div class="tab-pane">
                <?php
                \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
                echo backend\widgets\BListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => '_item',
                    'layout' => "{sorter}\n{summary}\n{items}\n{pager}",
                    //'pager' => ['class' => \kop\y2sp\ScrollPager::className()]
                    'pager' => ['class' => LinkPager::className()],
                    'viewParams' => array(
                        'istrue' => true,
                    ),
                ]);
                \yii\widgets\Pjax::end();
                ?>
            </div>
        </div>
        <!-- /first tab -->
    </div>
</div>
<!-- page tabs -->




