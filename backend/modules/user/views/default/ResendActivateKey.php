<?php
/**
 * Страница повторной отправки подвтерждения новому пользовтелю.
 * @var yii\base\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\modules\users\models\User $model
 */
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Перевыпустить активационный ключь');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Login wrapper -->
<div class="resend-wrapper">
    <?php $form = ActiveForm::begin(['id' => 'resend-form']); ?>

    <div class="popup-header">
        <span class="text-semibold"><?php echo Yii::t('user', 'Перевыпустить активационный ключь')?></span>
    </div>

    <div class="well">
        <div class="form-group has-feedback">
            <?php echo $form->field($User, 'email') ?>
            <i class="icon-users form-control-feedback"></i></div>
        <div class="row form-actions">
            <div class="col-xs-12">
                <?= Html::submitButton(Yii::t('user', '<i class="icon-menu2"></i> Перевыпустить активационный ключь'), ['class' => 'btn btn-warning pull-right', 'name' => 'login-button']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <p><a href="<?php echo Url::toRoute('/user/default/login') ?>"><i class="icon-info"></i> <?php echo Yii::t('user', 'Вход')?></a></p>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>