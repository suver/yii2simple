<?php
/**
 * Страница повторной отправки подвтерждения новому пользовтелю.
 * @var yii\base\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\modules\users\models\User $model
 */
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Восстановление пароля');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Login wrapper -->
<div class="recovery-wrapper">
    <?php $form = ActiveForm::begin(['id' => 'recoveryPassword-form']); ?>

    <div class="popup-header">
        <span class="text-semibold"><?php echo Yii::t('user', 'Смена пароля пользоватлея')?></span>
    </div>

    <div class="well">
        <div class="form-group has-feedback">
            <?php echo $form->field($ResetPasswordForm, 'password')->passwordInput() ?>
            <i class="icon-users form-control-feedback"></i>
        </div>
        <div class="form-group has-feedback">
            <?php echo $form->field($ResetPasswordForm, 'repassword')->passwordInput() ?>
            <i class="icon-users form-control-feedback"></i>
        </div>
        <div class="row form-actions">
            <div class="col-xs-9">
                <?= Html::submitButton(Yii::t('user', '<i class="icon-menu2"></i> Изменит пароль'), ['class' => 'btn btn-warning pull-right', 'name' => 'login-button']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>