<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\LoginForm $model
 */
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Login wrapper -->
<div class="login-wrapper">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <div class="popup-header">
           <span class="text-semibold"><?php echo Yii::t('user', 'Форма входа')?></span>
        </div>

        <div class="well">
            <div class="form-group has-feedback">
                <?php echo $form->field($model, 'email') ?>
                <i class="icon-users form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback">
                <?php echo $form->field($model, 'password')->passwordInput() ?>
                <i class="icon-lock form-control-feedback"></i>
            </div>
            <div class="row form-actions">
                <div class="col-xs-6">
                    <div class="checkbox checkbox-success">
                        <?= $form->field($model, 'rememberMe')->checkbox() ?>
                    </div>
                </div>
                <div class="col-xs-6">
                    <?php echo Html::submitButton(Yii::t('user', '<i class="icon-menu2"></i> Войти'), ['class' => 'btn btn-warning pull-right', 'name' => 'login-button']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p><a href="<?php echo Url::toRoute('/user/default/recovery') ?>"><i class="icon-info"></i> <?php echo Yii::t('user', 'Забыли пароль?')?></a></p>
                    <p><a href="<?php echo Url::toRoute('/user/default/signup') ?>"><i class="icon-info"></i> <?php echo Yii::t('user', 'Зарегистрироваться')?></a></p>
                </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>

