<?php
/**
 * Страница повторной отправки подвтерждения новому пользовтелю.
 * @var yii\base\View $this
 * @var yii\widgets\ActiveForm $form
 * @var common\modules\users\models\User $model
 */
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('user', 'Восстановление пароля');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Login wrapper -->
<div class="recovery-wrapper">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

    <div class="popup-header">
        <span class="text-semibold"><?php echo Yii::t('user', 'Форма запроса на востаовление пароля')?></span>
    </div>

    <div class="well">
        <div class="form-group has-feedback">
            <?= $form->field($model, 'email') ?>
            <i class="icon-users form-control-feedback"></i></div>
        <div class="row form-actions">
            <div class="col-xs-6">
                <a href="<?php echo Url::toRoute('/user/default/login') ?>"><i class="fa fa-sign-in"></i> <?php echo Yii::t('user', 'Авторизация')?></a>
            </div>
            <div class="col-xs-6">
                <?= Html::submitButton(Yii::t('user', '<i class="icon-menu2"></i> Востановить'), ['class' => 'btn btn-warning pull-right', 'name' => 'login-button']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>