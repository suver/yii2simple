<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\user\models\User;
use yii\helpers\Url;


$this->title = Yii::t('user', 'Регистрация пользователя');

$this->registerMetaTag(['name' => 'description', 'content' => 'Редактирование пользователя'], 'meta-description');

$this->params['breadcrumbs'] = [
    ['label' => Yii::t('user', 'Users'), 'url' => !empty(Url::previous()) ? [Url::previous()] : ['/user/list/index']],
    'User Add',
];

$this->sidebarLite = true;

?>

<!-- Login wrapper -->
<div class="signup-wrapper">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

    <div class="popup-header">
        <span class="text-semibold"><?php echo Yii::t('user', 'Регистрация пользователя')?></span>
    </div>

    <div class="well">
        <?php echo $form->field($User, 'email') ?>
        <?php echo $form->field($User, 'username')->textInput(); ?>
        <?php echo $form->field($User, 'password')->passwordInput() ?>
        <?php echo $form->field($User, 'repassword')->passwordInput() ?>

        <div class="row form-actions">
            <div class="col-xs-3">

            </div>
            <div class="col-xs-9">
                <?php echo Html::submitButton(Yii::t('user', '<i class="icon-menu2"></i> Зарегистрироватся'), ['class' => 'btn btn-warning pull-right', 'name' => 'login-button']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p><a href="<?php echo Url::toRoute('/user/default/login') ?>"><i class="icon-info"></i> <?php echo Yii::t('user', 'Вход')?></a></p>
                <p><a href="<?php echo Url::toRoute('/user/default/resendactivatekey') ?>"><i class="icon-info"></i> <?php echo Yii::t('user', 'Повторно отправить ключь активации')?></a></p>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
