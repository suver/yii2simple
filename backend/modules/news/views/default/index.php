<?php
use backend\widgets\BGridView;
use backend\widgets\BLinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\user\models\User;

//$this->title = [Yii::t('user','Список пользователей'), Yii::t('user','в виде Table Grid')];

$this->title = Yii::t('user','Список Новостей');
$this->pageInfo = Yii::t('user','Полный список новостей');

$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('user','Полный список новостей')], 'meta-description');

$this->params['breadcrumbs'] = [
    Yii::t('user','Список Новостей'),
];

?>
<div class="news-index">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a(Yii::t('news', 'Опубликовать {modelClass}', [
            'modelClass' => 'Новость',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


</div>




<!-- Table view -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title"><i class="icon-people"></i> <?php echo Yii::t('news','Новости') ?></h5>
        <span class="label label-danger pull-right">+<?php echo User::getCount() ?></span> </div>
    <div class="datatable-media">
        <?php
        \yii\widgets\Pjax::begin(['options' => ['class' => 'pjax-wraper']]);
        echo BGridView::widget([
            'dataProvider' => $dataProvider,
            'name' => 'user-grid',
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => Yii::t('news', 'Превью'),
                    'class' => 'yii\grid\DataColumn',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return '<img src="' . $data->image->getImage('50x50') . '">';
                    },
                ],
                'news_id',
                'title',
                'slug',
                [
                    'label' => Yii::t('news', 'Full Name'),
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) {
                        return $data->getAuthorFullname();
                    },
                ],
                [
                    'label' => Yii::t('news', 'views'),
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) {
                        return $data->getViews();
                    },
                ],
                [
                    'label' => Yii::t('news', 'Status'),
                    'class' => 'yii\grid\DataColumn',
                    'value' => function ($data) {
                        return $data->getStatus();
                    },
                ],
                [
                    'label' => Yii::t('news', 'Create Time'),
                    'class' => 'yii\grid\DataColumn',
                    'format' => ['date', 'd.m.Y H:m'],
                    'attribute' => 'create_time',
                ],
                [
                    'label' => Yii::t('news', 'Update Time'),
                    'class' => 'yii\grid\DataColumn',
                    'format' => ['date', 'd.m.Y H:m'],
                    'attribute' => 'update_time',
                ],
                [
                    'label' => '',
                    'class' => 'yii\grid\DataColumn',
                    'format' => 'html',
                    'value' => function ($data) {
                        //$view = '<a class="btn btn-default" href="' . Url::toRoute(['/news/default/view', 'id' => $data->id]) . '">' . Yii::t('news', 'Посм.') . '</a>';
                        $edit = '<a class="btn btn-default" href="' . Url::toRoute(['/news/default/view', 'news_id' => $data->news_id]) . '">' . Yii::t('news', 'Ред.') . '</a>';
                        $del = '<a class="btn btn-default" href="' . Url::toRoute(['/news/default/delete', 'news_id' => $data->news_id]) . '">' . Yii::t('news', 'Удал.') . '</a>';
                        return $edit . ' ' . $del;
                    },
                ],
                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        \yii\widgets\Pjax::end();
        ?>
    </div>
</div>
<!-- /table view -->