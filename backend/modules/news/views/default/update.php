<?php
use yii;
use yii\helpers\Html;


$this->title = Yii::t('user','{title}', ['title' => Html::encode($model->title)]);
$this->pageInfo = Yii::t('user','Редактирование новости');

$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('user','Полный список новостей ', ['title' => Html::encode($this->title)])], 'meta-description');

$this->params['breadcrumbs'] = [
    ['label' => Yii::t('user','Список новостей'), 'url' => ['index']],
    $this->params['breadcrumbs'][] = $this->title,
];
?>

<?php echo $this->render('_edit', [
    'model' => $model,
]) ?>