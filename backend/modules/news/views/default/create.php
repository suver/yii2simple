<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\modules\news\models\News $model
 */

$this->title = Yii::t('user','{title}', ['title' => Yii::t('user','Добавление новости')]);
$this->pageInfo = Yii::t('user','Добавление новости');

$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('user','Полный список новостей ', ['title' => Html::encode($this->title)])], 'meta-description');

$this->params['breadcrumbs'] = [
    ['label' => Yii::t('user','Список новостей'), 'url' => ['index']],
    $this->params['breadcrumbs'][] = Yii::t('user','Добавление новости'),
];
?>

<?php echo $this->render('_edit', [
    'model' => $model,
]) ?>