<?php

use yii\helpers\Html;
use suver\widgets\ActiveForm;
use common\modules\user\models\User;
use common\modules\news\models\News;

/**
 * @var yii\web\View $this
 * @var common\modules\news\models\News $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data',
        ],
        'fieldConfig' => [
            //'template' => '{label}<div class="col-sm-10">{input}{error}</div>',
            //'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ]
    ]); ?>


    <?php echo $form->field($model, 'image')->fileInput() ?>

    <?php echo $form->field($model, 'news_id')->input('text',['mask'=>'99999', 'icon'=>'icon-warning']) ?>

    <?php echo $form->field($model, 'title')->textInput(['maxlength' => 100, 'before'=>'<i class="icon-grid"></i>', 'after'=>'11']) ?>

    <?php echo $form->field($model, 'alias')->textInput(['maxlength' => 100, 'tooltip'=>Yii::t('news','Допускаются латинские буквы, цифры, знак дефис и знак подчеркивания - Aa-Zz, 0-9, -, _')])->info(Yii::t('news','Учавствует в генерации ссылки на новость')) ?>

    <?php echo $form->field($model, 'snippet')->elasticTextarea(['rows' => 6, 'limit' => 255]) ?>

    <?php echo $form->field($model, 'content')->simpleWysiwyg(['rows' => 10]) ?>

    <?php echo $form->field($model, 'status')->switchButton() ?>

    <?php if($User = User::findById($model->author_id)) { ?>

        <div class="form-group field-news-views">
            <label class="col-sm-2 control-label" for="news-views"><?php echo $model->getAttributeLabel('author_id') ?></label>
            <div class="col-sm-10">
                <p class="form-control-static"><?php echo Html::encode($User->getFullname()) ?> / <?php echo Html::encode($User->email) ?></p>
            </div>
        </div>

    <?php } ?>

    <?php if($model->isNewRecord) { ?>

        <?php if($model->views > 0) { ?>

            <div class="form-group field-news-views ">
                <label class="col-sm-2 control-label" for="news-views"><?php echo $model->getAttributeLabel('views') ?></label>
                <div class="col-sm-10">
                    <p class="form-control-static"><?php echo Html::encode($model->views) ?></p>
                </div>
            </div>

        <?php } ?>

        <div class="form-group field-news-update_time">
            <label class="col-sm-2 control-label" for="news-update_time"><?php echo $model->getAttributeLabel('update_time') ?></label>
            <div class="col-sm-10">
                <p class="form-control-static"><?php echo Html::encode($model->update_time) ?></p>
            </div>
        </div>

        <div class="form-group field-news-create_time">
            <label class="col-sm-2 control-label" for="news-create_time"><?php echo $model->getAttributeLabel('create_time') ?></label>
            <div class="col-sm-10">
                <p class="form-control-static"><?php echo Html::encode($model->create_time) ?></p>
            </div>
        </div>

    <?php } ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('news', 'Добавить') : Yii::t('news', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
