<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use suver\widgets\ActiveForm;
use common\modules\user\models\User;
use common\modules\news\models\News;
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'form-horizontal',
        'encrypt' => 'multipart/form-data',
    ],
    'fieldConfig' => [
        'template' => '<div class="col-sm-12">{input}{error}</div>',
        'labelOptions' => ['label'=>false,'class' => 'col-sm-2 control-label'],
    ]
]); ?>

<!-- Task detailed -->
<div class="row">
    <div class="col-lg-8">

        <!-- Task description -->
        <div class="block">


            <?php if(!$model->isNewRecord) { ?>
                <?php echo Yii::t('news', 'URL:') ?>
                <?php echo $form->field($model, 'slug')->editableInput(['maxlength' => 100, 'live-transliterate'=>'news-title', 'tooltip'=>Yii::t('news','Допускаются латинские буквы, цифры, знак дефис и знак подчеркивания - Aa-Zz, 0-9, -, _')])->info(Yii::t('news','Учавствует в генерации ссылки на новость')) ?>
            <?php } ?>

            <?php echo Yii::t('news', 'Заголовок') ?>
            <?php echo $form->field($model, 'title')->textInput(['maxlength' => 100])->label(false) ?>


            <?php if($model->isNewRecord) { ?>
                <?php echo Yii::t('news', 'URL полной новости') ?>
                <?php echo $form->field($model, 'slug')->SlugInput(['maxlength' => 100, 'live-transliterate'=>'news-title', 'tooltip'=>Yii::t('news','Допускаются латинские буквы, цифры, знак дефис и знак подчеркивания - Aa-Zz, 0-9, -, _')])->info(Yii::t('news','Учавствует в генерации ссылки на новость')) ?>
            <?php } ?>

            <ul class="headline-info">
                <li><?php echo Yii::t('news', 'Категория') ?>: <a href="#" data-toggle="modal" data-target="#categotyModal">UI Design</a></li>
                <?php if(!$model->isNewRecord) { ?>
                    <li><?php echo Yii::t('news', 'Просмотров') ?>: <a href="#"><?php echo $model->getViews() ?></a></li>
                    <li><?php echo Yii::t('news', 'Добавлена') ?>: <?php echo $model->create_time->ago() ?></li>
                    <li><?php echo Yii::t('news', 'Обнвлена') ?>: <?php echo $model->update_time->ago() ?></li>
                <?php } ?>
                <li><?php echo Yii::t('news', 'Автор') ?>: <a href="<?php echo Url::toRoute(['/user/edit/index', 'id' => $model->author_id]) ?>"><?php echo $model->getAuthorFullname() ?></a></li>
            </ul>

            <!-- Modal -->
            <div class="modal fade" id="categotyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo Yii::t('news', 'Закрыть') ?></span></button>
                            <h4 class="modal-title" id="myModalLabel"><?php echo Yii::t('news', 'Категории') ?></h4>
                        </div>
                        <div class="modal-body">
                            <?php //echo $form->field($model, 'title')->textInput(['maxlength' => 100]) ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('news', 'Закрыть') ?></button>
                            <button type="button" class="btn btn-primary"><?php echo Yii::t('news', 'Выбрать') ?></button>
                        </div>
                    </div>
                </div>
            </div>

            <?php //echo $model->content ?>
            <?php echo $form->field($model, 'content')->imperaviWysiwyg(['rows' => 20, 'pluginOption' => ['lang' => 'ru']]) ?>

            <hr>
            <div class="panel-footer">
                <div class="pull-left">
                    <ul class="footer-links-group">
                        <?php if(!$model->isNewRecord) { ?>
                            <li><i class="icon-plus-circle muted"></i> <?php echo Yii::t('news', 'Добавлена') ?>: <?php echo $model->create_time->format('d.m.Y H:i:s') ?></li>
                            <li><i class="icon-clock3 muted"></i> <?php echo Yii::t('news', 'Обнвлена') ?>: <?php echo $model->create_time->format('d.m.Y H:i:s') ?></li>
                        <?php } ?>
                        <li><i class="icon-bubble5 muted"></i> <a href="#" class="text-semibold">34 comments</a></li>
                    </ul>
                </div>
                <div class="pull-right">
                    <ul class="footer-icons-group">
                        <li><?php echo Html::a('<i class="icon-pencil"></i>', ['update', 'news_id' => $model->news_id]) ?></li>
                        <li><?php echo Html::a('<i class="icon-cancel-circle"></i>', ['delete', 'news_id' => $model->news_id], [
                                'data' => [
                                    'confirm' => Yii::t('news', 'Вы уверены что хотите удалить новость?'),
                                    'method' => 'post',
                                ],
                            ]) ?></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-wrench"></i></a>
                            <ul class="dropdown-menu icons-right dropdown-menu-right">
                                <li><?php echo Html::a(Yii::t('news', '<i class="icon-quill2"></i> Редактировать'), ['update', 'news_id' => $model->news_id]) ?></li>
                                <li><?php echo Html::a(Yii::t('news', '<i class="icon-cancel-circle"></i> Удалить'), ['delete', 'news_id' => $model->news_id], [
                                        'data' => [
                                            'confirm' => Yii::t('news', 'Вы уверены что хотите удалить новость?'),
                                            'method' => 'post',
                                        ],
                                    ]) ?></li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /task description -->

        <?php if(!$model->isNewRecord) { ?>
            <!-- Comments list -->
            <h6 class="heading-hr"><i class="icon-bubble"></i> <?php echo Yii::t('news', 'Коментарии') ?></h6>
            <div class="block">
                <div class="media"><a class="pull-left" href="#"><img class="media-object" src="images/demo/users/face25.png" alt=""></a>
                    <div class="media-body"><a href="#" class="media-heading">Eugene Kopyov</a>
                        <ul class="headline-info">
                            <li><a href="#">Reply</a></li>
                            <li><a href="#">Edit</a></li>
                        </ul>
                        Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</div>
                </div>
                <div class="media"><a class="pull-left" href="#"><img class="media-object" src="images/demo/users/face24.png" alt=""></a>
                    <div class="media-body"><a href="#" class="media-heading">Martin Wolf</a>
                        <ul class="headline-info">
                            <li><a href="#">Reply</a></li>
                            <li><a href="#">Edit</a></li>
                        </ul>
                        Cras tempus pretium ligula, quis viverra purus eleifend et.
                        <div class="media"><a class="pull-left" href="#"><img class="media-object" src="images/demo/users/face23.png" alt=""></a>
                            <div class="media-body"><a href="#" class="media-heading">Alexander Smith</a>
                                <ul class="headline-info">
                                    <li><a href="#">Reply</a></li>
                                    <li><a href="#">Edit</a></li>
                                </ul>
                                Donec vel porta lorem, at euismod justo. Vestibulum nulla orci, ornare sed auctor ac.</div>
                        </div>
                    </div>
                </div>
                <div class="media"><a class="pull-left" href="#"><img class="media-object" src="images/demo/users/face22.png" alt=""></a>
                    <div class="media-body"><a href="#" class="media-heading">Johnny Strong</a>
                        <ul class="headline-info">
                            <li><a href="#">Reply</a></li>
                            <li><a href="#">Edit</a></li>
                        </ul>
                        Quisque dignissim nibh nec massa egestas interdum. Proin congue vulputate velit, sodales mattis neque tempor a.</div>
                </div>
            </div>
            <!-- /comments list -->
            <!-- Add comment form -->
            <div class="block">
                <h6><i class="icon-bubble-plus"></i> <?php echo Yii::t('news', 'Добавить коментарий') ?></h6>
                <div class="well">
                    <form action="#" role="form">
                        <div class="form-group">
                            <textarea rows="5" cols="5" placeholder="Your message..." class="elastic form-control"></textarea>
                        </div>
                        <div class="form-actions text-right">
                            <input type="reset" value="<?php echo Yii::t('news', 'Отменить') ?>" class="btn btn-danger">
                            <input type="submit" value="<?php echo Yii::t('news', 'Добавить') ?>" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
            <!-- /add comment form -->
        <?php } ?>
    </div>
    <div class="col-lg-4">

        <!-- Превью -->
        <div class="panel panel-success">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-clock"></i> <?php echo Yii::t('news', 'Превью') ?></h6>
            </div>
            <div class="panel-body" style="text-align: center;">
                <div class="col-lg-12"><?php echo $form->field($model, 'status')->dropDownList(News::getStatusList(true)) ?></div>
                <div class="col-lg-12 ">
                    <?php echo Html::submitButton($model->isNewRecord ? Yii::t('news', 'Добавить') : Yii::t('news', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <?php if(!$model->isNewRecord) { ?>
                        <?php echo Yii::t('news', 'или') ?>
                        <?php echo Html::a( Yii::t('news', 'Удалить'), Url::toRoute(['/news/default/delete', 'news_id' => $model->news_id]), ['class' => 'text-danger', 'onclick' => 'confirm("' . Yii::t('news', 'Уверены что хотите удалить?') . '");']) ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- /Превью -->

        <!-- Превью -->
        <div class="panel panel-success">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-clock"></i> <?php echo Yii::t('news', 'Превью') ?></h6>
            </div>
            <div class="panel-body" style="text-align: center;">
                <?php echo suver\images\widgets\UploadImage::widget([
                    'image' => $model->image->getImage('400x400'),
                    'id' => 'image',
                    'width' => 400,
                    'height' => 400,
                    'model' => $model,
                    'field' => 'image',
                ]) ?>
            </div>
        </div>
        <!-- /Превью -->

        <!-- Attached files -->
        <?php echo suver\files\widgets\UploadFiles::widget([
            'id' => 'files',
            'model' => $model,
        ]) ?>
        <!-- /attached files -->

        <!-- Assigned users -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-people"></i> Assigned users</h6>
            </div>
            <ul class="message-list">
                <li class="message-list-header">Developers</li>
                <li>
                    <div class="clearfix">
                        <div class="chat-member"><a href="#"><img src="images/demo/users/face1.png" alt=""></a>
                            <h6>Eugene Kopyov <small>&nbsp; /&nbsp; Wed developer</small></h6>
                        </div>
                        <div class="chat-actions"><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-bubble3"></i></a><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-phone2"></i></a></div>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <div class="chat-member"><a href="#"><img src="images/demo/users/face2.png" alt=""></a>
                            <h6>Duncan McMart <small>&nbsp; /&nbsp; Front end dev</small></h6>
                        </div>
                        <div class="chat-actions"><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-bubble3"></i></a><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-phone2"></i></a></div>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <div class="chat-member"><a href="#"><img src="images/demo/users/face3.png" alt=""></a>
                            <h6>Lucy Smith <small>&nbsp; /&nbsp; UI expert</small></h6>
                        </div>
                        <div class="chat-actions"><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-bubble3"></i></a><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-phone2"></i></a></div>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <div class="chat-member"><a href="#"><img src="images/demo/users/face4.png" alt=""></a>
                            <h6>Angel Nowak <small>&nbsp; /&nbsp; Usability expert</small></h6>
                        </div>
                        <div class="chat-actions"><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-bubble3"></i></a><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-phone2"></i></a></div>
                    </div>
                </li>
                <li class="message-list-header">Management</li>
                <li>
                    <div class="clearfix">
                        <div class="chat-member"><a href="#"><img src="images/demo/users/face5.png" alt=""></a>
                            <h6>Vin Dins <small>&nbsp; /&nbsp; CEO</small></h6>
                        </div>
                        <div class="chat-actions"><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-bubble3"></i></a><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-phone2"></i></a></div>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <div class="chat-member"><a href="#"><img src="images/demo/users/face6.png" alt=""></a>
                            <h6>Robert Razinsky <small>&nbsp; /&nbsp; Team lead</small></h6>
                        </div>
                        <div class="chat-actions"><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-bubble3"></i></a><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-phone2"></i></a></div>
                    </div>
                </li>
                <li>
                    <div class="clearfix">
                        <div class="chat-member"><a href="#"><img src="images/demo/users/face7.png" alt=""></a>
                            <h6>Mary Robinson <small>&nbsp; /&nbsp; Project manager</small></h6>
                        </div>
                        <div class="chat-actions"><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-bubble3"></i></a><a href="#" class="btn btn-link btn-icon btn-xs"><i class="icon-phone2"></i></a></div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- /assigned users -->

    </div>
</div>
<!-- /task detailed -->

<?php ActiveForm::end(); ?>

