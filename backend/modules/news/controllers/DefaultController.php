<?php

namespace backend\modules\news\controllers;

use Yii;
use common\modules\news\models\News;
use common\modules\news\models\query\NewsQuery;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for News model.
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post','get','delete'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new News();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $news_id
     * @return mixed
     */
    public function actionView($news_id)
    {
        $model = $this->findModel($news_id);

        if(Yii::$app->request->post('name') AND Yii::$app->request->post('pk')) {
            $model->{Yii::$app->request->post('name')} = Yii::$app->request->post('value');
            $model->save(true, [Yii::$app->request->post('name')]);
            return Json::encode(['success' => true]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->hasErrors()) {
                var_dump($model->getErrors());
                Yii::$app->session->setFlash("error", $model->getFirstErrors());
            }

            return $this->redirect(['view', 'news_id' => $model->news_id]);
        } else {
            if($model->hasErrors()) {
                var_dump($model->getErrors());
                Yii::$app->session->setFlash("error", $model->getFirstErrors());
            }
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News;

        $model->author_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()))
        {
            if($model->save()) {
                return $this->redirect(['view', 'news_id' => $model->news_id]);
            }
            else {
                var_dump($model->getErrors());
                exit;
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $news_id
     * @return mixed
     */
    public function actionDelete($news_id)
    {
        $news = $this->findModel($news_id);
        $news->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $news_id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($news_id)
    {
        if (($model = News::findOne($news_id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
