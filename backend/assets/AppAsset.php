<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/londinium-theme.min.css',
        'css/icons.min.css',
        'css/font-awesome.min.css',
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext',
        'css/site.css',
    ];

    public $js = [
        'js/jquery.pjax.js',
        'js/plugins/interface/daterangepicker.js',
        'js/plugins/interface/moment.js',
        'js/plugins/interface/jgrowl.min.js',
        'js/plugins/interface/collapsible.min.js',
        'js/plugins/forms/select2.min.js',
        'js/backend.js',
        'js/uploadmeavatar.js',

        // http://www.malsup.com/jquery/form
        'js/jquery.ui.widget.js',
        'js/fileupload/jquery.fileupload.js',
        'js/fileupload/jquery.iframe-transport.js',

    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_BEGIN,
    ];
}
