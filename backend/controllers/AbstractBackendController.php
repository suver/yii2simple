<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
#use app\modules\user\filters\AccessControl;
use yii\web\Controller;
use common\modules\user\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
abstract class AbstractBackendController extends Controller
{
    public $pageTitle = '';
    public $pageInfo = '';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['login', 'error'], // Define specific actions
                        'allow' => true, // Has access
                        'roles' => ['@'], // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false, // Do not have access
                        'roles'=>['?'], // Guests '?'
                    ],
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

}
