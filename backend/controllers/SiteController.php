<?php
namespace backend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\controllers\AbstractBackendController;
use yii\base\ErrorException;

/**
 * Site controller
 */
class SiteController extends AbstractBackendController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

}
