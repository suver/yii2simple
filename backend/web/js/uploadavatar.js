/**
 * Created by suver on 25.08.14.
 */

$(function(){

    $(document).on("click", "[data-change-avatar]", function(){

        var avatar_user = $(this).attr('data-change-avatar');
        if(!$(this).data("field_id"))
        {
            $(this).data("field_id", "upload_user_avatar_" + avatar_user);
        }

        field_id =  $(this).data("field_id");

        // имитация нажатия на поле выбора файла
        input_name = $(this).attr('data-field-name');

        if(!$("#" + field_id).length)
        {
            element = $(this).after("<input type='file' name='" + input_name + "' id='" + field_id + "' data-url='" + $(this).attr("data-url") + "' style='display:none;'>");

            var csrf_name = $("[data-csr-tocken=" + avatar_user + "]").attr('name');
            var csrf_value = $("[data-csr-tocken=" + avatar_user + "]").attr('value');

            // инициализация плагина jQuery File Upload
            $("#" + field_id).fileupload({
                // этот элемент будет принимать перетаскиваемые на него файлы
                //dropZone: $('#drop'),
                dataType: 'json',
                // Функция будет вызвана при помещении файла в очередь
                add: function (e, data) {

                    avatarSize = [];
                    $('[data-avatar-user=' + avatar_user + ']').each(function(i){
                        avatarSize[i] = $(this).attr("data-avatar-size");
                    });

                    data.formData = {sizes: avatarSize};
                    data.formData[csrf_name] = csrf_value;
                    // Автоматически загружаем файл при добавлении в очередь
                    data.submit();

                    // Сбрасываем лоадеры на ноль
                    $("[data-avatar-progress]").css('width','0%');
                },
                done: function (e, data) {
                    $("[data-avatar-size][data-avatar-user=" + avatar_user + "]").attr('src', data.result.image);
                    for(item in data.result.sizes){
                        $("img[data-avatar-size=" + item + "][data-avatar-user=" + avatar_user + "]").attr('src', data.result.sizes[item]);
                    };
                },
                progress: function(e, data){
                    // Вычисление процента загрузки
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $("[data-avatar-loader-progress]").css('opacity', 1);

                    // обновляем шкалу
                    $("[data-avatar-progress]").css('width', progress + '%');
                    $("[aria-valuenow]", "[data-avatar-progress]").val(progress);
                    $(".sr-only", "[data-avatar-progress]").text(progress + "%");

                    if(progress == 100){
                        $("[data-avatar-loader-progress]").css('opacity', 0);
                    }
                },
                fail:function(e, data){
                    console.log('Avatar upload Error', data);
                }
            });
        }

        $("#" + field_id).click();
    });



    /*$(document).on('drop dragover', function (e) {
        e.preventDefault();
    });*/


    $(document).on('click', "[data-remove-avatar]", function(){
        _url = $(this).attr('data-url');
        var avatar_user = $(this).attr('data-remove-avatar');

        var csrf_name = $("[data-csr-tocken=" + avatar_user + "]").attr('name');
        var csrf_value = $("[data-csr-tocken=" + avatar_user + "]").attr('value');

        avatarSize = [];
        $('[data-avatar-size]').each(function(i){
            avatarSize[i] = $(this).attr("data-avatar-size");
        });

        formData = {};
        formData[csrf_name] = csrf_value;
        formData['sizes'] = avatarSize;

        $.ajax({
            url: _url,
            data: formData,
            dataType: 'json',
            type: 'post',
            success: function(data){

            }
        })
        .done(function(data){
            $("[data-avatar-size][data-avatar-user=" + avatar_user + "]").attr('src', data.image);
            for(item in data.sizes){
                $("[data-avatar-size=" + item + "][data-avatar-user=" + avatar_user + "]").attr('src', data.sizes[item]);
            };
        });
    });

});

