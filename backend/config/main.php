<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'admin' => [
            'class' => 'backend\modules\admin\Module'
        ],
        'user' => [
            'class' => 'backend\modules\user\Module'
        ],
        'news' => [
            'class' => 'backend\modules\news\Module'
        ],
    ],
    'components' => [
        'urlManager' => [
            'rules' => [
                // Модуль [[Admin]]
                '' => 'admin/default/index',

                // Модуль [[Users]]
                '<action:(login|logout)>' => 'default/<action>',
            ]
        ],
        'request' => [
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'cookieValidationKey' => 'sdfHFJSKHhuwesHJhsd8fwJHjhsdf98werwer454',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'error/error',
        ],
        'i18n' => [
            'translations' => [
                'user' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'ru',
                    'basePath' => '@common/modules/user/messages',
                ],
            ],
        ],
    ],
    'params' => $params,
];
