<?php
use backend\assets\AppAsset;
use backend\assets\BootstrapAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;


/**
 * @var \yii\web\View $this
 * @var string $content
 */

BootstrapAsset::register($this);

AppAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="<?php echo !empty($this->sidebarLite) ? 'sidebar-narrow wysihtml5-supported' : 'sidebar-wide' ?>">

<?php $this->beginBody() ?>
<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><img src="/images/logo.png" alt="Londinium"></a>
        <a class="sidebar-toggle" data-sidebar-toggle><i class="icon-paragraph-justify2"></i></a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons"><span class="sr-only">Toggle navbar</span><i class="icon-grid3"></i></button>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar"><span class="sr-only">Toggle navigation</span><i class="icon-paragraph-justify2"></i></button>
    </div>
    <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
        <?php echo backend\modules\user\widgets\ActivityDropdown::widget(['userModel'=>Yii::$app->user->identity]); ?>
        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><i class="icon-paragraph-justify2"></i><span class="label label-default">6</span></a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="popup-header">
                    <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
                    <span>Messages</span>
                    <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
                </div>
                <ul class="popup-messages">
                    <li class="unread"><a href="#"><img src="/images/demo/users/face1.png" alt="" class="user-face"><strong>Eugene Kopyov <i class="icon-attachment2"></i></strong><span>Aliquam interdum convallis massa...</span></a></li>
                    <li><a href="#"><img src="/images/demo/users/face2.png" alt="" class="user-face"><strong>Jason Goldsmith <i class="icon-attachment2"></i></strong><span>Aliquam interdum convallis massa...</span></a></li>
                    <li><a href="#"><img src="/images/demo/users/face3.png" alt="" class="user-face"><strong>Angel Novator</strong><span>Aliquam interdum convallis massa...</span></a></li>
                    <li><a href="#"><img src="/images/demo/users/face4.png" alt="" class="user-face"><strong>Monica Bloomberg</strong><span>Aliquam interdum convallis massa...</span></a></li>
                    <li><a href="#"><img src="/images/demo/users/face5.png" alt="" class="user-face"><strong>Patrick Winsleur</strong><span>Aliquam interdum convallis massa...</span></a></li>
                </ul>
            </div>
        </li>
        <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle"><i class="icon-grid"></i></a>
            <div class="popup dropdown-menu dropdown-menu-right">
                <div class="popup-header">
                    <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
                    <span>Tasks list</span>
                    <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>Category</th>
                        <th class="text-center">Priority</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><span class="status status-success item-before"></span> <a href="#">Frontpage fixes</a></td>
                        <td><span class="text-smaller text-semibold">Bugs</span></td>
                        <td class="text-center"><span class="label label-success">87%</span></td>
                    </tr>
                    <tr>
                        <td><span class="status status-danger item-before"></span> <a href="#">CSS compilation</a></td>
                        <td><span class="text-smaller text-semibold">Bugs</span></td>
                        <td class="text-center"><span class="label label-danger">18%</span></td>
                    </tr>
                    <tr>
                        <td><span class="status status-info item-before"></span> <a href="#">Responsive layout changes</a></td>
                        <td><span class="text-smaller text-semibold">Layout</span></td>
                        <td class="text-center"><span class="label label-info">52%</span></td>
                    </tr>
                    <tr>
                        <td><span class="status status-success item-before"></span> <a href="#">Add categories filter</a></td>
                        <td><span class="text-smaller text-semibold">Content</span></td>
                        <td class="text-center"><span class="label label-success">100%</span></td>
                    </tr>
                    <tr>
                        <td><span class="status status-success item-before"></span> <a href="#">Media grid padding issue</a></td>
                        <td><span class="text-smaller text-semibold">Bugs</span></td>
                        <td class="text-center"><span class="label label-success">100%</span></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </li>
        <?php
        echo \backend\widgets\UserMenu\Widget::widget([
            'title' => '<span><img src="' . Yii::$app->user->identity->avatar->getImage('32x32') . '" data-avatar-user="me" data-avatar-size="32x32"> ' . Yii::$app->user->identity->name . ' ' . Yii::$app->user->identity->surname . '</span>',
            'items' => [
                'profile' => [
                    'visible' => true,
                    'url' => ['create'],
                    'label' => '<i class="icon-user"></i> ' . Yii::t('app', 'Profile')
                ],
                'profile' => [
                    'visible' => true,
                    'url' => ['create'],
                    'label' => '<i class="icon-bubble4"></i> ' . Yii::t('app', 'Messages')
                ],
                'profile' => [
                    'visible' => true,
                    'url' => ['/user/profile/me'],
                    'label' => '<i class="icon-user"></i> ' . Yii::t('app', 'Профиль')
                ],
                'logout' => [
                    'visible' => true,
                    'url' => ['/user/default/logout'],
                    'label' => '<i class="icon-exit"></i> ' . Yii::t('app', 'Выход')
                ],
            ],
        ]);
        ?>
    </ul>
</div>
<!-- /navbar -->

<!-- Page container -->
<div class="page-container" data-page-container>
    <!-- Sidebar -->
    <div class="sidebar">
        <div class="sidebar-content">
            <!-- User dropdown -->
            <div class="user-menu dropdown">
                <a href="#" class="dropdown-toggle" data-info-open="#info-sidebar-info"><img src="<?php echo Yii::$app->user->identity->avatar->getImage('48x48') ?>" data-avatar-user="me" data-avatar-size="48x48">
                    <div class="user-info"><?php echo Yii::$app->user->identity->name ?> <?php echo Yii::$app->user->identity->surname ?> <span><?php echo Yii::$app->user->identity->username ?></span></div>
                </a>
                <div id="info-sidebar-info" class="popup dropdown-menu dropdown-menu-right">
                    <div class="thumbnail">
                        <div class="thumb">
                            <input type="hidden" name="<?php echo Yii::$app->request->csrfParam ?>" value="<?php echo Yii::$app->request->getCsrfToken() ?>" data-csr-tocken="me">
                            <img alt="" src="<?php echo Yii::$app->user->identity->avatar->getImage('188x188') ?>" data-avatar-user="me" data-avatar-size="188x188">
                            <div class="thumb-loader-progress" data-avatar-loader-progress>
                                <span>
                                    <div class="progress" data-avatar-progress></div>
                                </span>
                            </div>
                            <div class="thumb-options">
                                <span>
                                    <a href="#" class="btn btn-icon btn-success" data-change-me-avatar="me"><i class="icon-pencil"></i></a>
                                    <a href="#" class="btn btn-icon btn-success" data-remove-me-avatar="me"><i class="icon-remove"></i></a>
                                </span>
                            </div>
                        </div>
                        <div class="caption text-center">
                            <h6><?php echo Yii::$app->user->identity->name ?> <?php echo Yii::$app->user->identity->surname ?> <small><?php echo Yii::$app->user->identity->username ?></small></h6>
                        </div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item"><i class="icon-pencil3 text-muted"></i> My posts <span class="label label-success">289</span></li>
                        <li class="list-group-item"><i class="icon-people text-muted"></i> Users online <span class="label label-danger">892</span></li>
                        <li class="list-group-item"><i class="icon-stats2 text-muted"></i> Reports <span class="label label-primary">92</span></li>
                        <li class="list-group-item"><i class="icon-stack text-muted"></i> Balance
                            <h5 class="pull-right text-danger">$45.389</h5>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /user dropdown -->
            <!-- Main navigation -->
            <?php
            echo \backend\widgets\BMenu::widget([
                'options' => [
                    'class' => 'navigation',
                ],
                'items' => [
                    ['label' => Yii::t("app", "Dashboard"), 'icon' => 'icon-screen2', 'url' => ['/']],
                    ['label' => Yii::t("app", "Пользователи"), 'icon' => 'icon-user', 'url' => ['/user/list/index'], 'items' => [
                        ['label' => Yii::t("app", "Список пользователей"), 'url' => ['/user/list/index']],
                        ['label' => Yii::t("app", "Добавить пользователя"), 'url' => ['/user/add/index']],
                    ]],
                    ['label' => Yii::t("app", "Новости"), 'icon' => 'icon-newspaper', 'url' => ['/news/default'], 'items' => [
                        ['label' => Yii::t("app", "Список Новостей"), 'url' => ['/news/default']],
                        ['label' => Yii::t("app", "Добавить Новость"), 'url' => ['/news/default/create']],
                    ]],
                    ['label' => Yii::t("app", "Примеры"), 'icon' => 'icon-wink2', 'url' => [''], 'items' => [
                        ['label' => Yii::t("app", "Иконки"), 'url' => ['/admin/default/icons']],
                        ['label' => Yii::t("app", "Форма"), 'url' => ['/admin/default/fields']],
                    ]],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]);
            ?>
            <!-- /main navigation -->
        </div>
    </div>
    <!-- /sidebar -->
    <!-- Page content -->
    <div class="page-content">
        <!-- Page header -->
        <div class="page-header">
            <div class="page-title">
                <h3><?php echo $this->title ?><?php echo !empty($this->pageInfo) ? ' <small>' . $this->pageInfo . '</small>' : ''  ?></h3>
            </div>
            <div id="reportrange" class="range">
                <div class="visible-xs header-element-toggle"><a class="btn btn-primary btn-icon"><i class="icon-calendar"></i></a></div>
                <div class="date-range"></div>
                <span class="label label-danger">9</span></div>
        </div>
        <!-- /page header -->
        <!-- Breadcrumbs line -->
        <div class="breadcrumb-line">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>

        <?php echo backend\widgets\jGrowl::widget(); ?>

        <!-- /breadcrumbs line -->
        <?= $content ?>
        <!-- Footer -->
        <div class="footer clearfix">
            <div class="pull-left"><?= Yii::powered() ?></div>
            <div class="pull-right icons-group"> <a href="#"><i class="icon-screen2"></i></a> <a href="#"><i class="icon-balance"></i></a> <a href="#"><i class="icon-cog3"></i></a> </div>
        </div>
        <!-- /footer -->
    </div>
<!-- /page content -->
</div>
<!-- /content -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>