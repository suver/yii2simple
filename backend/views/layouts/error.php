<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;


/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode(is_array($this->title) ? $this->title['0'] : $this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="full-width page-condensed">
<?php $this->beginBody() ?>
<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header"><a class="navbar-brand" href="#">
        <img src="images/logo.png" alt="Londinium"></a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
            <span class="sr-only">Toggle navbar</span><i class="icon-grid3"></i>
        </button>
    </div>
</div>
<!-- /navbar -->

<?= $content ?>

<!-- Footer -->
<div class="footer clearfix">
    <div class="pull-left"><?= Yii::powered() ?></div>
    <div class="pull-right icons-group"> <a href="#"><i class="icon-screen2"></i></a> <a href="#"><i class="icon-balance"></i></a> <a href="#"><i class="icon-cog3"></i></a> </div>
</div>
<!-- /footer -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>