<?php
use backend\assets\AppAsset;
use backend\assets\BootstrapAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');
/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
BootstrapAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo Html::encode(is_array($this->title) ? $this->title['0'] : $this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="full-width page-condensed">
<?php $this->beginBody() ?>
<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-right"><span class="sr-only">Toggle navbar</span><i class="icon-grid3"></i>
        </button>
        <a class="navbar-brand" href="#"><img src="images/logo.png" alt="Londinium"></a>
    </div>
</div>
<!-- /navbar -->

<?php backend\widgets\jGrowl::begin([
    'ignoreFlashType' => ['success','info'],
]); ?>
<div class="container">
    <div class="callout callout-{type} fade in">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <h5>{header}</h5>
        <p>{message}</p>
    </div>
</div>
<?php backend\widgets\jGrowl::end(); ?>

<?php echo $content ?>

<!-- Footer -->
<div class="footer clearfix">
    <div class="pull-left"><?= Yii::powered() ?></div>
    <div class="pull-right icons-group"> <a href="#"><i class="icon-screen2"></i></a> <a href="#"><i class="icon-balance"></i></a> <a href="#"><i class="icon-cog3"></i></a> </div>
</div>
<!-- /footer -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
