<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */

$this->title = $name;
?>

<!-- Error wrapper -->
<div class="error-wrapper text-center">
    <h1><?= nl2br(Html::encode($exception->statusCode)) ?></h1>
    <h6><?= nl2br(Html::encode($exception->getMessage())) ?></h6>
    <!-- Error content -->
    <div class="error-content">
        <div class="row">
            <div class="col-md-6"> <a href="<?php echo Url::to('site/index') ?>" class="btn btn-danger btn-block">Back to dashboard</a> </div>
        </div>
    </div>
    <!-- /error content -->
</div>
<!-- /error wrapper -->
