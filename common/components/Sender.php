<?php

namespace common\components;

use common\modules\user\models\User;
use Yii;

class Sender {

    public $template_path;
    public $from;

    protected function params($params)
    {
        // TO
        if(isset($params['to']))
        {
            if(is_object($params['to']))
            {
                $user = User::findById($params['to']->id);
                if($user)
                {
                    $params['to'] = $user->email;
                }
                else
                {
                    throw new Exception(Yii::t('user', 'User by UserID:{user_id} not found',array("{user_id}"=>$params['to']->id)));
                }
            }
            elseif(is_numeric($params['to']))
            {
                $user = User::findById($params['to']);
                if($user)
                {
                    $params['to'] = $user->email;
                }
                else
                {
                    throw new Exception(Yii::t('user', 'User by UserID:{user_id} not found',array("{user_id}"=>$params['to'])));
                }
            }
            elseif(preg_match("/@/", $params['to']))
            {
                $user = User::findByEmail($params['to']);
                if($user)
                {
                    $params['to'] = $params['to'];
                }
                else
                {
                    throw new Exception(Yii::t('user', 'User by Email:{email} not found',array("{email}"=>$params['to'])));
                }
            }
            else
            {
                $user = User::findByUsername($params['to']);
                if($user)
                {
                    $params['to'] = $user->email;
                }
                else
                {
                    throw new Exception(Yii::t('user', 'User by Username:{username} not found',array("{username}"=>$params['to'])));
                }
            }
        }
        elseif(!Yii::$app->user->isGuset)
        {
            $user = User::findById(Yii::$app->user->identity->id);
            $params['to'] = $user->email;
        }
        else
        {
            throw new Exception('Param [to] must be exsist. [to=UserID, to=Email, to=Username or you mast be authorized]');
        }

        //FROM
        if(empty($params['from']))
        {
            if(isset(Yii::$app->params['supportEmail']) AND empty($this->from))
            {
                $params['from'] = Yii::$app->params['supportEmail'];
            }
            elseif(isset($this->from) AND empty(Yii::$app->params['supportEmail']))
            {
                $params['from'] = $this->from;
            }
            else
            {
                throw new Exception('Param [from] must be exsist. [params[supportEmail]=Email or component setting FROM=email]');
            }
        }
        else
        {
            if(is_object($params['from']))
            {
                $user = User::findById($params['from']->user_id);
                if($user)
                {
                    $params['from'] = $user->email;
                }
                else
                {
                    throw new Exception(Yii::t('user', 'User by UserID:{user_id} not found',array("{user_id}"=>$params['from']->user_id)));
                }
            }
        }

        //MESSAGE
        if(isset($params['message']))
        {
            if(empty($params['type']))
            {
                $params['type'] = 'setTextBody';
            }

            switch($params['type'])
            {
                case "text":
                    $params['type'] = 'setTextBody';
                    break;
                case "html":
                    $params['type'] = 'setHtmlBody';
                    break;
                default:
                    $params['type'] = 'setTextBody';
            }
        }
        else
        {
            throw new Exception('Param [message] must be exsist');
        }

        return $params;
    }

    public function email($params=array())
    {
        $params = $this->params($params);

        $emailer = Yii::$app->mail->compose();
        $emailer->setFrom($params['from']);
        $emailer->setTo($params['to']);
        if(isset($params['subject']))
        {
            $emailer->setSubject($params['subject']);
        }
        if(isset($params['replyTo']))
        {
            $emailer->setReplyTo($params['replyTo']);
        }
        if(isset($params['cc']))
        {
            $emailer->setCc($params['cc']);
        }
        if(isset($params['bcc']))
        {
            $emailer->setBcc($params['bcc']);
        }
        if(isset($params['message']))
        {
            $emailer->{$params['type']}($params['message'], $params['type']);
        }

        return $emailer->send();
    }


}