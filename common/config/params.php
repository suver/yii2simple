<?php
return [
    'adminEmail' => 'suver@farpse.com',
    'supportEmail' => 'mail@farpse.com',
    'user.passwordResetTokenExpire' => 3600,
    'host' => 'http://yii2simple.dev/',
];
