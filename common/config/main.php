<?php
return [
    'sourceLanguage' => 'en',
    'language' => 'ru',
    'charset' => 'utf-8',
    'timeZone' => 'Europe/Moscow',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'components' => [
        'image' => array(
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD',  //GD or Imagick
        ),
        'user' => [
            'class' => 'common\modules\user\components\IUser',
            'identityClass' => 'common\modules\user\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/user/default/login'],
        ],
        'view' => [
            'class' => 'common\components\View',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                // gii
                'gii' => 'gii',
                'gii/<controller>' => 'gii/<controller>',
                'gii/<controller>/<action>' => 'gii/<controller>/<action>',

                // Общие правила
                '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ]
            ]
        ],
        'authManager'=>array(
            'class' => 'yii\rbac\PhpManager',
            //'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest', 'user', 'author', 'writer', 'moderator', 'admin'],
        ),
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
                'blogs' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'ru',
                    'basePath' => '@common/modules/blogs/messages',
                ],
                'user' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'ru',
                    'basePath' => '@common/modules/user/messages',
                ],

                'news' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'ru',
                    'basePath' => '@common/modules/news/messages',
                ],
                /*'categories' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'ru',
                    'basePath' => '@common/modules/blogs/modules/categories/messages',
                ],
                'comments' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'ru',
                    'basePath' => '@common/modules/comments/messages',
                ],*/
            ],
        ],


        'logger' => [
            'class' => 'suver\logger\Logger',
        ],

        'temporaryStorage' => [
            'class' => 'suver\temporaryStorage\temporaryStorage',
        ],

        'activity' => [
            'class' => 'suver\activity\Activity',
            'user'  => [
                'class'  => 'common\modules\user\models\User',
                'fields' => [
                    'id'    =>'id',
                    'email' => 'email',
                    'fullname' => 'fullname',
                ],
            ],
        ],

        'mailer' => [
            'class' => 'suver\mailer\Mailer',
            'user'  => [
                'class'  => 'common\modules\user\models\User',
                'fields' => [
                    'id'    =>'id',
                    'email' => 'email',
                    'phone' => 'phone',
                    'fullname' => 'fullname',
                ],
            ],
            'methods'  => [
                'default' => [
                    'method' => 'suver\mailer\method\SwiftMailer',
                    'sender' => 'mail@farpse.com',
                    'viewPath' => '@common/mail',
                    'layout' => 'html',
                    'transport' => [
                        'class' => 'Swift_SmtpTransport',
                        'host'  => 'smtp.yandex.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                        'username' => 'mail@farpse.com',
                        'password' => 'on82xni7',
                        'port'  => '465', // Port 25 is a very common port too
                        'encryption' => 'ssl', // It is often used, check your provider or mail server specs

                    ],
                ],
                'smpt' => [
                    'method' => 'suver\mailer\method\SwiftMailer',
                    'sender' => 'mail@farpse.com',
                    'transport' => [
                        'class' => 'Swift_SmtpTransport',
                        'host'  => 'smtp.yandex.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                        'username' => 'mail@farpse.com',
                        'password' => 'on82xni7',
                        'port'  => '465', // Port 25 is a very common port too
                        'encryption' => 'ssl', // It is often used, check your provider or mail server specs

                    ],
                ],
                'mail' => [
                    'method' => 'suver\mailer\method\SwiftMailer',
                    'sender' => 'mail@farpse.com',
                    'transport' => [
                        'class' => 'Swift_MailTransport',
                    ],
                ],
                'sendmail' => [
                    'method' => 'suver\mailer\method\SwiftMailer',
                    'sender' => 'mail@farpse.com',
                    'transport' => [
                        'class' => 'Swift_SendmailTransport',
                        'command' => '/usr/sbin/exim -bs',
                    ],
                ],
                'sms' => [
                    'class' => 'suver\mailer\method\Sms',
                    'useFileTransport' => false,
                    'transport' => [

                    ],
                ],
            ],
        ],

        'sender' => [
            'class' => 'common\components\Sender',
            'template_path' => [
                'email' => '@common/templates/email',
            ],
        ],

        'assetManager' => [
            'linkAssets' => true,
        ],
    ],
    //'bootstrap' => ['gii'],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1'],
        ],
        'user' => [
            'class' => 'common\modules\user\Module'
        ],
        'admin' => [
            'class' => 'common\modules\admin\Module'
        ],
        'blogs' => [
            'class' => 'common\modules\blogs\Blogs',
            'modules' => [
                'categories' => [
                    'class' => 'common\modules\blogs\modules\categories\Categories'
                ],
            ]
        ],
        /*'comments' => [
            'class' => 'common\modules\comments\Comments'
        ],*/
        'images' => [
            'class' => 'suver\images\Module',
            'cacheImage' => '@www/assets/images/{owner}',
            'storageImage' => 'statics/images/{owner}',
            'storageTemporary' => 'statics/tmp/{owner}',
            'rootDirectory' => Yii::getAlias('@root'),
            'defaultImage' => 'statics/default/{owner}.png',
        ],
        'files' => [
            'class' => 'suver\files\Module',
            'cacheFiles' => '@www/assets/files/{owner}',
            'storageFiles' => 'statics/files/{owner}',
            'storageTemporary' => 'statics/tmp/{owner}',
            'rootDirectory' => Yii::getAlias('@root'),
        ],
        'yii2images' => [
            'class' => 'rico\yii2images\Module',
            //be sure, that permissions ok
            //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
            'imagesStorePath' => '@root/static', //path to origin images
            'imagesCachePath' => '/assets/cache', //path to resized copies
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
            'placeHolderPath' => '@webroot/images/placeHolder.png' // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
        ],
    ],
];
