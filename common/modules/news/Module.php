<?php

namespace common\modules\news;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\news\controllers';

    /**
     * @var integer Количество записей на главной странице модуля.
     */
    public $recordsPerPage = 5;

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
