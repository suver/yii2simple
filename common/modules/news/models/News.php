<?php

namespace common\modules\news\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\modules\user\models\User;

/**
 * This is the model class for table "news".
 *
 * @property integer $news_id
 * @property integer $author_id
 * @property string $title
 * @property string $slug
 * @property string $snippet
 * @property string $content
 * @property string $image
 * @property integer $status
 * @property integer $views
 * @property integer $create_time
 * @property integer $update_time
 */
class News extends \yii\db\ActiveRecord
{

    const IMAGES_PATH = 'statics/news/images';
    const ASSETS_PATH = '@www/assets/images';
    const DEFAULT_IMAGE = 'statics/default/default-news-image.png';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE = -1;

    public function getStatus()
    {
        $statusList = self::getStatusList();
        return isset($statusList[$this->status]) ? $statusList[$this->status] : NULL ;
    }

    public static function getStatusList($safe=false)
    {
        if($safe)
        {
            return [
                self::STATUS_ACTIVE => Yii::t('news', 'Активная / Показывать'),
                self::STATUS_INACTIVE => Yii::t('news', 'Не активная / Скрывать'),
            ];
        }
        else
        {
            return [
                self::STATUS_ACTIVE => Yii::t('news', 'Активная / Показывать'),
                self::STATUS_INACTIVE => Yii::t('news', 'Не активная / Скрывать'),
                self::STATUS_DELETE => Yii::t('news', 'Удалена'),
            ];
        }
    }

    public function getAuthorFullname()
    {
        $User = User::findById($this->author_id);
        if($User)
        {
            return $User->getFullname();
        }
        return false;
    }


    public function getViews()
    {
        return $this->views;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'suver\behaviors\DatetimeBehavior',
                'attributes' => [
                    'insert' => ['create_time', 'update_time'],
                    'update' => ['update_time'],
                ],
            ],
            'preview' => [
                'class' => 'suver\images\behaviors\Images',
                //'rootDirectory' => Yii::getAlias('@root'),
                //'storageImage' =>  self::IMAGES_PATH,
                //'cacheImage' => self::ASSETS_PATH,
                //'defaultImage' => self::DEFAULT_IMAGE,
                'attribute' => 'image',
            ],
            'snippet' => [
                'class' => 'suver\behaviors\AutoSnippet',
                'attributes' => [
                    'content' => 'snippet',
                ],
            ],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => ['author_id', 'title', 'slug', 'snippet', 'content', 'status', 'views', 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'author_id', 'title', 'slug', 'content'], 'required'],
            [['news_id', 'author_id', 'status', 'views', 'image'], 'integer'],
            [['snippet', 'content'], 'string'],
            [['title', 'slug'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'news_id' => Yii::t('news', 'ID Новости'),
            'author_id' => Yii::t('news', 'Автор'),
            'title' => Yii::t('news', 'Заголовок'),
            'slug' => Yii::t('news', 'Алиас для URL'),
            'snippet' => Yii::t('news', 'Описание / Снипет'),
            'content' => Yii::t('news', 'Текст новости'),
            'image' => Yii::t('news', 'Превью новости'),
            'status' => Yii::t('news', 'Статус'),
            'views' => Yii::t('news', 'Просмотров'),
            'create_time' => Yii::t('news', 'Создана'),
            'update_time' => Yii::t('news', 'Обновлена'),
        ];
    }

    /**
     * Поиск записей по переданным критериям.
     * @param array|null Массив с критериями для выборки.
     * @return \yii\data\ActiveDataProvider dataProvider с результатами поиска.
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'news_id'=>[
                        'label' => $this->getAttributeLabel('news_id'),
                    ],
                    'title'=>[
                        'label' => $this->getAttributeLabel('title'),
                    ],
                    'slug'=>[
                        'label' => $this->getAttributeLabel('slug'),
                    ],
                    'status'=>[
                        'label' => $this->getAttributeLabel('status'),
                    ],
                    'create_time'=>[
                        'label' => $this->getAttributeLabel('create_time'),
                    ],
                    'update_time'=>[
                        'label' => $this->getAttributeLabel('update_time'),
                    ]
                ]
                /*
                 'attributes' => [
                    'age',
                    'name' => [
                        'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                        'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Name',
                    ]
                 ]
                 */
            ],
            /*'pagination' => [
                'pageSize' => 2,
            ]*/
        ]);

        if(!empty($params['filter']))
        {
            $query->orFilterWhere(['like', 'title', $params['filter']])
                ->orFilterWhere(['like', 'slug', $params['filter']]);

        }

        // load the seach form data and validate
        if(!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['news_id' => $this->news_id]);
        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'create_time', $this->create_time]);

        return $dataProvider;
    }

    public function removeImage()
    {
        $scenario = $this->scenario;
        $this->setAttribute('image', NULL);
        if($this->save())
        {
            $this->scenario = $scenario;
            return true;
        }
        else
        {
            $this->scenario = $scenario;
            return false;
        }
    }
}
