<?php
namespace common\modules\news\models\query;

use Yii;
use yii\db\ActiveQuery;
use common\modules\news\models\News;

/**
 * Class PostQuery
 * @package common\modules\blogs\models\query
 * Класс кастомных запросов модели [[User]]
 */
class NewsQuery extends ActiveQuery
{
	/**
	 * Выбираем только активных пользователей
	 * @param ActiveQuery $query
	 */
	public function active()
	{
		$this->andWhere('status = :status', [':status' => News::STATUS_ACTIVE]);
		return $this;
	}

	/**
	 * Выбираем только неактивных пользователей
	 * @param ActiveQuery $query
	 */
	public function inactive()
	{
		$this->andWhere('status = :status', [':status' => News::STATUS_INACTIVE]);
		return $this;
	}

}