<?php
namespace common\modules\blogs\modules\categories;


/**
 * Backend-модуль [[Categories]]
 */
class Categories extends \yii\base\Module
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'common\modules\blogs\modules\categories\controllers';
}