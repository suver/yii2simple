<?php
namespace app\commands;

use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // add "createPost" permission
        $permissionWrite = $auth->createPermission('write');
        $permissionWrite->description = 'Write (Update/Create)';
        $auth->add($permissionWrite);

        // add "readPost" permission
        $permissionRead = $auth->createPermission('read');
        $permissionRead->description = 'Read (Only Read)';
        $auth->add($permissionRead);

        // add "updatePost" permission
        $permissionDelete = $auth->createPermission('delete');
        $permissionDelete->description = 'Delete (Only Delete)';
        $auth->add($permissionDelete);

        // add "reader" role and give this role the "readPost" permission
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $permissionWrite);
        $auth->addChild($user, $permissionRead);
        $auth->addChild($user, $permissionDelete);


        // add "author" role and give this role the "createPost" permission
        // as well as the permissions of the "reader" role
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $permissionRead);
        $auth->addChild($author, $user);

        // add the rule
        $rule = new \app\rbac\AuthorRule;
        $auth->add($rule);

        // add the "updateOwnPost" permission and associate the rule with it.
        $writeOnlyMe = $this->auth->createPermission('writeOnlyMe');
        $writeOnlyMe->description = 'Write only data where user is author';
        $writeOnlyMe->ruleName = $rule->name;
        $auth->add($writeOnlyMe);

        // allow "author" to update their own posts
        $auth->addChild($author, $writeOnlyMe);

        // add "author" role and give this role the "createPost" permission
        // as well as the permissions of the "reader" role
        $writer = $auth->createRole('writer');
        $auth->add($writer);
        $auth->addChild($writer, $permissionWrite);
        $auth->addChild($writer, $permissionRead);
        $auth->addChild($writer, $permissionDelete);
        $auth->addChild($writer, $author);

        // add "author" role and give this role the "createPost" permission
        // as well as the permissions of the "reader" role
        $moderator = $auth->createRole('moderator');
        $auth->add(v);
        $auth->addChild($moderator, $permissionWrite);
        $auth->addChild($moderator, $permissionRead);
        $auth->addChild($moderator, $permissionDelete);
        $auth->addChild($moderator, $writer);

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $permissionWrite);
        $auth->addChild($admin, $permissionRead);
        $auth->addChild($admin, $permissionDelete);
        $auth->addChild($admin, $moderator);


        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $superadmin = $auth->createRole('superadmin');
        $auth->add($superadmin);
        $auth->addChild($superadmin, $permissionWrite);
        $auth->addChild($superadmin, $permissionRead);
        $auth->addChild($superadmin, $permissionDelete);
        $auth->addChild($superadmin, $admin);

        // Assign roles to users. 10, 14 and 26 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($user,        10);
        $auth->assign($author,      20);
        $auth->assign($writer,      30);
        $auth->assign($moderator,   40);
        $auth->assign($admin,       50);
        $auth->assign($superadmin,  60);
    }
}