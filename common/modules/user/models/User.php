<?php
namespace common\modules\user\models;

use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\data\ActiveDataProvider;
use \yii\base\Security;

use common\modules\user\models\Activity;
use yii\base\ModelEvent;
use common\modules\user\models\query\UserQuery;
use Yii;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $update_time
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface, \suver\mailer\MailerUserInterface, \suver\activity\ActivityUserInterface
{
    public $passwordHashStrategy = 'password_hash';

    const STATUS_DELETED = -1;
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const ROLE_USER       = 10;
    const ROLE_AUTOR      = 20;
    const ROLE_WRITER     = 30;
    const ROLE_MODERATOR  = 40;
    const ROLE_ADMIN      = 50;
    const ROLE_SUPERADMIN = 60;

    const AVATAR_PATH = 'statics/avatars';
    const ASSETS_PATH = '@www/assets/images';
    const DEFAULT_AVATAR = 'statics/default/default-ava-m.png';

    /**
     * События модели.
     */
    const EVENT_AFTER_VALIDATE_SUCCESS = 'afterValidateSuccess';
    const EVENT_AFTER_REGISTER_SUCCESS = 'afterRegisterSuccess';
    const EVENT_AFTER_ACTIVATE_SUCCESS = 'afterActivateSuccess';


    /**
     * @var string Plain password. Used for model validation.
     */
    public $password;
    public $repassword;

    /**
     * @var string Current user's password.
     */
    public $current_password;

    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE      => Yii::t('user', 'Пользователь активен'),
            self::STATUS_DELETED       => Yii::t('user', 'Пользователь удален'),
            self::STATUS_INACTIVE     => Yii::t('user', 'Пользователь не активен'),
        ];
    }

    public function getStatus()
    {
        $statuses = self::getStatuses();
        return $statuses[$this->status];
    }

    public static function getRoles()
    {
        return [
            self::ROLE_USER       => Yii::t('user', 'Роль Пользователя'),
            self::ROLE_AUTOR      => Yii::t('user', 'Роль Автора'),
            self::ROLE_WRITER     => Yii::t('user', 'Роль Писателя'),
            self::ROLE_MODERATOR  => Yii::t('user', 'Роль Модератора'),
            self::ROLE_ADMIN      => Yii::t('user', 'Роль Администратора'),
            self::ROLE_SUPERADMIN => Yii::t('user', 'Роль Суперадмина'),
        ];
    }

    public function getRole()
    {
        $roles = self::getRoles();
        return $roles[$this->role];
    }

    public function getFullname()
    {
        return $this->name . ' ' . $this->surname;
    }

    public function getAvatar()
    {
        return ($this->avatar) ?  '/' . self::AVATAR_PATH . '/' . $this->avatar : '/' . self::DEFAULT_AVATAR ;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    public function init()
    {
        parent::init();
    }

    /**
     * Creates a new user
     *
     * @param  array       $attributes the attributes given by field => value
     * @return static|null the newly created model, or null on failure
     */
    public static function create($attributes)
    {
        if ($attributes['password'] == null) {
            $attributes['password'] = Password::generate(8);
        }

        /** @var User $user */
        $user = new static();
        $user->setAttributes($attributes);
        $user->setPassword($attributes['password']);
        $user->generateAuthKey();
        if ($user->save()) {



            // the following three lines were added:
            $auth = Yii::$app->authManager;
            $adminRole = $auth->getRole('author');
            $auth->assign($adminRole, $user->getId());

            return $user;
        } else {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'suver\behaviors\DatetimeBehavior',
                'attributes' => [
                    'insert' => ['create_time', 'update_time'],
                    'update' => ['update_time'],
                ],
            ],
            'avatar' => [
                'class' => 'suver\behaviors\Images',
                'rootDirectory' => Yii::getAlias('@root'),
                'storageImage' =>  self::AVATAR_PATH,
                'cacheImage' => self::ASSETS_PATH,
                'defaultImage' => self::DEFAULT_AVATAR,
                'attribute' => 'avatar',
            ],
        ];
    }

    // @see common\behaviors\DatetimeBehavior
    public function __set($name, $value)
    {
        if(in_array($name, ['create_time', 'update_time']))
        {
            $this->setAttribute($name, $this->getBehavior('timestamp')->atach($value));
        }
        parent::__set($name, $value);
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = NULL)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param  string      $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by phone
     *
     * @param  string      $phone
     * @return static|null
     */
    public static function findByPhone($phone)
    {
        return false;
    }

    /**
     * Finds user by email if user status=1
     *
     * @param  string      $email
     * @return static|null
     */
    public static function findByEmailIfActive($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by id
     *
     * @param  string      $id
     * @return static|null
     */
    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds user by id
     *
     * @param  string      $id
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default'        => ['name', 'surname', 'username', 'email', 'password', 'avatar', 'role', 'status'],
            'register'        => ['username', 'email', 'password', 'role', 'status'],
            'activate'         => ['status','auth_key'],
            'create'          => ['username', 'email', 'password', 'role'],
            'update'          => ['username', 'email', 'password', 'role', 'name', 'surname'],
            'update-password' => ['password', 'current_password'],
            'update-email'    => ['email'],
            'update-avatar'    => ['avatar'],
            // Backend scenarios
            'admin-update' => ['name', 'surname', 'username', 'email', 'password', 'repassword', 'avatar', 'status', 'role'],
            'admin-create' => ['name', 'surname', 'username', 'email', 'password', 'repassword', 'avatar', 'status', 'role'],

            // Recovery password
            'resend' => ['email'],
            'recovery' => ['email'],
            'password' => ['password', 'repassword', 'oldpassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            // status rules
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_INACTIVE, self::STATUS_ACTIVE]],

            // role and access rules
            ['role', 'default', 'value' => self::ROLE_USER],
            ['role', 'in', 'range' => [self::ROLE_USER, self::ROLE_AUTOR, self::ROLE_WRITER, self::ROLE_MODERATOR, self::ROLE_ADMIN, self::ROLE_SUPERADMIN]],

            // username rules
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            // email rules
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'on' => ['admin-create', 'create', 'register']],
            ['email', 'exist', 'on' => ['resend', 'recovery'], 'message' => Yii::t('user', 'Пользователь с указанным адресом не существует.')],

            // avatar rules
            ['avatar', 'safe'],
            ['avatar', 'file', 'extensions'=>'jpg,jpeg,gif,png', 'skipOnEmpty' => true],

            // password rules
            ['password', 'required', 'on' => ['register', 'update_password'], 'skipOnEmpty' => true],
            ['password', 'string', 'min' => 6, 'on' => ['register', 'update_password', 'create'], 'skipOnEmpty' => true],

        ];
    }

    public function attributeLabels()
    {
        return [
            'username'  => \Yii::t('app', 'Имя пользователя'),
            'email'     => \Yii::t('app', 'Email'),
            'surname'     => \Yii::t('app', 'Фамилия'),
            'name'     => \Yii::t('app', 'Имя'),
            'avatar'    => \Yii::t('app', 'Аватар'),
            'password'  => \Yii::t('app', 'Пароль'),
            'status'    => \Yii::t('app', 'Статус'),
            'role'      => \Yii::t('app', 'Роль пользователя'),
            'id'        => \Yii::t('app', 'ID'),
            'repassword' => \Yii::t('app', 'Повторить пароль'),
            'create_time' => \Yii::t('app', 'Дата создания'),
            'update_time' => \Yii::t('app', 'Дата обновления'),
        ];
    }

    // Активируем пользователя
    public function activate()
    {
        $this->status = self::STATUS_ACTIVE;
        $this->auth_key = Security::generateRandomKey();
        return $this->save(false, array('status', 'auth_key'));
    }

    // Блокируем пользователя
    public function inactivate()
    {
        $this->status = self::STATUS_INACTIVE;
        $this->auth_key = Security::generateRandomKey();
        return $this->save(false, array('status', 'auth_key'));
    }

    // Активируем пользователя
    public function createNewAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
        return $this->save(false, array('auth_key'));
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Security::generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Security::generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    // Меняем пароль пользователя
    public function changePassword($password=false)
    {
        $this->password = ($password) ? $password : Security::generateRandomKey(8);
        $this->auth_key = Security::generateRandomKey();
        $this->setPassword($this->password);
        return $this->save(false, array('password_hash', 'auth_key'));
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)) {
            // Проверяем если это новая запись
            if($this->isNewRecord) {
                // Хэшируем пароль
                if(!empty($this->password)) {
                    $this->setPassword($this->password);
                }
                if($this->scenario === 'register')
                {
                    $this->status = self::STATUS_INACTIVE;
                }
                // Генерируем уникальный ключ
                $this->auth_key = Security::generateRandomKey();
            } else {
                // Обновляем пароль если был отправлен запрос для его смены
                if($this->scenario === 'password') {
                    $this->setPassword($this->password);
                }
                // При редактировании пароля пользователя в админке, генерируем password_hash
                if($this->scenario === 'admin-update' && !empty($this->password)) {
                    $this->setPassword($this->password);
                }
                // Удаляем пользователя
                if($this->scenario === 'delete') {
                    $this->status = self::STATUS_DELETED;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // Проверяем если это новая запись
        if($this->isNewRecord) {
            if(!$this->hasErrors() && $this->scenario === 'register') {
                $event = new ModelEvent;
                $event->sender = $this;
                $this->trigger(self::EVENT_AFTER_REGISTER_SUCCESS, $event);
            }
        }
        else
        {
            if(!$this->hasErrors() && $this->scenario === 'activate') {
                $event = new ModelEvent;
                $event->sender = $this;
                $this->trigger(self::EVENT_AFTER_ACTIVATE_SUCCESS, $event);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        /* Добавляем событие модели которое доступно только после её успешной валидации.
           Это событие обычно используется для повторных отправок электронных писем. */
        if(!$this->hasErrors() && ($this->scenario === 'resend' || $this->scenario === 'recovery')) {
            $event = new ModelEvent;
            $event->sender = self::find()->where(['email' => $this->email])->one();
            $this->trigger(self::EVENT_AFTER_VALIDATE_SUCCESS, $event);
        }
        parent::afterValidate();
    }


    public function search($params, $withoutMe=false)
    {
        $query = self::find();

        if($withoutMe)
        {
            $query->withoutMe();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id'=>[
                        'label' => $this->getAttributeLabel('id'),
                    ],
                    'username'=>[
                        'label' => $this->getAttributeLabel('username'),
                    ],
                    'email'=>[
                        'label' => $this->getAttributeLabel('email'),
                    ],
                    'role'=>[
                        'label' => $this->getAttributeLabel('role'),
                    ],
                    'status'=>[
                        'label' => $this->getAttributeLabel('status'),
                    ],
                    'create_time'=>[
                        'label' => $this->getAttributeLabel('create_time'),
                    ],
                    'update_time'=>[
                        'label' => $this->getAttributeLabel('update_time'),
                    ]
                ]
                /*
                 'attributes' => [
                    'age',
                    'name' => [
                        'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                        'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Name',
                    ]
                 ]
                 */
            ],
            /*'pagination' => [
                'pageSize' => 2,
            ]*/
        ]);

        if(!empty($params['filter']))
        {
            $query->andFilterWhere(['like', 'username', $params['filter']])
                ->andFilterWhere(['like', 'email', $params['filter']]);

        }

        // load the seach form data and validate
        if(!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'creation_date', $this->creation_date]);

        return $dataProvider;
    }


    /**
     * Finds user by id
     *
     * @param  string      $id
     * @return static|null
     */
    public static function getCount()
    {
        return static::find()->addSelect('COUNT(*)')->scalar();
    }

    public function removeAvatar()
    {
        $scenario = $this->scenario;
        $this->scenario = 'update-avatar';
        $this->setAttribute('avatar', NULL);
        if($this->save())
        {
            $this->scenario = $scenario;
            return true;
        }
        else
        {
            $this->scenario = $scenario;
            return false;
        }
    }

}
