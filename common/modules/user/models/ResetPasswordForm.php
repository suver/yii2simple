<?php
namespace common\modules\user\models;

use Yii;
use yii\base\Model;
use yii\base\ModelEvent;
use common\modules\user\models\User;

/**
 * Login form
 */
class ResetPasswordForm extends Model
{
    public $repassword;
    public $password;

    protected $email;
    protected $auth_key;

    private $_user = false;

    /**
     * События модели.
     */
    const EVENT_RESET_PASSWORD_SUCCESS = 'afterValidateSuccess';

    public function __construct($email, $auth_key)
    {
        $this->email = $email;
        $this->auth_key = $auth_key;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['repassword', 'password'], 'required'],
            ['password', 'string', 'min' => 6, 'skipOnEmpty' => true],
            ['repassword', 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors())
        {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password))
            {
                $this->addError('password', Yii::t('user','Не верный пароль'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function recoveryPassword()
    {
        $user = $this->getUser();
        if ($this->validate() && $user)
        {
            if($user->changePassword($this->password))
            {
                $event = new ModelEvent;
                $event->sender = $this->getUser();
                $this->trigger(self::EVENT_RESET_PASSWORD_SUCCESS, $event);
                return true;
            }
            return false;
        }
        else
        {
            var_dump($this->getErrors());exit;
            return false;
        }
    }

    public function isCurrentPasrams()
    {
        return $this->getUser() ? true : false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false)
        {
            $this->_user = User::find()->where(['and', 'email = :email', 'auth_key = :auth_key'], [':email' => $this->email, ':auth_key' => $this->auth_key])->active()->one();
        }

        return $this->_user;
    }
}
