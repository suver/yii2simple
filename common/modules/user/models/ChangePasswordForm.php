<?php
namespace common\modules\user\models;

use Yii;
use yii\base\Model;
use yii\base\ModelEvent;
use common\modules\user\models\User;

/**
 * Login form
 */
class ChangePasswordForm extends Model
{
    public $new_password;
    public $new_repassword;
    public $password;

    protected $email;
    protected $auth_key;

    private $_user = false;

    /**
     * События модели.
     */
    const EVENT_RESET_PASSWORD_SUCCESS = 'afterValidateSuccess';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['new_password', 'new_repassword'], 'required'],
            [['new_password', 'new_repassword', 'password'], 'string', 'min' => 6, 'skipOnEmpty' => true],
            ['new_repassword', 'compare', 'compareAttribute' => 'new_password', 'skipOnEmpty' => false],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password'  => \Yii::t('app', 'Текущий пароль'),
            'new_password'     => \Yii::t('app', 'Новый пароль'),
            'new_repassword'    => \Yii::t('app', 'Повторите пароль'),
        ];
    }

    /**
     * Проверяем правильность пароля
     */
    public function validatePassword($user_id=false)
    {
        if (!$this->hasErrors())
        {
            $user = $this->getUser($user_id);
            if (!$user || !$user->validatePassword($this->password))
            {
                $this->addError('password', Yii::t('user','Не верный пароль'));
            }
        }
    }

    /**
     * Меняем пароль
     *
     * @return boolean whether the user is logged in successfully
     */
    public function changePassword($user_id=false)
    {
        $user = $this->getUser($user_id);
        if ($this->validate() && $user)
        {
            if($user->changePassword($this->new_password))
            {
                $event = new ModelEvent;
                $event->sender = $this->getUser();
                $this->trigger(self::EVENT_RESET_PASSWORD_SUCCESS, $event);

                // Указываем activity
                Yii::$app->activity->set('changePassword', Yii::t('user', '{master_user} изменил(a) свой пароль'), [], Yii::$app->user->id);

                Yii::$app->mailer->send([
                    'to'=>User::findById(1),
                    'message' => 'Вы изменили пароль',
                    'subject' => 'Вы изменили пароль',
                    'template' => 'ChangePasswordFromProfileSuccess',
                    'user' => [
                        'email' => $user->email,
                        'password' => $this->new_password,
                        'fullname' => $user->getFullname(),
                    ],
                ], 'default');

                return true;
            }
        }
        else
        {
            //var_dump($this->getErrors());exit;
            return false;
        }
    }

    /**
     * Вернум пользователя
     *
     * @return User|null
     */
    public function getUser($user_id=false)
    {
        if ($this->_user === false)
        {
            if(!$user_id AND Yii::$app->user->isGuest)
            {
                return false;
            }
            if(!$user_id)
            {
                $this->_user = User::findById(Yii::$app->user->id);
            }
            else
            {
                $this->_user = User::findById($user_id);
            }
        }

        return $this->_user;
    }
}
