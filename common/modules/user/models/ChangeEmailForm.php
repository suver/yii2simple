<?php
namespace common\modules\user\models;

use Yii;
use yii\base\Model;
use yii\base\ModelEvent;
use common\modules\user\models\User;
use yii\helpers\Url;

/**
 * Login form
 */
class ChangeEmailForm extends Model
{
    public $email;
    public $password;

    private $_user = false;

    public $key;
    public $user;

    /**
     * События модели.
     */
    const EVENT_CHANGE_EMAIL_SUCCESS = 'afterValidateSuccess';
    const EVENT_SEND_EMAIL_SUCCESS = 'afterSendSuccess';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required', 'on' => 'start'],
            [['user', 'key'], 'required', 'on' => 'confirm'],
            ['user', 'integer'],
            [['key', 'user'], 'safe'],
            ['key', 'string', 'max' => 50 ],
            ['password', 'string', 'min' => 6, 'skipOnEmpty' => true],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password'  => \Yii::t('app', 'Текущий пароль'),
            'email'     => \Yii::t('app', 'Новый Email'),

            'key'     => \Yii::t('app', 'Ключь доступа'),
            'user'     => \Yii::t('app', 'Пользователь'),
        ];
    }

    /**
     * Проверяем правильность пароля
     */
    public function validatePassword($user_id=false)
    {
        if (!$this->hasErrors())
        {
            $user = $this->getUser($user_id);
            if (!$user || !$user->validatePassword($this->password))
            {
                $this->addError('password', Yii::t('user','Не верный пароль'));
            }
        }
    }

    /**
     * Шлем уведомление о том что небходимо подтвердить смену email
     * @return bool
     */
    public function sendChangeLinkToEmail($user_id=false)
    {
        $user = $this->getUser($user_id);
        if ($this->validate() && $user)
        {
            $event = new ModelEvent;
            $event->sender = $this->getUser();
            $this->trigger(self::EVENT_SEND_EMAIL_SUCCESS, $event);

            if(Yii::$app->temporaryStorage->set($user->getAuthKey(), $this->email, 'change_mail', $user->id))
            {
                // Указываем activity
                Yii::$app->activity->set('sendChangeLinkToEmail', Yii::t('user', '{master_user} запросил(a) ссылку для изменения email'), [], Yii::$app->user->id);

                Yii::$app->mailer->send([
                    'to' => User::findById(1),
                    'message' => 'Вы хотите изменить ваш email на новый',
                    'subject' => 'Вы хотите изменить ваш email на новый',
                    'template' => 'ChangeEmailFromProfile',
                    'link' => Url::toRoute(['/user/default/changemail', 'key' => $user->getAuthKey(), 'user' => $user->id], true),
                    'key' => $user->getAuthKey(),
                    'user' => [
                        'fullname' => $user->getFullname()
                    ],
                ], 'default');

                return true;
            }
        }
        //var_dump($this->getErrors());exit;
        return false;
    }

    /**
     * Меняем email пользователя
     *
     * @return boolean whether the user is logged in successfully
     */
    public function changeUserEmail()
    {
        $user = $this->getUser($this->user);
        if ($this->validate() && $user->auth_key == $this->key)
        {
            // меняем email

            $event = new ModelEvent;
            $event->sender = $this->getUser();
            $this->trigger(self::EVENT_CHANGE_EMAIL_SUCCESS, $event);

            if($email = Yii::$app->temporaryStorage->get($this->key, 'change_mail', $user->id))
            {
                $oldEmail = $user->email;
                $user->scenario = 'update-email';
                $user->setAttribute('email', $email);
                if($user->save()) {
                    Yii::$app->temporaryStorage->delete($this->key, 'change_mail', $user->id);

                    // Указываем activity
                    Yii::$app->activity->set('changeUserEmail', Yii::t('user', '{master_user} изменил(a) свой email'), [], Yii::$app->user->id);

                    Yii::$app->mailer->send([
                        'to' => User::findById(1),
                        'message' => 'Вы изменили ваш email на новый',
                        'subject' => 'Вы изменили ваш email на новый',
                        'template' => 'ChangeEmailFromProfileSuccess',
                        'user' => [
                            'oldEmail' => $oldEmail,
                            'email' => $user->email,
                            'fullname' => $user->getFullname()
                        ],
                    ], 'default');

                    Yii::$app->mailer->send([
                        'to' => $oldEmail,
                        'message' => 'Вы изменили ваш email на новый',
                        'subject' => 'Вы изменили ваш email на новый',
                        'template' => 'ChangeEmailFromProfileSuccess',
                        'user' => [
                            'oldEmail' => $oldEmail,
                            'email' => $user->email,
                            'fullname' => $user->getFullname()
                        ],
                    ], 'default');
                    return true;
                }
            }
        }
        //var_dump($this->getErrors());exit;
        return false;
    }

    /**
     * Вернум пользователя
     *
     * @return User|null
     */
    public function getUser($user_id=false)
    {
        if ($this->_user === false)
        {
            if(!$user_id AND Yii::$app->user->isGuest)
            {
                return false;
            }
            if(!$user_id)
            {
                $this->_user = User::findById(Yii::$app->user->id);
            }
            else
            {
                $this->_user = User::findById($user_id);
            }
        }

        return $this->_user;
    }
}
