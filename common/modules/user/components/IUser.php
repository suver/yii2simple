<?php

namespace common\modules\user\components;

use common\modules\user\models\User;
use Yii;

class IUser extends \yii\web\User
{

    public function init()
    {
        parent::init();

        $this->on(\yii\web\User::EVENT_AFTER_LOGIN, function($identity){
            if($identity) {
                //var_dump($identity->identity);
                // Указываем activity
                Yii::$app->activity->set('login', Yii::t('user', '{master_user} авторизовался', [], $identity->identity->getId()));
            }
        });

        $this->on(\yii\web\User::EVENT_BEFORE_LOGOUT, function($identity){
            // Указываем activity
            Yii::$app->activity->set('logout', Yii::t('user', '{master_user} вышел', [], $identity->identity->id));
        });
    }

    public function activity($action, $text, $params=[], $slave_user_id=false)
    {
        if(!Yii::$app->user->isGuest)
        {
            return Yii::$app->activity->set($action, $text, $params, Yii::$app->user->identity->getId(), $slave_user_id);
        }
        return false;
    }

    public function getFullname()
    {
        if(!Yii::$app->user->isGuest)
        {
            return User::findById(Yii::$app->user->identity->id)->getFullname();
        }
        return false;
    }
}

