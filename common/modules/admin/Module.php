<?php

namespace common\modules\admin;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\admin\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
