<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $user
 */

$Mailer->setSubject('Вы изменили пароль');

?>

<p>Ув, <?php echo $message['user']['fullname'] ?></p>

<p>Вы, успешно изменили пароль. Ваш новый пароль: <?php echo $message['user']['password'] ?></p>

<?php echo $message['message'] ?>