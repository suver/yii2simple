<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $user
 */

$Mailer->setSubject('Вы изменили Email');

?>

<p>Ув, <?php echo $message['user']['fullname'] ?></p>

<p>Вы, успешно изменили email c <?php echo $message['user']['oldEmail'] ?> на <?php echo $message['user']['email'] ?></p>

<?php echo $message['message'] ?>