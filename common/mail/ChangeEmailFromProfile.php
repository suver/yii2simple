<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\User $user
 */

$Mailer->setSubject('Вы пытаетесь изменить Email');

?>

<p>Ув, <?php echo $message['user']['fullname'] ?></p>

<p>Вы, или кто то другой пытаетесь изменить email, привязанный к аккаунту.</p>

<p>Что бы изменить ваш текущий email на новый необходимо перейти по ссылке <a href="<?php echo $message['link'] ?>"><?php echo $message['link'] ?></a></p>

<?php echo $message['message'] ?>