<?php

namespace suver\files\models;

use Yii;
use suver\files\ModuleTrait;
use yii\helpers\Security;
use suver\helpers\FileHelper;


/**
 * This is the model class for table "suver_images_images".
 *
 * @property integer $id
 * @property integer $item_id
 * @property string $object
 * @property string $title
 * @property string $snippet
 * @property string $file
 * @property integer $status
 * @property integer $download
 * @property string $create_time
 * @property string $update_time
 */
class Files extends \yii\db\ActiveRecord
{

    use ModuleTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE = -1;
    const STATUS_TEMP = -2;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $module = self::getModuleInstanse();

        return [
            'timestamp' => [
                'class' => 'suver\behaviors\DatetimeBehavior',
                'attributes' => [
                    'insert' => ['create_time', 'update_time'],
                    'update' => ['update_time'],
                ],
            ],
            'files' => [
                'class' => 'suver\behaviors\Files',
                'rootDirectory' => FileHelper::path($module->rootDirectory, ['owner' => $this->object]),
                'storageFile' =>  FileHelper::path($module->storageFile, ['owner' => $this->object]),
                'cacheFile' => FileHelper::path($module->cacheFile, ['owner' => $this->object]),
                'attribute' => 'file',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suver_files_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file', 'name', 'mime', 'ext', 'size', 'status', 'create_time', 'update_time'], 'required'],
            [['item_id', 'size', 'status', 'download'], 'integer'],
            [['title', 'snippet'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['object'], 'string', 'max' => 255],
            [['title', 'file'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('suver/files', 'ID'),
            'item_id' => Yii::t('suver/files', 'Item ID'),
            'object' => Yii::t('suver/files', 'Object'),
            'title' => Yii::t('suver/files', 'Название'),
            'snippet' => Yii::t('suver/files', 'Описание'),
            'image' => Yii::t('suver/files', 'Изоображение'),
            'name' => Yii::t('suver/files', 'Имя файла'),
            'mime' => Yii::t('suver/files', 'Mime тип'),
            'ext' => Yii::t('suver/files', 'Расширение'),
            'size' => Yii::t('suver/files', 'Размер'),
            'status' => Yii::t('suver/files', 'Статус'),
            'download' => Yii::t('suver/files', 'Скачиваний'),
            'create_time' => Yii::t('suver/files', 'Создан'),
            'update_time' => Yii::t('suver/files', 'Обновлен'),
        ];
    }



    public function afterFind() {

        parent::afterFind();

        $Module = \Yii::$app->getModule('files');

        $Image = $this->getBehavior('files');


        $Image->cacheFile = FileHelper::path($Module->cacheFile, ['owner' => $this->object]);
        $Image->storageFile = FileHelper::path($Module->storageFile, ['owner' => $this->object]);
        $Image->rootDirectory = FileHelper::path($Module->rootDirectory, ['owner' => $this->object]);

        $this->attachBehavior('files', $Image);
    }

    public static function add($file, $title=NULL, $snippet=NULL, $status=self::STATUS_ACTIVE, $object=NULL, $item_id=NULL){

        $module = self::getModuleInstanse();

        $ext = end(explode(".", $file->name));

        $storageFile = FileHelper::path($module->storageFile);
        $rootDirectory = FileHelper::path($module->rootDirectory);

        FileHelper::directoryCreator($rootDirectory, $storageFile);

        $name = Security::generateRandomKey() . "." . $ext;

        $file->saveAs($rootDirectory . '/' . $storageFile . '/' . $name);

        $model = new self;

        $model->file = $name;
        $model->name = $file->baseName;
        $model->ext = $file->extension;
        $model->mime = $file->type;
        $model->size = $file->size;
        $model->title = $title;
        $model->snippet = $snippet;
        $model->status = $status;
        $model->object = $object;
        $model->item_id = $item_id;

        if($model->save()){
            return $model;
        }
        else {
            return false;
        }
    }

    public static function findById($id){
        return self::findOne(["id" => $id]);
    }
}
