<?php

namespace suver\files;


use yii;

class Module extends \yii\base\Module
{
    public $storageFiles;
    public $storageTemporary;
    public $cacheFiles;
    public $rootDirectory;
    public $defaultFiles;
    public $attribute = 'image';
    public $controllerNamespace = 'suver\files\controllers';
    public $enableCrsf = true;

    public $className = 'suver\files\Module';

    public function init()
    {
        parent::init();
        $this->registerTranslations();

        // custom initialization code goes here
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['suver/files'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@suver/files/messages',
        ];
    }
}
