<?php
/**
 * Created by PhpStorm.
 * User: suver
 * Date: 21.09.14
 * Time: 19:49
 */

namespace suver\files\widgets;

/**
 * Class ActiveForm
 * @package suver\widgets
 *
 */


use yii;
use yii\base\Widget;
use yii\helpers\Html;


class UploadFiles extends Widget
{
    public $image;
    public $id = false;
    public $name = NULL;
    public $field = 'File';
    public $value = '';
    public $url = '/files/upload/add';
    public $height = '200';
    public $width = '200';
    public $model;

    public function init()
    {
        parent::init();

        $this->registerTranslations();
        $view = $this->getView();
        \suver\files\UploadFilesAsset::register($view);
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['suver/files'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@suver/files/messages',
        ];
    }

    public function run()
    {
        $owner = '';
        if(is_object($this->model)) {
            $owner = end(explode('\\', $this->model->className()));
        }


        $element_name = ($this->name) ? $this->name : $owner . '[' . $this->field . ']' ;

        $element_id = ($this->id) ? $this->id : $this->name . '_' . $this->id ;

        $view = $this->getView();
        $view->registerJs('

        ');


        return $this->render('UploadFiles', array(
            'element_name' => $element_name,
            'element_id' => $element_id,
            'url' => $this->url,
            'field' => $this->field,
            'owner' => $owner,
            'model' => $this->model,
        ));
    }
}