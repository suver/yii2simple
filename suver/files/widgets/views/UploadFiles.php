<!-- Multiple file uploader with header -->

<div class="panel panel-primary" data-UploadFiles>
    <input type="hidden" name="<?php echo \Yii::$app->request->csrfParam ?>" value="<?php echo \Yii::$app->request->getCsrfToken() ?>" data-csr-tocken="<?php echo $element_id ?>">
    <input type="hidden" name="<?php echo $element_name ?>" data-url="<?php echo $url ?>" value="" data-UploadFiles-field>
    <div class="panel-heading">
        <h6 class="panel-title"><i class="icon-upload3"></i> <?php echo \Yii::t('suver/files', 'Прикрепленные файлы') ?></h6>
    </div>
    <ul class="list-group list-file"
        data-UploadFiles-list="<?php echo $element_id ?>"
        id="<?php echo $element_id ?>"
        data-url="<?php echo $url ?>"
        data-element-name="<?php echo $element_name ?>"
        data-owner="<?php echo $owner ?>">

        <li class="list-group-item has-button">
            <i class="icon-file-pdf"></i> <a href="#">Lorem_ipsum_dolor.pdf</a> <a href="#" class="btn btn-link btn-icon"><i class="icon-download"></i></a>
        </li>

    </ul>
    <button class="btn btn-primary" data-UploadFiles-add="<?php echo $element_id ?>"><?php echo \Yii::t('suver/files', 'Добавить файл') ?></button>
</div>

<!-- /multiple file uploader with header -->
