<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace suver\files;

use yii;
use yii\web\AssetBundle;

/**
 * This asset bundle provides the javascript files for client validation.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UploadFilesAsset extends AssetBundle
{
    public $sourcePath = '@suver/files/assets';

    public $js = [
        'fileupload/jquery.fileupload.js',
        'UploadFiles.js',
    ];
    public $css = [
        //'passfield/css/passfield.min.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
