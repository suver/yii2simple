Виджеты для проектов на Yii2
===========================
Добавляют различные возможности для проектов на Yii


Installation
-------------
1. Добавьте Yii2-files в ваш composer.json файл:
    <pre>
       {
            "require": {
                "suver/yii2-files": "dev-master"
            }
       }
    </pre>

2. Запустите
    <pre>
      php composer.phar update
    </pre>

3. Запустите
    <pre>
    php yii migrate/up --migrationPath=suver/files/migrations
    </pre>


4. Добавьте в файл настроек
    ```php
    'components' => [
            'files' => [
                'class' => 'suver\files\Module',
                'cacheFile' => '@www/assets/files/{owner}',
                'storageFile' => 'statics/files/{owner}',
                'storageTemporary' => 'statics/tmp/{owner}',
                'rootDirectory' => Yii::getAlias('@root'),
            ],
        ],
    ```



Usage
-----
