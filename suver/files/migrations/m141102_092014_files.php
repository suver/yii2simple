<?php

use yii\db\Schema;

class m141102_092014_files extends \yii\db\Migration
{
    public function up()
    {
        // Настройки MySql таблицы
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        // Таблица постов
        $this->createTable('{{%suver_files_files}}', [
            'id' => Schema::TYPE_PK,
            'item_id' => Schema::TYPE_INTEGER . ' NULL DEFAULT NULL',
            'object' => Schema::TYPE_STRING . ' NULL DEFAULT NULL',
            'title' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'snippet' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
            'file' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'name' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'mime' => Schema::TYPE_STRING . '(15) NULL DEFAULT NULL',
            'ext' => Schema::TYPE_STRING . '(15) NULL DEFAULT NULL',
            'size' => Schema::TYPE_INTEGER . '(25) NULL DEFAULT NULL',
            'status' => 'tinyint(2) NOT NULL DEFAULT 1',
            'download' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'create_time' => Schema::TYPE_DATETIME . ' NOT NULL',
            'update_time' => Schema::TYPE_DATETIME . ' NOT NULL',
        ], $tableOptions);

        // Индексы
        $this->createIndex('item_id', '{{%suver_files_files}}', 'item_id');
        $this->createIndex('object', '{{%suver_files_files}}', 'object');
        $this->createIndex('title', '{{%suver_files_files}}', 'title');
        $this->createIndex('status', '{{%suver_files_files}}', 'status');
        $this->createIndex('download', '{{%suver_files_files}}', 'download');
        $this->createIndex('create_time', '{{%suver_files_files}}', 'create_time');
        $this->createIndex('update_time', '{{%suver_files_files}}', 'update_time');
    }

    public function down()
    {
        $this->dropTable('{{%suver_files_files}}');
    }
}
