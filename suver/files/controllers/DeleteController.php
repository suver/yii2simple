<?php

namespace suver\files\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for News model.
 */
class Delete extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['delete'],
                ],
            ],
        ];
    }


    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id)
    {
        if(Yii::$app->request->isDelete){
            $params = Yii::$app->request->delete('FileUpload');
            if(empty($params)){
                return Json::encode(['success' => false, 'error' => Yii::t('news', 'Не переданы парметры запроса')]);
            }

            // Удаляем картинку новости
            Yii::$app->activity->setMe('changeMyAvatar', Yii::t('user', '{master_user} удалил(a) картинку новости {news_title}', [
                'news_id' => $model->news_id,
                'news_title' => $model->title,
                'objects' => [[ 'class' => 'News','news_id' => $model->news_id]]
            ]));

            $model->scenario = 'update-image';
            if($model->save()){
                return Json::encode(['success' => true]);
            }
            else {
                return Json::encode(['success' => false, 'error' => $model->getErrors()]);
            }
        }


        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}
