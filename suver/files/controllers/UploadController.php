<?php

namespace suver\files\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use \suver\helpers\FileHelper;
use suver\files\models\Files;
use yii\helpers\Security;

/**
 * DefaultController implements the CRUD actions for News model.
 */
class UploadController extends Controller
{
    public $enableCsrfValidation = true;

    public function init()
    {
        $this->enableCsrfValidation = $this->module->enableCrsf;
        parent::init();
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionInsert()
    {
        // $this->module

        // Меняем картинку новости
        if(Yii::$app->request->isPost) {
            $params = Yii::$app->request->post();
            if (isset($params)) {

                $image = UploadedFile::getInstanceByName('file');

                FileHelper::setParams($params);
                $image = Images::add($image, NULL, NULL, Images::STATUS_ACTIVE, $params['owner']);

                return Json::encode(['success' => true, 'filelink' => $_image]);
            }
            return Json::encode(['success' => false, 'error' => Yii::t('suver/files', 'Не переданы параметры. Вы должны передать загружаемое изоображение.')]);
        }
        return Json::encode(['success' => false, 'error' => Yii::t('suver/files', 'Вы должены отправить POST запрос')]);
    }



    /**
     * Добавляет превью
     * @return JSON
     */
    public function actionAdd()
    {
        // $this->module

        // Меняем картинку новости
        if(Yii::$app->request->isPost AND Yii::$app->request->post('elements', false)) {
            $element = Yii::$app->request->post('elements', false);
            $params = Yii::$app->request->post();

            if (isset($params)) {
                $_files = [];
                $_ids = [];
                $files = UploadedFile::getInstancesByName('file');

                FileHelper::setParams($params);

                foreach($files as $file) {
                    $_file = Files::add($file, NULL, NULL, Files::STATUS_ACTIVE, $params['owner']);
                    $_ids[] = $_file->id;
                }

                return Json::encode(['success' => true, 'files' => $_files, 'elements' => $_ids]);
            }
            return Json::encode(['success' => false, 'error' => Yii::t('suver/files', 'Не переданы параметры. Вы должны передать загружаемое изоображение.')]);
        }
        return Json::encode(['success' => false, 'error' => Yii::t('suver/files', 'Вы должены отправить POST запрос')]);
    }
}
