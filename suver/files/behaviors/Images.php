<?php

namespace suver\images\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\helpers\Security;
use suver\helpers\FileHelper;

class Images extends \suver\behaviors\Images
{
    public $storageImage;
    public $cacheImage;
    public $rootDirectory;
    public $defaultImage;
    public $attribute;

    public function events()
    {
        return [
            ActiveRecord::EVENT_INIT => 'init',
            ActiveRecord::EVENT_BEFORE_INSERT => 'before_save',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'before_save',
            ActiveRecord::EVENT_AFTER_INSERT => 'after_save',
            ActiveRecord::EVENT_AFTER_UPDATE => 'after_save',
            ActiveRecord::EVENT_AFTER_FIND => 'load',
            ActiveRecord::EVENT_BEFORE_DELETE => 'delete',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'before_validate',
            ActiveRecord::EVENT_AFTER_VALIDATE => 'after_validate',
        ];
    }

    public function beforeInit(){

        $Module = \Yii::$app->getModule('images');

        if($this->owner) {
            $object = end(explode("\\", get_class($this->owner)));

            $this->cacheImage = FileHelper::path($Module->cacheImage, ['owner' => $object]);
            $this->storageImage = FileHelper::path($Module->storageImage, ['owner' => $object]);
            $this->rootDirectory = FileHelper::path($Module->rootDirectory, ['owner' => $object]);
            $this->defaultImage = FileHelper::path($Module->defaultImage, ['owner' => $object]);
        }

        $this->attribute = $Module->attribute;

        if($this->owner) {
            $this->owner->{$this->attribute} = new ImagesObj(false, $this);
        }
    }

    public function attachImage($absolutePath)
    {
        $this->beforeAttachImage();
        if(!preg_match('#http#', $absolutePath)){
            if (!file_exists($absolutePath)) {
                throw new \Exception('File not exist! :'.$absolutePath);
            }
        }else{
            //nothing
        }

        $old_file = $this->model->getOldAttribute($this->attribute);

        $storageImage = str_replace("@www", "@webroot", $this->storageImage);

        $ext = end(explode(".", $absolutePath));

        // generate a unique file name
        $this->model->{$this->attribute} = Security::generateRandomKey().".{$ext}";

        if(!file_exists( $path = $this->rootDirectory . '/' . Yii::getAlias($storageImage)))
        {
            $dirs = explode("/", Yii::getAlias($storageImage));
            $path = $this->rootDirectory;
            foreach($dirs as $dir)
            {
                $path = $path . '/' . $dir;
                if(!file_exists($path)) {
                    @mkdir($path);
                }
            }
        }

        // the path to save file, you can set an uploadPath
        // in Yii::$app->params (as used in example below)
        $path = Yii::getAlias($storageImage) . '/' . $this->model->{$this->attribute} . '/' . $this->model->{$this->attribute};

        if(copy($absolutePath, $this->rootDirectory . '/' . $path))
        {
            if(!empty($old_file))
            {
                @unlink($this->rootDirectory . '/' . Yii::getAlias($storageImage) . '/' . $old_file);
            }
        }
        $this->afterAttachImage();

        $this->save();

        return $path;
    }

    public function before_validate(){
        if(is_object($this->owner->{$this->attribute}))
            $this->owner->{$this->attribute} = $this->owner->{$this->attribute}->getValue();
    }

    public function after_validate(){
        $this->load();
    }

    public function before_save()
    {
        if(is_object($this->owner->{$this->attribute}))
            $this->owner->{$this->attribute} = $this->owner->{$this->attribute}->getValue();
    }

    public function after_save() {

        $this->load();
    }

    public function load()
    {
        $this->beforeLoad();

        $Module = \Yii::$app->getModule('images');

        if($this->owner) {
            $object = end(explode("\\", get_class($this->owner)));

            $this->cacheImage = FileHelper::path($Module->cacheImage, ['owner' => $object]);
            $this->storageImage = FileHelper::path($Module->storageImage, ['owner' => $object]);
            $this->rootDirectory = FileHelper::path($Module->rootDirectory, ['owner' => $object]);
            $this->defaultImage = FileHelper::path($Module->defaultImage, ['owner' => $object]);
        }

        if($this->owner AND !is_object($this->owner->{$this->attribute})) {
            $this->setValue($this->owner->{$this->attribute});
        }

        if($this->model) {
            $this->owner->{$this->attribute} = new ImagesObj((string) $this->model->{$this->attribute}, $this);
        }
        else {
            $this->owner->{$this->attribute} = new ImagesObj(false, $this);
        }
        $this->afterLoad();
    }

    public function beforeLoad(){
        $this->model = \suver\images\models\Images::findById($this->owner->attributes[$this->attribute]);
    }

    public function beforeDelete()
    {
        $this->model = \suver\images\models\Images::findById($this->owner->attributes[$this->attribute]);
    }

    public function afterDelete()
    {
        $this->model->delete();
    }

}


class ImagesObj extends \suver\behaviors\ImagesObj {


    public function __toString()
    {
        return ($this->getValue()) ? (string) $this->getValue() : $this->image ;
    }
}