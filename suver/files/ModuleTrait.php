<?php
/**
 * Created by PhpStorm.
 * User: kostanevazno
 * Date: 17.07.14
 * Time: 0:20
 */

namespace suver\files;


use yii\base\Exception;

trait ModuleTrait
{
    /**
     * @var null|\suver\images\Module
     */
    private static $_module;

    /**
     * @return null|\suver\images\Module
     */
    protected static function getModuleInstanse()
    {
        if (self::$_module == null) {
            self::$_module = \Yii::$app->getModule('files');
        }

        if(!self::$_module){
            throw new Exception("\n\n\n\n\nYii2 files module not found, may be you didn't add it to your config?\n\n\n\n");
        }

        return self::$_module;
    }
}