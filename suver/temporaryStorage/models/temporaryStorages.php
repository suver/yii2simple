<?php

namespace suver\temporaryStorage\models;

use Yii;

/**
 * This is the model class for table "temporary_storage".
 *
 * @property integer $id
 * @property integer $item_id
 * @property string $key
 * @property string $type
 * @property string $data
 * @property string $date_created
 */
class temporaryStorages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suver_temporary_storage';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'suver\behaviors\DatetimeBehavior',
                'attributes' => [
                    'insert' => ['date_created'],
                    'update' => ['date_created'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key','data', 'date_created'], 'required'],
            [['item_id'], 'integer'],
            [['data'], 'string'],
            [['date_created'], 'safe'],
            [['key', 'type'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('suver/temporaryStorage', 'ID'),
            'item_id' => Yii::t('suver/temporaryStorage', 'Item ID'),
            'key' => Yii::t('suver/temporaryStorage', 'Key'),
            'type' => Yii::t('suver/temporaryStorage', 'Type'),
            'data' => Yii::t('suver/temporaryStorage', 'Data'),
            'date_created' => Yii::t('suver/temporaryStorage', 'Date Created'),
        ];
    }
}
