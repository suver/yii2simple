<?php

use yii\db\Schema;

class m140921_112343_temporary_storage extends \yii\db\Migration
{
    public function up()
    {
        // Настройки MySql таблицы
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        // Таблица для смены e-mail адресов пользователя
        $this->createTable('{{%suver_temporary_storage}}', [
            'id' => Schema::TYPE_PK,
            'item_id' => Schema::TYPE_INTEGER . ' NULL DEFAULT NULL',
            'key' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'type' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'data' => Schema::TYPE_TEXT . ' NOT NULL',
            'date_created' => Schema::TYPE_DATETIME . ' NOT NULL',
        ], $tableOptions);

        // Индексы
        $this->createIndex('item_id', '{{%suver_temporary_storage}}', 'item_id');
        $this->createIndex('date_created', '{{%suver_temporary_storage}}', 'date_created');
        $this->createIndex('key', '{{%suver_temporary_storage}}', 'key');
        $this->createIndex('type', '{{%suver_temporary_storage}}', 'type');

    }

    public function down()
    {

        $this->dropTable('{{%suver_temporary_storage}}');
    }
}
