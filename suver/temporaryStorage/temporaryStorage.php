<?php

namespace suver\temporaryStorage;

use \yii;
use \yii\base\BootstrapInterface;
use \yii\base\Application;
use suver\temporaryStorage\models\temporaryStorages;

class temporaryStorage extends \yii\base\Widget
{
    public function bootstrap($app)
    {
        $app->on(Application::EVENT_BEFORE_REQUEST, function () {
            // do something here
        });
    }

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function test()
    {
        return true;
    }

    public function set($key, $data, $type=NULL, $item_id=false)
    {
        $paramSearch['key'] = $key;

        if(!empty($type)) {
            $paramSearch['type'] = $type;
        }
        else {
            $paramSearch['type'] = NULL;
        }

        if(!empty($item_id)) {
            $paramSearch['item_id'] = $item_id;
        }
        else {
            $paramSearch['item_id'] = NULL;
        }

        $temporaryStorage = temporaryStorages::findOne($paramSearch);
        if(!$temporaryStorage) {
            $temporaryStorage = new temporaryStorages();
            $temporaryStorage->key = $key;
            $temporaryStorage->item_id = $item_id;
            $temporaryStorage->type = $type;
            $temporaryStorage->data = serialize($data);
        }
        else {
            $temporaryStorage->data = serialize($data);
            $temporaryStorage->date_created = date("Y-m-d H:i:s");
        }

        if ($temporaryStorage->save()) {
            return true;
        }

        var_dump($temporaryStorage->getErrors());
        exit;
    }

    public function get($key, $type=NULL, $item_id=false)
    {
        $paramSearch['key'] = $key;

        if(!empty($type)) {
            $paramSearch['type'] = $type;
        }
        else {
            $paramSearch['type'] = NULL;
        }

        if(!empty($item_id)) {
            $paramSearch['item_id'] = $item_id;
        }
        else {
            $paramSearch['item_id'] = NULL;
        }

        $temporaryStorages = temporaryStorages::findOne($paramSearch);
        if($temporaryStorages)
        {
            return unserialize($temporaryStorages->data);
        }
        else
        {
            return false;
        }
    }

    public function delete($key, $type=NULL, $item_id=false)
    {
        $paramSearch['key'] = $key;

        if(!empty($type)) {
            $paramSearch['type'] = $type;
        }
        else {
            $paramSearch['type'] = NULL;
        }

        if(!empty($item_id)) {
            $paramSearch['item_id'] = $item_id;
        }
        else {
            $paramSearch['item_id'] = NULL;
        }

        $temporaryStorages = temporaryStorages::findOne($paramSearch);
        if($temporaryStorages) {
            return $temporaryStorages->delete();
        }
        else
        {
            return false;
        }
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['suver/temporaryStorage'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@suver/temporaryStorage/messages',
        ];
    }
}
