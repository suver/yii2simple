<?php

namespace suver\behaviors;

use yii\db\BaseActiveRecord;
use yii\db\Expression;

/**
 *
 * use common\behaviors\AutoSnippet;
 *
 * public function behaviors()
 * {
 *     return [
 *         AutoSnippet::className(),
 *     ];
 * }
 *
 * public function behaviors()
 * {
 *     return [
 *         'snippet' => [
 *             'class' => AutoSnippet::className(),
 *             'attributes' => [
 *                 'from_field' => 'to_field',
 *             ],
 *         ],
 *     ];
 * }
 * ```
 *
 *
 * Добавив в класс метод можно получить автообновление занчения снипета выделяя из текста первый кусок обраленный тегом <snippet>
 */
class AutoSnippet extends \yii\behaviors\AttributeBehavior
{

    public $attributes = [
        'text' => 'snippet',
    ];

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'update',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'update',
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'update',
        ];
    }

    public function update($event)
    {
        foreach($this->attributes as $from=>$to)
        {
            $text = $this->owner->{$from};

            $snippet = NULL;
            if(preg_match("/<snippet>(.*)<\/snippet>/", $text, $match)) {
                $snippet = $match['1'];
            }

            $this->owner->{$to} = $snippet;
        }
    }

}