<?php

namespace suver\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\helpers\Security;
use suver\helpers\FileHelper;
use suver\helpers\Generator;

class Images extends Behavior
{
    public $storageImage;
    public $cacheImage;
    public $rootDirectory;
    public $defaultImage;
    public $attribute;

    protected $value;
    protected $model;

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_INIT => 'init',
            ActiveRecord::EVENT_BEFORE_INSERT => 'save',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'save',
            ActiveRecord::EVENT_AFTER_FIND => 'load',
            ActiveRecord::EVENT_AFTER_DELETE => 'delete',
        ];
    }


    public function beforeInit(){
        $this->model = $this->owner;
    }

    public function init()
    {
        $this->beforeInit();

        ImagesObj::createAdditionPath($this->storageImage);

        if($this->owner) {
            $this->owner->{$this->attribute} = new ImagesObj(false, $this);
        }

        $this->afterInit();
    }

    public function afterInit(){

    }


    public function beforeAttachImage(){

    }

    public function attachImage($absolutePath)
    {
        $this->beforeAttachImage();
        if(!preg_match('#http#', $absolutePath)){
            if (!file_exists($absolutePath)) {
                throw new \Exception('File not exist! :'.$absolutePath);
            }
        }else{
            //nothing
        }

        $old_file = $this->model->getOldAttribute($this->attribute);

        $storageImage = str_replace("@www", "@webroot", $this->storageImage);

        $ext = end(explode(".", $absolutePath));

        // generate a unique file name
        $this->model->{$this->attribute} = Security::generateRandomKey().".{$ext}";

        if(!file_exists( $path = $this->rootDirectory . '/' . Yii::getAlias($storageImage)))
        {
            $dirs = explode("/", Yii::getAlias($storageImage));
            $path = $this->rootDirectory;
            foreach($dirs as $dir)
            {
                $path = $path . '/' . $dir;
                if(!file_exists($path)) {
                    @mkdir($path);
                }
            }
        }

        // the path to save file, you can set an uploadPath
        // in Yii::$app->params (as used in example below)
        $path = Yii::getAlias($storageImage) . '/' . $this->model->{$this->attribute} . '/' . $this->model->{$this->attribute};

        if(copy($absolutePath, $this->rootDirectory . '/' . $path))
        {
            if(!empty($old_file))
            {
                @unlink($this->rootDirectory . '/' . Yii::getAlias($storageImage) . '/' . $old_file);
            }
        }
        $this->afterAttachImage();

        $this->load();

        return $path;
    }

    public function afterAttachImage(){

    }


    public function beforeSave(){

    }

    public function save()
    {
        $this->beforeSave();

        $image = UploadedFile::getInstance($this->model, $this->attribute);
        if($image AND $image->getBaseName())
        {
            $old_file = $this->model->getOldAttribute($this->attribute);

            $storageImage = str_replace("@www", "@webroot", $this->storageImage);

            $ext = end(explode(".", $image->name));
            // generate a unique file name
            $this->owner->{$this->attribute} = Generator::randomString().".{$ext}";

            if(!file_exists( $path = $this->rootDirectory . '/' . Yii::getAlias($storageImage)))
            {
                $dirs = explode("/", Yii::getAlias($storageImage));
                $path = $this->rootDirectory;
                foreach($dirs as $dir)
                {
                    $path = $path . '/' . $dir;
                    if(!file_exists($path)) {
                        @mkdir($path);
                    }
                }
            }

            // the path to save file, you can set an uploadPath
            // in Yii::$app->params (as used in example below)
            $path = $this->rootDirectory . '/' . Yii::getAlias($storageImage) . '/' . $this->model->{$this->attribute};

            if($image->saveAs($path))
            {
                if(!empty($old_file))
                {
                    @unlink($this->rootDirectory . '/' . Yii::getAlias($storageImage) . '/' . $old_file);
                }
            }
        }

        $this->afterSave();

        $this->load();
    }

    public function afterSave(){

    }

    public function beforeLoad(){
        $this->model = $this->owner;
    }

    public function load()
    {
        $this->beforeLoad();

        if($this->owner AND !is_object($this->owner->{$this->attribute})) {
            $this->setValue($this->owner->{$this->attribute});
        }

        if($this->model) {
            $this->owner->{$this->attribute} = new ImagesObj((string) $this->model->{$this->attribute}, $this);
        }
        else {
            $this->owner->{$this->attribute} = new ImagesObj(false, $this);
        }

        $this->afterLoad();
    }

    public function afterLoad(){

    }


    public function beforeDelete(){

    }

    public function delete()
    {
        $this->beforeDelete();
        $storageImage = str_replace("@www", "@webroot", $this->storageImage);

        @unlink($this->rootDirectory . '/' . Yii::getAlias($storageImage) . '/' . $this->model->{$this->attribute});

        $this->afterDelete();

        $this->load();
    }

    public function afterDelete(){

    }
}



class ImagesObj
{
    protected $image;
    protected $behavior;


    public function __construct($image, $obj)
    {
        $this->image = $image;
        $this->behavior = $obj;
    }

    public function __toString()
    {
        return ($this->image) ? $this->image : '' ;
    }

    public function getValue()
    {
        return $this->behavior->getValue();
    }

    public function getModel()
    {
        return $this->behavior->owner;
    }

    public function getImage($size=false, $type='resize')
    {
        if(!in_array($type, array('crop', 'resize')))
        {
            throw new Exception('Incorrect type');
        }

        $width  = NULL ;
        $height = NULL ;
        if($size)
        {
            List($width, $height) = explode("x", $size);
            $width  = ($width  != '-') ? $width  : NULL ;
            $height = ($height != '-') ? $height : NULL ;
        }

        $webroot = str_replace("@www", "@webroot", $this->behavior->cacheImage . '/' . $size . '/' . $type);
        $web = str_replace("@www", "@web", $this->behavior->cacheImage . '/' . $size . '/' . $type);

        if(!empty($this->image))
        {
            if(!file_exists(Yii::getAlias($webroot) . '/' . $this->image))
            {
                $storageImage = str_replace("@www", "@webroot", $this->behavior->storageImage);
                if(!empty($this->image) AND file_exists($this->behavior->rootDirectory . '/' . $storageImage . '/' . $this->image))
                {
                    $image_path = $this->image;
                    $image = Yii::$app->image->load($this->behavior->rootDirectory . '/' . $storageImage . '/' . $this->image);
                }
                else
                {
                    $image_path = $this->behavior->defaultImage;
                    $image = Yii::$app->image->load($this->behavior->rootDirectory . '/' . $this->behavior->defaultImage);
                }

                $image->{$type}($width, $height);

                ImagesObj::createAdditionPath(dirname(Yii::getAlias($webroot) . '/' . $image_path));

                $image->save(Yii::getAlias($webroot) . '/' . $image_path, $quality = 100);

                return Yii::getAlias($web) . '/' . $image_path;
            }
            else
            {
                return Yii::getAlias($web) . '/' . $this->image;
            }
        }
        else
        {
            $image_path = $this->behavior->defaultImage;

            $image = Yii::$app->image->load($this->behavior->rootDirectory . '/' . $this->behavior->defaultImage);

            $image->{$type}($width, $height);

            ImagesObj::createAdditionPath(dirname(Yii::getAlias($webroot) . '/' . $image_path));
            $image->save(Yii::getAlias($webroot) . '/' . $image_path, $quality = 100);

            return Yii::getAlias($web) . '/' . $image_path;
        }
    }

    public static function createAdditionPath($path)
    {
        if(!file_exists($path))
        {
            $list = explode("/", $path);
            $_folder = '';
            foreach($list as $folder)
            {
                $_folder .= $folder . '/';
                if(!file_exists($_folder))
                {
                    mkdir($_folder);
                }
            }
        }
    }
}