<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace suver\behaviors;

use yii\db\BaseActiveRecord;
use yii\db\Expression;

/**
 *
 * use common\behaviors\DatetimeBehavior;
 *
 * public function behaviors()
 * {
 *     return [
 *         DatetimeBehavior::className(),
 *     ];
 * }
 *
 * public function behaviors()
 * {
 *     return [
 *         'timestamp' => [
 *             'class' => DatetimeBehavior::className(),
 *             'attributes' => [
 *                 ActiveRecord::EVENT_BEFORE_INSERT => 'creation_time',
 *                 ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
 *             ],
 *             'value' => new Expression('NOW()'),
 *         ],
 *     ];
 * }
 * ```
 *
 *
 * Добавив в класс метод можно получить автообновление занчения и присваивание объекта уже при указании параметра напрямую
 * // @see common\behaviors\DatetimeBehavior
 * public function __set($name, $value)
 * {
 *      if(in_array($name, ['create_time', 'update_time']))
 *      {
 *          $this->setAttribute($name, $this->getBehavior('timestamp')->atach($value));
 *      }
 *      parent::__set($name, $value);
 * }
 */
class DatetimeBehavior extends \yii\behaviors\AttributeBehavior
{
    /**
     * @var array list of attributes that are to be automatically filled with timestamps.
     * The array keys are the ActiveRecord events upon which the attributes are to be filled with timestamps,
     * and the array values are the corresponding attribute(s) to be updated. You can use a string to represent
     * a single attribute, or an array to represent a list of attributes.
     * The default setting is to update both of the `created_at` and `updated_at` attributes upon AR insertion,
     * and update the `updated_at` attribute upon AR updating.
     */
    public $attributes = [
        'insert' => ['created_at', 'updated_at'],
        'update' => 'updated_at',
    ];

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_INIT => 'evaluateAttributes',
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'evaluateAttributes',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'evaluateAttributes',
            BaseActiveRecord::EVENT_AFTER_INSERT => 'evaluateAttributes',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'evaluateAttributes',
            BaseActiveRecord::EVENT_AFTER_FIND => 'evaluateAttributes',
        ];
    }

    /**
     * Evaluates the attribute value and assigns it to the current attributes.
     * @param Event $event
     */
    public function evaluateAttributes($event)
    {
        switch($event->name)
        {
            case BaseActiveRecord::EVENT_BEFORE_INSERT:
            case BaseActiveRecord::EVENT_AFTER_INSERT:
                $attributes = (array) $this->attributes['insert'];
                break;
            case BaseActiveRecord::EVENT_BEFORE_UPDATE:
            case BaseActiveRecord::EVENT_AFTER_UPDATE:
                $attributes = (array) $this->attributes['update'];
                break;
            default:
                $attributes = (array) $this->attributes['insert'] + (array) $this->attributes['update'];

        }

        foreach($attributes as $attribute)
        {
            $value = ($this->owner->$attribute) ? $this->owner->$attribute : date("Y-m-d H:i:s");
            if(in_array($event->name, [BaseActiveRecord::EVENT_BEFORE_UPDATE, BaseActiveRecord::EVENT_AFTER_UPDATE]))
            {
                if(is_object($this->owner->$attribute))
                {
                    $oldAttribute = new DatetimeObj($event->sender->getOldAttribute($attribute));
                    if($oldAttribute->getTimestamp() == $this->owner->$attribute->getTimestamp())
                    {
                        $value = date("Y-m-d H:i:s");
                    }
                }
                else
                {
                    if($event->sender->getOldAttribute($attribute) == $this->owner->$attribute)
                    {
                        $value = date("Y-m-d H:i:s");
                    }
                }
            }
            else if(in_array($event->name, [BaseActiveRecord::EVENT_AFTER_FIND]))
            {
                $value = $this->owner->$attribute;
            }

            $this->owner->$attribute = new DatetimeObj($value);
        }
    }

    public function atach($value)
    {
        return new DatetimeObj($value);
    }

}


class DatetimeObj extends \DateTime
{
    protected $translate = array(
        'ru' => array(
            "am" => "дп",
            "pm" => "пп",
            "AM" => "ДП",
            "PM" => "ПП",
            "Monday" => "Понедельник",
            "Mon" => "Пн",
            "Tuesday" => "Вторник",
            "Tue" => "Вт",
            "Wednesday" => "Среда",
            "Wed" => "Ср",
            "Thursday" => "Четверг",
            "Thu" => "Чт",
            "Friday" => "Пятница",
            "Fri" => "Пт",
            "Saturday" => "Суббота",
            "Sat" => "Сб",
            "Sunday" => "Воскресенье",
            "Sun" => "Вс",
            "January" => "Января",
            "Jan" => "Янв",
            "February" => "Февраля",
            "Feb" => "Фев",
            "March" => "Марта",
            "Mar" => "Мар",
            "April" => "Апреля",
            "Apr" => "Апр",
            "May" => "Мая",
            "May" => "Мая",
            "June" => "Июня",
            "Jun" => "Июн",
            "July" => "Июля",
            "Jul" => "Июл",
            "August" => "Августа",
            "Aug" => "Авг",
            "September" => "Сентября",
            "Sep" => "Сен",
            "October" => "Октября",
            "Oct" => "Окт",
            "November" => "Ноября",
            "Nov" => "Ноя",
            "December" => "Декабря",
            "Dec" => "Дек",
            "st" => "ое",
            "nd" => "ое",
            "rd" => "е",
            "th" => "ое",

            "year" => "год",
            "month" => "месяц",
            "day" => "дней",
            "hour" => "часов",
            "minute" => "минут",
            "second" => "секунд",
            "seconds" => "секунд",
            "ago" => "назад",
        ),
    );

    function format($format, $lang='ru') {
        $date = parent::format($format);
        return strtr($date, $this->translate[$lang]);
    }

    function ago($lang='ru')
    {
        $etime = time() - $this->getTimestamp();

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
            30 * 24 * 60 * 60       =>  'month',
            24 * 60 * 60            =>  'day',
            60 * 60                 =>  'hour',
            60                      =>  'minute',
            1                       =>  'second'
        );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return strtr($r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago', $this->translate[$lang]);
            }
        }
    }

    /**
     * String magic method
     * @return string the DB expression
     */
    public function __toString()
    {
        return $this->format("Y-m-d H:i:s");
    }

}