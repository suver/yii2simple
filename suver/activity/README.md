Activity для проектов на Yii2
===========================
Добавляет возможность логирования действий (активность) пользователей в БД.


Installation
-------------
1. Добавьте Yii2-activity в ваш composer.json файл:
    <pre>
       {
            "require": {
                "suver/yii2-activity": "dev-master"
            }
       }
    </pre>

2. Запустите
    <pre>
      php composer.phar update
    </pre>

3. Запустите
    <pre>
    php yii migrate/up --migrationPath=suver/activity/migrations
    </pre>


4. Добавьте в файл настроек
    ```php
    'components' => [
            'activity' => [
                'class' => 'suver\activity\Activity',
            ],
        ],
    ```



Usage
-----

Для занесения информации в БД достаточно вызвать

    ```php
        Yii::$app->activity->set('ТИП ДЕЙСТВИЯ', 'ОПИСАНИЕ');  // тип действия, короткий вызов. Если пользователь авторизован, то действие будет записано авторизованному пользователю

        // или

        Yii::$app->activity->set($action, 'ОПИСАНИЕ {variable}', ['variable'=>11], ID_ТЕКУЩЕГО_ПОЛЬЗОВТАЛЕЯ, ID_ПОЛЬЗОВАТЕЛЯ НАД КОТОРЫМ ПРОИЗВЕЛИ ДЕЙСТВИЕ); // тип действия, 3 последних праметра не обязательны и любой может быть опущен
    ```