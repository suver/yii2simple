<?php

use yii\db\Schema;

class m140826_222959_create_activity extends \yii\db\Migration
{
    public function safeUp()
    {
        // Настройки MySql таблицы
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        // Таблица комментариев
        $this->createTable('{{%suver_activity}}', array(
            'id' => Schema::TYPE_PK,
            'master_user_id' => Schema::TYPE_INTEGER.' NULL DEFAULT NULL',
            'slave_user_id' => Schema::TYPE_INTEGER.' NULL DEFAULT NULL',
            'action' => Schema::TYPE_STRING.' NULL DEFAULT NULL',
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'params' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
            'time' => Schema::TYPE_DATETIME.' NOT NULL',
        ), $tableOptions);

        // Индексы
        $this->createIndex('master_user_id', '{{%suver_activity}}', 'master_user_id');
        $this->createIndex('slave_user_id', '{{%suver_activity}}', 'slave_user_id');
        $this->createIndex('action', '{{%suver_activity}}', 'action');
        $this->createIndex('time', '{{%suver_activity}}', 'time');

        // Связи
        //$this->addForeignKey('FK_activity_user', '{{%activity}}', 'user_id', '{{%users}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%suver_activity}}');
    }
}
