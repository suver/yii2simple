<?php
namespace suver\activity;

/**
 * Created by PhpStorm.
 * User: suver
 * Date: 17.09.14
 * Time: 20:46
 */

interface ActivityUserInterface
{
    public function getId();
    public function getFullname();

    public static function findById($id);
    public static function findByPhone($phone);
}