<?php

namespace suver\activity;

use \yii;
use \yii\base\BootstrapInterface;
use \yii\base\Application;
use suver\activity\models\Activitys;

class Activity extends \yii\base\Widget
{
    public $user;

    public function bootstrap($app)
    {
        $app->on(Application::EVENT_BEFORE_REQUEST, function () {
            // do something here
        });
    }

    public function init()
    {
        parent::init();
        Activitys::setConf(['user'=>$this->user]);
        $this->registerTranslations();
    }

    public function test()
    {
        return true;
    }

    public function set($action, $text, $params=[], $master_user_id=NULL, $slave_user_id=NULL)
    {
        if(!$master_user_id AND !Yii::$app->user->isGuest)
        {
            $master_user_id = Yii::$app->user->id;
        }

        $params['master_user_id'] = $master_user_id;
        $params['slave_user_id'] = $slave_user_id;

        $Activity = new Activitys();
        $Activity->master_user_id = $master_user_id;
        $Activity->slave_user_id = $slave_user_id;
        $Activity->action = $action;
        $Activity->text = $text;
        $Activity->params = serialize($params);
        $Activity->time = date("Y-m-d H:i:s");

        if($Activity->save())
        {
            return true;
        }
        else
        {
            var_dump($Activity->getErrors());exit;
        }
    }



    public function setMe($action, $text, $params=[], $slave_user_id=NULL)
    {
        if(Yii::$app->user->isGuest)
        {
            throw new \Exception('Вы не можете занести эту активность неавторизованному пользователю');
        }

        $master_user_id = NULL;
        if(!Yii::$app->user->isGuest)
        {
            $master_user_id = Yii::$app->user->id;
        }

        $params['master_user_id'] = $master_user_id;
        $params['slave_user_id'] = $slave_user_id;

        $Activity = new Activitys();
        $Activity->master_user_id = Yii::$app->user->id;
        $Activity->slave_user_id = $slave_user_id;
        $Activity->action = $action;
        $Activity->text = $text;
        $Activity->params = serialize($params);
        $Activity->time = date("Y-m-d H:i:s");

        if($Activity->save())
        {
            return true;
        }
        else
        {
            var_dump($Activity->getErrors());exit;
        }
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['suver/activity'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@suver/activity/messages',
        ];
    }

    public function getForUser($user_id)
    {
        Activitys::setConf(['user'=>$this->user]);
        return Activitys::find()->orWhere(['master_user_id'=>$user_id])->orWhere(['slave_user_id'=>$user_id]);
    }

    public function get()
    {
        Activitys::setConf(['user'=>$this->user]);
        return Activitys::find();
    }

    public function getMessage()
    {
        Activitys::setConf(['user'=>$this->user]);
        return Activitys::find();
    }
}
