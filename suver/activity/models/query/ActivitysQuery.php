<?php
namespace suver\activity\models\query;

use yii\db\ActiveQuery;
use suver\activity\models\Activitys;

/**
 * Class PostQuery
 * @package common\modules\blogs\models\query
 * Класс кастомных запросов модели [[User]]
 */
class ActivitysQuery extends ActiveQuery
{
    /**
     * Выбираем только активных пользователей
     * @param ActiveQuery $query
     */
    public function active()
    {
        $this->andWhere('status = :status', [':status' => User::STATUS_ACTIVE]);
        return $this;
    }

    /**
     * Выбираем только неактивных пользователей
     * @param ActiveQuery $query
     */
    public function inactive()
    {
        $this->andWhere('status = :status', [':status' => User::STATUS_INACTIVE]);
        return $this;
    }

    /**
     * Выбираем только забаненных пользователей
     * @param ActiveQuery $query
     */
    public function banned()
    {
        $this->andWhere('status = :status', [':status' => User::STATUS_INACTIVE]);
        return $this;
    }

    /**
     * Выбираем только простых пользователей
     * @param ActiveQuery $query
     */
    public function registered()
    {
        $this->andWhere('role = :role_user', [':role_user' => User::ROLE_USER]);
        return $this;
    }


    /**
     * Выбираем только простых пользователей
     * @param ActiveQuery $query
     */
    public function withoutMe()
    {
        $this->andWhere('id NOT IN('.\Yii::$app->user->id.')');

        return $this;
    }
}