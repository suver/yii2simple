<?php

namespace suver\activity\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property integer $master_user_id
 * @property integer $slave_user_id
 * @property string $action
 * @property string $text
 * @property string $params
 * @property string $time
 */
class Activitys extends \yii\db\ActiveRecord
{
    private static $_conf;

    public static function setConf($conf=[])
    {
        self::$_conf = $conf;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suver_activity';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'suver\behaviors\DatetimeBehavior',
                'attributes' => [
                    'insert' => ['time'],
                    'update' => ['time'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action', 'text', 'params', 'time'], 'required'],
            [['master_user_id', 'slave_user_id'], 'integer'],
            [['text', 'params'], 'string'],
            [['time'], 'safe'],
            [['action'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('suver/activity', 'ID'),
            'master_user_id' => Yii::t('suver/activity', 'Master User ID'),
            'slave_user_id' => Yii::t('suver/activity', 'Slave User ID'),
            'action' => Yii::t('suver/activity', 'Action'),
            'text' => Yii::t('suver/activity', 'Text'),
            'params' => Yii::t('suver/activity', 'Params'),
            'time' => Yii::t('suver/activity', 'Time'),
        ];
    }

    public function getIcon()
    {
        switch($this->action)
        {
            case "login":
                return 'icon-key';
                break;
            case "logout":
                return 'icon-exit';
                break;
            case "editMyInfo":
            case "changeMyAvatar":
            case "deleteMyAvatar":
                return 'icon-pencil3';
                break;
            case "editUserInfo":
            case "changeUserAvatar":
            case "deleteUserAvatar":
                return 'icon-pencil3';
                break;
            default:
                return 'icon-plus-circle';
        }
    }

    public function getMasterUser()
    {
        $userClass = self::$_conf['user']['class'];

        if(new $userClass instanceof ActivityUserInterface)
        {
            $findMethod = 'findById';
            return $userClass::findById($this->master_user_id);
        }
        else
        {
            return $userClass::findOne([self::$_conf['user']['fields']['id'] => $this->master_user_id]);
        }
    }

    public function getSlaveUser()
    {
        $userClass = self::$_conf['user']['class'];

        if(new $userClass instanceof ActivityUserInterface)
        {
            $findMethod = 'findById';
            return $userClass::findById($this->slave_user_id);
        }
        else
        {
            return $userClass::findOne([self::$_conf['user']['fields']['id'] => $this->slave_user_id]);
        }
    }

    protected function findUser($field, $value)
    {
        $userClass = self::$_conf['user']['class'];

        if(new $userClass instanceof ActivityUserInterface)
        {
            $findMethod = 'findBy' . $field;
            return $userClass::$findMethod($value);
        }
        else
        {
            return $userClass::findOne([self::$_conf['user']['fields'][$field] => $value]);
        }
    }

    public function getMessage()
    {
        $params = unserialize($this->params);

        if(!empty(self::$_conf['user']) AND !empty(self::$_conf['user']['class']))
        {
            $UserClass = self::$_conf['user']['class'];
            $_UserObj = new $UserClass;

            $MasterUser = $this->findUser('id', $this->master_user_id);
            $SalveUser  = $this->findUser('id', $this->slave_user_id);

            if($_UserObj instanceof ActivityUserInterface)
            {
                if($this->slave_user_id)
                    $params['slave_user'] = $SalveUser->getFullname();

                if($this->master_user_id)
                    $params['master_user'] = $MasterUser->getFullname();
            }
            else
            {
                if($this->slave_user_id)
                    $params['slave_user'] = $SalveUser->{self::$_conf['user']['fields']['fullname']};

                if($this->master_user_id)
                    $params['master_user'] = $MasterUser->{self::$_conf['user']['fields']['fullname']};
            }
        }

        return Yii::t('suver/activity', $this->text, $params);
    }
}
