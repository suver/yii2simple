Виджеты для проектов на Yii2
===========================
Добавляют различные возможности для проектов на Yii


Installation
-------------
1. Добавьте Yii2-activity в ваш composer.json файл:
    <pre>
       {
            "require": {
                "suver/yii2-images": "dev-master"
            }
       }
    </pre>

2. Запустите
    <pre>
      php composer.phar update
    </pre>

3. Запустите
    <pre>
    php yii migrate/up --migrationPath=suver/images/migrations
    </pre>


4. Добавьте в файл настроек
    ```php
    'components' => [
        'images' => [
            'class' => 'suver\images\Module',
            'cacheImage' => '@www/assets/images/{owner}',
            'storageImage' => 'statics/images/{owner}',
            'storageTemporary' => 'statics/tmp/{owner}',
            'rootDirectory' => Yii::getAlias('@root'),
            'defaultImage' => 'statics/default/{owner}.png',
        ],
    ],
    ```



Usage
-----
