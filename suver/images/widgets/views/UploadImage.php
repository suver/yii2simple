<div class="thumbnail" id="<?php echo $element_id ?>" data-element-id="<?php echo $element_id ?>" data-element-name="<?php echo $element_name ?>" data-owner="<?php echo $owner ?>">
    <input type="file" name="ImageFile[]" data-url="<?php echo $url ?>" data-UploadImage-field style="display: none;">
    <input type="hidden" name="<?php echo $element_name ?>" data-url="<?php echo $url ?>" value="<?php echo $value ?>" data-UploadImage-id>
    <input type="hidden" name="<?php echo Yii::$app->request->csrfParam ?>" value="<?php echo Yii::$app->request->getCsrfToken() ?>" data-csr-tocken="<?php echo $element_id ?>">
    <div class="thumb">
        <img src="<?php echo $image ?>" data-UploadImage-size="<?php echo $width ?>x<?php echo $height ?>">
        <div class="thumb-loader-progress-container" data-UploadImage-loader-progres="">
            <div class="progress progress-container progress-striped active" data-UploadImage-progress="">
                <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;"><span class="sr-only">85%</span></div>
            </div>
        </div>
        <div class="thumb-options">
            <span>
                <a href="#" class="btn btn-icon btn-success" data-change-UploadImage="<?php echo $element_id ?>"><i class="icon-pencil"></i></a>
                <a href="#" class="btn btn-icon btn-success" data-remove-UploadImage="<?php echo $element_id ?>"><i class="icon-remove"></i></a>
            </span>
        </div>
    </div>
    <!--div class="caption text-center">
        <h6>
            Денис Бутко <small>suver</small>
        </h6>
    </div-->
</div>