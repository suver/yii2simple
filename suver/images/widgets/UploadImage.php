<?php
/**
 * Created by PhpStorm.
 * User: suver
 * Date: 21.09.14
 * Time: 19:49
 */

namespace suver\images\widgets;

/**
 * Class ActiveForm
 * @package suver\widgets
 *
 */


use yii;
use yii\base\Widget;
use yii\helpers\Html;


class UploadImage extends Widget
{
    public $image;
    public $id = false;
    public $name = NULL;
    public $field = 'File';
    public $value = '';
    public $url = '/images/upload/add';
    public $height = '200';
    public $width = '200';
    public $model;

    public function init()
    {
        parent::init();

        $this->registerTranslations();
        $view = $this->getView();
        \suver\images\UploadImageAsset::register($view);
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['suver/images'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@suver/images/messages',
        ];
    }

    public function run()
    {
        $owner = '';
        if(is_object($this->model)) {
            $owner = end(explode('\\', $this->model->className()));
        }


        $element_name = ($this->name) ? $this->name : $owner . '[' . $this->field . ']' ;

        $element_id = ($this->id) ? $this->id : $this->name . '_' . $this->id ;

        if(is_object($this->model->{$this->field}) AND empty($value)){
            if(in_array(get_class($this->model->{$this->field}), ['suver\images\behaviors\ImagesObj', 'suver\behaviors\ImagesObj'])){
                $value = $this->model->{$this->field}->getValue();
            }
        }
        else {
            $value = ($this->value) ? $this->value : $this->model->{$this->field};
        }

        return $this->render('UploadImage', array(
            'image' => $this->image,
            'element_name' => $element_name,
            'element_id' => $element_id,
            'url' => $this->url,
            'height' => $this->height,
            'width' => $this->width,
            'field' => $this->field,
            'owner' => $owner,
            'model' => $this->model,
            'value' => $value,
        ));
    }
}