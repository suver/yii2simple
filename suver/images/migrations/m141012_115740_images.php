<?php

use yii\db\Schema;

class m141012_115740_images extends \yii\db\Migration
{
    public function up()
    {
        // Настройки MySql таблицы
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        // Таблица постов
        $this->createTable('{{%suver_images_images}}', [
            'id' => Schema::TYPE_PK,
            'item_id' => Schema::TYPE_INTEGER . ' NULL DEFAULT NULL',
            'object' => Schema::TYPE_STRING . ' NULL DEFAULT NULL',
            'title' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'snippet' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
            'image' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'name' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'mime' => Schema::TYPE_STRING . '(15) NULL DEFAULT NULL',
            'ext' => Schema::TYPE_STRING . '(15) NULL DEFAULT NULL',
            'size' => Schema::TYPE_INTEGER . '(25) NULL DEFAULT NULL',
            'status' => 'tinyint(2) NOT NULL DEFAULT 1',
            'views' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'create_time' => Schema::TYPE_DATETIME . ' NOT NULL',
            'update_time' => Schema::TYPE_DATETIME . ' NOT NULL',
        ], $tableOptions);

        // Индексы
        $this->createIndex('item_id', '{{%suver_images_images}}', 'item_id');
        $this->createIndex('object', '{{%suver_images_images}}', 'object');
        $this->createIndex('title', '{{%suver_images_images}}', 'title');
        $this->createIndex('status', '{{%suver_images_images}}', 'status');
        $this->createIndex('views', '{{%suver_images_images}}', 'views');
        $this->createIndex('create_time', '{{%suver_images_images}}', 'create_time');
        $this->createIndex('update_time', '{{%suver_images_images}}', 'update_time');
    }

    public function down()
    {
        $this->dropTable('{{%suver_images_images}}');
    }
}
