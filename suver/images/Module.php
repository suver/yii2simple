<?php

namespace suver\images;


use yii;

class Module extends \yii\base\Module
{
    public $storageImage;
    public $storageTemporary;
    public $cacheImage;
    public $rootDirectory;
    public $defaultImage;
    public $attribute = 'image';
    public $controllerNamespace = 'suver\images\controllers';
    public $enableCrsf = true;

    public $className = 'suver\image\Module';

    public function init()
    {
        parent::init();
        $this->registerTranslations();

        // custom initialization code goes here
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['suver/images'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@suver/images/messages',
        ];
    }
}
