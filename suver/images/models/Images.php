<?php

namespace suver\images\models;

use Yii;
use suver\images\ModuleTrait;
use yii\base\Security;
use suver\helpers\FileHelper;
use suver\helpers\Generator;


/**
 * This is the model class for table "suver_images_images".
 *
 * @property integer $id
 * @property integer $item_id
 * @property string $object
 * @property string $title
 * @property string $snippet
 * @property string $image
 * @property integer $status
 * @property integer $views
 * @property string $create_time
 * @property string $update_time
 */
class Images extends \yii\db\ActiveRecord
{

    use ModuleTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETE = -1;
    const STATUS_TEMP = -2;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $module = self::getModuleInstanse();

        return [
            'timestamp' => [
                'class' => 'suver\behaviors\DatetimeBehavior',
                'attributes' => [
                    'insert' => ['create_time', 'update_time'],
                    'update' => ['update_time'],
                ],
            ],
            'image' => [
                'class' => 'suver\behaviors\Images',
                'rootDirectory' => FileHelper::path($module->rootDirectory, ['owner' => $this->object]),
                'storageImage' =>  FileHelper::path($module->storageImage, ['owner' => $this->object]),
                'cacheImage' => FileHelper::path($module->cacheImage, ['owner' => $this->object]),
                'defaultImage' => FileHelper::path($module->defaultImage, ['owner' => $this->object]),
                'attribute' => 'image',
            ],
            /*
            'image' => [
                'class' => 'suver\behaviors\Images',
                'rootDirectory' => $module->rootDirectory,
                'storageImage' =>  $module->storageImage,
                'cacheImage' => $module->cacheImage,
                'defaultImage' => $module->defaultImage,
                'attribute' => 'image',
            ],
            */
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suver_images_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'name', 'mime', 'ext', 'size', 'status', 'create_time', 'update_time'], 'required'],
            [['item_id', 'size', 'status', 'views'], 'integer'],
            [['title', 'snippet'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['object'], 'string', 'max' => 255],
            [['title', 'image'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('suver/images', 'ID'),
            'item_id' => Yii::t('suver/images', 'Item ID'),
            'object' => Yii::t('suver/images', 'Object'),
            'title' => Yii::t('suver/images', 'Название'),
            'snippet' => Yii::t('suver/images', 'Описание'),
            'image' => Yii::t('suver/images', 'Изоображение'),
            'name' => Yii::t('suver/images', 'Имя файла'),
            'mime' => Yii::t('suver/images', 'Mime тип'),
            'ext' => Yii::t('suver/images', 'Расширение'),
            'size' => Yii::t('suver/images', 'Размер'),
            'status' => Yii::t('suver/images', 'Статус'),
            'views' => Yii::t('suver/images', 'Просмотров'),
            'create_time' => Yii::t('suver/images', 'Создан'),
            'update_time' => Yii::t('suver/images', 'Обновлен'),
        ];
    }



    public function afterFind() {

        parent::afterFind();

        $Module = \Yii::$app->getModule('images');

        $Image = $this->getBehavior('image');


        $Image->cacheImage = FileHelper::path($Module->cacheImage, ['owner' => $this->object]);
        $Image->storageImage = FileHelper::path($Module->storageImage, ['owner' => $this->object]);
        $Image->rootDirectory = FileHelper::path($Module->rootDirectory, ['owner' => $this->object]);
        $Image->defaultImage = FileHelper::path($Module->defaultImage, ['owner' => $this->object]);

        $this->attachBehavior('image', $Image);
    }

    public static function add($image, $title=NULL, $snippet=NULL, $status=self::STATUS_ACTIVE, $object=NULL, $item_id=NULL){

        $module = self::getModuleInstanse();

        $ext = end(explode(".", $image->name));

        $storageImage = FileHelper::path($module->storageImage);
        $rootDirectory = FileHelper::path($module->rootDirectory);

        FileHelper::directoryCreator($rootDirectory, $storageImage);

        $name = Generator::randomString() . "." . $ext;

        $image->saveAs($rootDirectory . '/' . $storageImage . '/' . $name);

        $model = new self;

        $model->image = $name;
        $model->name = $image->baseName;
        $model->ext = $image->extension;
        $model->mime = $image->type;
        $model->size = $image->size;
        $model->title = $title;
        $model->snippet = $snippet;
        $model->status = $status;
        $model->object = $object;
        $model->item_id = $item_id;

        if($model->save()){
            return $model;
        }
        else {
            return false;
        }
    }

    public static function findById($id){
        return self::findOne(["id" => $id]);
    }
}
