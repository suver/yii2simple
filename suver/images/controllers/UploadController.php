<?php

namespace suver\images\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use \suver\helpers\FileHelper;
use suver\images\models\Images;
use yii\helpers\Security;

/**
 * DefaultController implements the CRUD actions for News model.
 */
class UploadController extends Controller
{
    public $enableCsrfValidation = true;

    public function init()
    {
        $this->enableCsrfValidation = $this->module->enableCrsf;
        parent::init();
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionInsert()
    {
        // $this->module

        // Меняем картинку новости
        if(Yii::$app->request->isPost) {
            $params = Yii::$app->request->post();
            $size = Yii::$app->request->post('size', '400x400');
            if (isset($params)) {

                $image = UploadedFile::getInstanceByName('file');

                FileHelper::setParams($params);
                $image = Images::add($image, NULL, NULL, Images::STATUS_ACTIVE, $params['owner']);
                $_image = $image->image->getImage($size);

                return Json::encode(['success' => true, 'filelink' => $_image]);
            }
            return Json::encode(['success' => false, 'error' => Yii::t('suver/images', 'Не переданы параметры. Вы должны передать загружаемое изоображение.')]);
        }
        return Json::encode(['success' => false, 'error' => Yii::t('suver/images', 'Вы должены отправить POST запрос')]);
    }



    /**
     * Добавляет превью
     * @return JSON
     */
    public function actionAdd()
    {
        // $this->module

        // Меняем картинку новости
        if(Yii::$app->request->isPost AND Yii::$app->request->post('elements', false)) {
            $element = Yii::$app->request->post('elements', false);
            $size = Yii::$app->request->post('size', false);
            $params = Yii::$app->request->post();

            if (isset($params)) {
                $_images = [];
                $_ids = [];
                $images = UploadedFile::getInstancesByName('ImageFile');

                FileHelper::setParams($params);

                foreach($images as $image) {
                    $_image = Images::add($image, NULL, NULL, Images::STATUS_ACTIVE, $params['owner']);
                    $_images[] = $_image->image->getImage($size);
                    $_ids[] = $_image->id;
                }

                return Json::encode(['success' => true, 'images' => $_images, 'elements' => $_ids]);
            }
            return Json::encode(['success' => false, 'error' => Yii::t('suver/images', 'Не переданы параметры. Вы должны передать загружаемое изоображение.')]);
        }
        return Json::encode(['success' => false, 'error' => Yii::t('suver/images', 'Вы должены отправить POST запрос')]);
    }
}
