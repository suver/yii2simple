<?php

namespace suver\images\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use \suver\helpers\FileHelper;
use suver\images\models\Images;
use yii\helpers\Security;

/**
 * DefaultController implements the CRUD actions for News model.
 */
class ListController extends Controller
{

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionJson()
    {

        //FileHelper::setParams(['owner' => 'Images']);
        $Images = Images::find()->all();
        foreach($Images as $Image) {
            $_Images[$Image->id] = [
                'id' => $Image->id,
                'thumb' => $Image->image->getImage('100x75'),
                'image' => $Image->image->getImage('400x400'),
                'file' => $Image->image,
                'owner' => $Image->owner,
                'item_id' => $Image->item_id,
                'name' => $Image->name,
                'title' => $Image->title,
                'snippet' => $Image->snippet,
                'mime' => $Image->mime,
                'ext' => $Image->ext,
                'size' => $Image->size,
                'status' => $Image->status,
                'views' => $Image->views,
                'create_time' => $Image->create_time,
                'update_time' => $Image->update_time,
            ];
        }

        return Json::encode($_Images);
    }


}
