/**
 * Created by suver on 25.08.14.
 */

$(function(){

    $(document).on("click", "[data-change-UploadImage]", function(){

        var id = $(this).attr('data-change-UploadImage');
        var element_id = $("#" + id).attr('data-element-id');
        var element_name = $("#" + id).attr('data-element-name');
        var owner = $("#" + id).attr('data-owner');
        var csrf_name = $("[data-csr-tocken=" + element_id + "]").attr('name');
        var csrf_value = $("[data-csr-tocken=" + element_id + "]").attr('value');

        // инициализация плагина jQuery File Upload
        $("[data-UploadImage-field]", "#" + id).fileupload({
            // этот элемент будет принимать перетаскиваемые на него файлы
            //dropZone: $('#drop'),
            dataType: 'json',
            // Функция будет вызвана при помещении файла в очередь
            add: function (e, data) {

                _size = $("img", "#" + id).attr("data-UploadImage-size");

                data.formData = {
                    size: _size,
                    owner: owner,
                    elements: element_name,
                    element_id: id,
                    element_name: element_name
                };

                data.formData[csrf_name] = csrf_value;

                // Автоматически загружаем файл при добавлении в очередь
                data.submit();

                // Сбрасываем лоадеры на ноль
                $("[data-UploadImage-progress]", "#" + id).css('width','0%');
            },
            done: function (e, data) {
                $("img", "#" + id).attr('src', data.result.images['0']);
                $("[data-UploadImage-id]", "#" + id).attr('value', data.result.elements['0']);
            },
            progress: function(e, data){
                // Вычисление процента загрузки
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("[data-UploadImage-loader-progress]", "#" + id).css('opacity', 1);

                // обновляем шкалу
                $("[data-UploadImage-progress]", "#" + id).css('width', progress + '%');
                $("[aria-valuenow]", "[data-UploadImage-progress]").val(progress);
                $(".sr-only", "[data-UploadImage-progress]").text(progress + "%");

                if(progress == 100){
                    $("[data-UploadImage-loader-progress]").css('opacity', 0);
                }
            },
            fail:function(e, data){
                console.log('UploadImage upload Error', data);
            }
        });

        $("[data-UploadImage-field]", "#" + id).click();

        return false;
    });



    /*$(document).on('drop dragover', function (e) {
        e.preventDefault();
    });*/


    $(document).on('click', "[data-remove-UploadImage]", function(){

        var id = $(this).attr('data-change-UploadImage');
        var element_id = $("#" + id).attr('data-element-id');
        var element_name = $("#" + id).attr('data-element-name');

        var csrf_name = $("[data-csr-tocken=" + element_id + "]").attr('name');
        var csrf_value = $("[data-csr-tocken=" + element_id + "]").attr('value');

        _url = $(this).attr('data-url');

        _size = $("[data-UploadImage-size]", "#" + id).attr("data-UploadImage-size");


        formData = {};
        formData[csrf_name] = csrf_value;
        formData['size'] = _size;
        formData['elements'] = element_name;
        formData['element_id'] = id;
        formData['element_name'] = element_name;
        formData['sizes'] = avatarSize;

        $.ajax({
            url: _url,
            data: formData,
            dataType: 'json',
            type: 'delete',
            success: function(data){

            }
        })
        .done(function(data){

        });

        return false;
    });

});



