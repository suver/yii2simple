<?php

namespace suver\mailer;

use \Yii;
use \yii\base\BootstrapInterface;
use \yii\base\Application;
use suver\mailer\models\Emails;
use suver\mailer\swiftmailer\SwiftMailer;
use \common\modules\user\models\User;


class Mailer extends \yii\base\Widget
{

    public $template_path;
    public $from;
    public $methods;
    public $user;

    private $_user;

    private $_params = [];

    public function bootstrap($app)
    {
        $app->on(Application::EVENT_BEFORE_REQUEST, function () {

        });
    }

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    protected function findUser($field, $value)
    {
        $userClass = $this->user['class'];

        if(new $userClass instanceof MailerUserInterface)
        {
            $findMethod = 'findBy' . $field;
            return $this->_user = $userClass::$findMethod($value);
        }
        else
        {
            return $this->_user = $userClass::findOne([$this->user->fields[$field] => $value]);
        }
    }

    protected function getUserValue($field)
    {
        if($this->_user instanceof MailerUserInterface)
        {
            $getMethod = 'get' . $field;
            return $this->_user->$getMethod();
        }
        else
        {
            return $this->_user->{$this->user->fields[$field]};
        }
    }

    public function __call($method, $params)
    {
        if(!method_exists($this, $method))
        {
            if(preg_match("/^set(\w+)/", $method, $match))
            {
                $this->_params[strtolower($match['1'])] = $params['0'];
            }
            else if(preg_match("/^get(\w+)/", $method, $match) AND isset($this->_params[$match['1']]))
            {
                return $this->_params[strtolower($match['1'])];
            }
        }
    }

    protected function params()
    {
        $this->_params['to_user_id'] = NULL;
        $this->_params['from_user_id'] = NULL;

        if(empty($this->_params['mailType']))
        {
            $this->_params['mailType'] = 'default';
        }

        if(empty($this->_params['method']))
        {
            $this->_params['method'] = 'default';
        }

        if(!empty($this->user) AND !empty($this->user['class']))
        {
            $userTo = false;
            // TO
            if(isset($this->_params['to']))
            {
                if(is_object($this->_params['to']))
                {
                    $userTo = $this->findUser('id', $this->_params['to']->{$this->user['fields']['id']});
                    if($userTo)
                    {
                        $this->_params['to'] = [$userTo->{$this->user['fields']['email']} => $userTo->{$this->user['fields']['fullname']}];
                    }
                    else
                    {
                        throw new \Exception(Yii::t('user', 'Пользователсь TO не найден'));
                    }
                }
                elseif(is_numeric($this->_params['to']))
                {
                    $userTo = $this->findUser('id', $this->_params['to']);
                    if($userTo)
                    {
                        $this->_params['to'] = [$userTo->{$this->user['fields']['email']} => $userTo->{$this->user['fields']['fullname']}];
                    }
                    else
                    {
                        throw new \Exception(Yii::t('user', 'Пользователсь TO не найден'));
                    }
                }
                elseif(preg_match("/@/", $this->_params['to']))
                {
                    $userTo = $this->findUser('email', $this->_params['to']);
                    if($userTo)
                    {
                        $this->_params['to'] = [$userTo->{$this->user['fields']['email']} => $userTo->{$this->user['fields']['fullname']}];
                    }
                    else
                    {
                        $this->_params['to'] = $this->_params['to'];
                    }
                }
                else
                {
                    $userTo = $this->findUser('username', $this->_params['to']);
                    if($userTo)
                    {
                        $this->_params['to'] = [$userTo->{$this->user['fields']['email']} => $userTo->{$this->user['fields']['fullname']}];
                    }
                    else
                    {
                        throw new Exception(Yii::t('user', 'Пользователсь TO не найден'));
                    }
                }
                $this->_params['to_user_id'] = !empty($userTo) ? $userTo->{$this->user['fields']['id']} : NULL;
            }
            else
            {
                throw new \Exception('Параметр [to] должен быть указан. [to=UserID, to=Email, to=Username или to=UserObject]');
            }

            //FROM
            if(!empty($this->_params['from']))
            {
                if(is_object($this->_params['from']))
                {
                    $userFrom = $this->findUser('id', $params['from']->{$this->user['fields']['id']});
                    if($userFrom)
                    {
                        $this->_params['from'] = [$userFrom->{$this->user['fields']['email']} => $userFrom->{$this->user['fields']['fullname']}];
                    }
                    else
                    {
                        throw new \Exception(Yii::t('user', 'Пользователсь FROM не найден'));
                    }
                }
                elseif(is_numeric($this->_params['from']))
                {
                    $userFrom = $this->findUser('id', $this->_params['from']);
                    if($userFrom)
                    {
                        $this->_params['from'] = [$userFrom->{$this->user['fields']['email']} => $userFrom->{$this->user['fields']['fullname']}];
                    }
                    else
                    {
                        throw new \Exception(Yii::t('user', 'Пользователсь FROM не найден'));
                    }
                }
                elseif(preg_match("/@/", $this->_params['from']))
                {
                    $userFrom = $this->findUser('email', $this->_params['from']);
                    if($userFrom)
                    {
                        $this->_params['from'] = [$userFrom->{$this->user['fields']['email']} => $userFrom->{$this->user['fields']['fullname']}];
                    }
                    else
                    {
                        $this->_params['from'] = $this->_params['from'];
                    }
                }
                else
                {
                    $userFrom = $this->findUser('username', $this->_params['from']);
                    if($userFrom)
                    {
                        $this->_params['from'] = [$userFrom->{$this->user['fields']['email']} => $userFrom->{$this->user['fields']['fullname']}];
                    }
                    else
                    {
                        throw new Exception(Yii::t('user', 'Пользователсь FROM не найден'));
                    }
                }

                $this->_params['from_user_id'] = !empty($userFrom) ? $userFrom->{$this->user['fields']['id']} : NULL;
            }
        }


        if(empty($this->_params['from']))
        {
            $this->_params['from'] = $this->_params['sender'];
        }

        $this->_params['date_created'] = date("Y-m-d H:i:s");


        $View = $this->getView();

        if(!empty($this->_params['template']))
        {
            $this->_params['body'] = $View->render($this->_params['viewPath'] . '/' . $this->_params['template'], ['message' => $this->_params, 'Mailer' => $this]);
            if(empty($this->_params['type']))
                $this->_params['type'] = 'text/html';
        }
        else
        {
            $this->_params['body'] = $this->_params['message'];
        }

        if(!empty($this->_params['template']))
        {
            $this->_params['full_message'] = $View->render($this->_params['viewPath'] . "/layouts/" . $this->_params['layout'], ['message' => $this->_params, 'content' => $this->_params['body'], 'Mailer' => $this]);
            if(empty($this->_params['type']))
                $this->_params['type'] = 'text/html';
        }
        else
        {
            $this->_params['full_message'] = $this->_params['body'];
        }



        if(isset($this->_params['type']))
        {
            switch($this->_params['type'])
            {
                case "text":
                    $this->_params['type'] = 'text/plain';
                    break;
                case "html":
                    $this->_params['type'] = 'text/html';
                    break;
            }
        }
        else
        {
            $this->_params['type'] = 'text/plain';
        }

        return $this->_params;
    }

    public function send($params=array(), $method=false, $mailType=null)
    {
        $this->_params = $params;

        if(!$method AND isset($this->_params['method']))
        {
            $method = $this->_params['method'];
        }
        elseif($method)
        {
            $this->_params['method'] = $method;
        }
        else
        {
            $method = $this->_params['method'] = 'default';
        }

        $this->_params['mailType'] = $mailType;



        if(!isset($this->methods[$method]) OR empty($this->methods[$method]['method']) OR empty($this->methods[$method]['transport']))
        {
            throw new \Exception('Способ отправки не задан настройками или задан не полностью');
        }

        $methodClass = $this->methods[$method]['method'];

        $methodObject = new $methodClass;
        $methodObject->setTransport($this->methods[$method]['transport']);

        $this->_params['viewPath'] = isset($this->methods[$method]['viewPath']) ? $this->methods[$method]['viewPath'] : '@app/mail';
        $this->_params['layout'] = isset($this->methods[$method]['layout']) ? $this->methods[$method]['layout'] : 'main';


        $this->_params['sender'] = $this->methods[$method]['sender'];
        $this->_params = $this->params();

        if($return = $methodObject->sendMessage($this->_params))
        {
            $this->_params['date_sended'] = date("Y-m-d H:i:s");
        }

        $this->saveMessage($this->_params);

        return $return;
    }

    public function saveMessage($params)
    {
        $Emails = new Emails();
        $Emails->type   = $params['mailType'];
        $Emails->method = $params['method'];
        $Emails->params = serialize($params);
        $Emails->to     = serialize($params['to']);
        $Emails->from   = serialize($params['from']);
        $Emails->subject    = $params['subject'];
        $Emails->message    = $params['message'];
        $Emails->to_user_id = $params['to_user_id'];
        $Emails->from_user_id   = $params['from_user_id'];
        $Emails->date_created   = $params['date_created'];
        $Emails->date_sended    = $params['date_sended'];

        if($Emails->save())
        {
            return true;
        }
        else
        {
            var_dump($Emails->getErrors());
        }
        return false;
    }

    public function test()
    {
        return true;
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['suver/mailer'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@suver/mailer/messages',
        ];
    }
}
