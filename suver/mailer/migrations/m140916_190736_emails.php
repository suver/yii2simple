<?php

use yii\db\Schema;

class m140916_190736_emails extends \yii\db\Migration
{
    public function up()
    {
        // Настройки MySql таблицы
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        // Таблица для смены e-mail адресов пользователя
        $this->createTable('{{%suver_emails}}', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_STRING . '(255) NULL DEFAULT NULL',
            'method' => Schema::TYPE_STRING . '(255) NOT NULL',
            'from' => Schema::TYPE_STRING . '(255) NOT NULL',
            'from_user_id' => Schema::TYPE_INTEGER . ' NULL DEFAULT NULL',
            'to' => Schema::TYPE_STRING . '(255) NOT NULL',
            'to_user_id' => Schema::TYPE_INTEGER . ' NULL DEFAULT NULL',
            'subject' => Schema::TYPE_STRING . '(255) NULL DEFAULT NULL',
            'message' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
            'params' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
            'date_created' => Schema::TYPE_DATETIME . ' NULL DEFAULT NULL',
            'date_sended' => Schema::TYPE_DATETIME . ' NULL DEFAULT NULL',
        ], $tableOptions);

        // Индексы
        $this->createIndex('to_user_id', '{{%suver_emails}}', 'to_user_id');
        $this->createIndex('to', '{{%suver_emails}}', 'to');
        $this->createIndex('from_user_id', '{{%suver_emails}}', 'from_user_id');
        $this->createIndex('from', '{{%suver_emails}}', 'from');
        $this->createIndex('method', '{{%suver_emails}}', 'method');
        $this->createIndex('type', '{{%suver_emails}}', 'type');
        $this->createIndex('date_created', '{{%suver_emails}}', 'date_created');
        $this->createIndex('date_sended', '{{%suver_emails}}', 'date_sended');

        // Связи
        //$this->addForeignKey('FK_user_email_user', '{{%emails}}', 'user_id', '{{%users}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {

        $this->dropTable('{{%suver_emails}}');
    }
}
