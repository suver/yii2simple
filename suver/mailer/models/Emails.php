<?php

namespace suver\mailer\models;

use Yii;

/**
 * This is the model class for table "emails".
 *
 * @property integer $id
 * @property string $type
 * @property string $method
 * @property string $from
 * @property integer $from_user_id
 * @property string $to
 * @property integer $to_user_id
 * @property string $subject
 * @property string $message
 * @property string $date_created
 * @property string $date_sended
 */
class Emails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suver_emails';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method', 'from', 'to', 'date_created'], 'required'],
            [['from_user_id', 'to_user_id'], 'integer'],
            [['message'], 'string'],
            [['date_created', 'date_sended'], 'safe'],
            [['type', 'method', 'from', 'to', 'subject'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('suver/mailer', 'ID'),
            'type' => Yii::t('suver/mailer', 'Type'),
            'method' => Yii::t('suver/mailer', 'Method'),
            'from' => Yii::t('suver/mailer', 'From'),
            'from_user_id' => Yii::t('suver/mailer', 'From User ID'),
            'to' => Yii::t('suver/mailer', 'To'),
            'to_user_id' => Yii::t('suver/mailer', 'To User ID'),
            'subject' => Yii::t('suver/mailer', 'Subject'),
            'message' => Yii::t('suver/mailer', 'Message'),
            'date_created' => Yii::t('suver/mailer', 'Date Created'),
            'date_sended' => Yii::t('suver/mailer', 'Date Sended'),
        ];
    }
}
