<?php
namespace suver\mailer;

/**
 * Created by PhpStorm.
 * User: suver
 * Date: 17.09.14
 * Time: 20:46
 */

interface MailerUserInterface
{
    public function getId();
    public function getEmail();
    public function getPhone();
    public function getFullname();

    public static function findById($id);
    public static function findByUsername($username);
    public static function findByEmail($email);
    public static function findByPhone($phone);
}