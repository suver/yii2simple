Отправка уведомлений для проектов на Yii2
=========================================
Добавляет возможность отправлять email уведомления с занесением их в БД. Поддержка шаблонов. Основа на SwiftMailer

Installation
-------------
1. Добавьте Yii2-mailer в ваш composer.json файл:
    <pre>
       {
            "require": {
                "suver/yii2-mailer": "dev-master"
            }
       }
    </pre>

2. Запустите
    <pre>
      php composer.phar update
    </pre>

3. Запустите
    <pre>
    php yii migrate --migrationPath=suver/mailer/migrations
    </pre>


4. Настройка компоненты
    ```php
        'components' => [
            'mailer' => [
                'class' => 'suver\mailer\Mailer',
                // Вы можете интегрировать в mailer модель пользователя.
                // Это даст возможность передавать не только email в качестве параметров Кому и ОтКого, но и id
                // пользователя, объект пользователя или Username пользователя.
                // Более того тогда ID пользователя будет записан в БД
                'user'  => [
                    // Полный namespace к классу модели пользователя
                    'class'  => 'common\modules\user\models\User',
                    // Соотвествие полей mailer и модели пользователя
                    'fields' => [
                        'id'    =>'user_id', //  поле ID соответсвует полю User_id модели пользователя
                        'email' => 'email',
                        'phone' => 'phone',
                        'fullname' => 'fullname',
                    ],
                ],
                // Варианты отправки уведомлений и сообщений
                'methods'  => [
                    // Метод по умолчанию
                    'default' => [
                        'method' => 'suver\mailer\method\SwiftMailer',
                        'sender' => 'mail@mail.com',
                        'viewPath' => '@common/mail', // Папка с шаблонами писем. Может быть переопределен
                        'layout' => 'html', // Лайут по умолчанию. Может быть переопределен
                        'transport' => [
                            'class' => 'Swift_SmtpTransport',
                            'host'  => 'smtp.yandex.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                            'username' => 'mail@farpse.com',
                            'password' => 'on82xni7',
                            'port'  => '465', // Port 25 is a very common port too
                            'encryption' => 'ssl', // It is often used, check your provider or mail server specs

                        ],
                    ],
                    // Отправка через SwiftMailer с помощью протокала SMTP
                    'smpt' => [
                        'method' => 'suver\mailer\method\SwiftMailer',
                        'sender' => 'mail@farpse.com',
                        'transport' => [
                            'class' => 'Swift_SmtpTransport',
                            'host'  => 'smtp.yandex.com',
                            'username' => 'mail@mail.com',
                            'password' => '******',
                            'port'  => '465',
                            'encryption' => 'ssl',

                        ],
                    ],
                    // Отправка через SwiftMailer с помощью протокала встроенной в PHP функции mail
                    'mail' => [
                        'method' => 'suver\mailer\method\SwiftMailer',
                        'sender' => 'mail@mail.com',
                        'transport' => [
                            'class' => 'Swift_MailTransport',
                        ],
                    ],
                    // Отправка через SwiftMailer с помощью SendMail
                    'sendmail' => [
                        'method' => 'suver\mailer\method\SwiftMailer',
                        'sender' => 'mail@mail.com',
                        'transport' => [
                            'class' => 'Swift_SendmailTransport',
                            'command' => '/usr/sbin/exim -bs',
                        ],
                    ],
                    // Отправка через SwiftMailer с помощью SendMail
                    'myNextEmailName' => [
                        'method' => 'suver\mailer\method\SwiftMailer',
                        'sender' => 'mail@mail.com',
                        'transport' => [
                            'class' => 'Swift_SendmailTransport',
                            'command' => '/usr/sbin/exim -bs',
                        ],
                    ],
                    // Скоро
                    'sms' => [
                        'class' => 'suver\mailer\method\Sms',
                        'useFileTransport' => false,
                        'transport' => [

                        ],
                    ],
                ],
            ],
        ],
    ```



Usage
-----

Примеры использования

    ```php
        Yii::$app->mailer->send('smpt', [          // Отправка при полной интеграции пользователю с ID 10 от пользвателя с ID 1
            'to'=>User::findById(10),
            'from'=>User::findById(1),
            'message' => '111 123',
            'subject' => '222 223',
        ])

        // или

        Yii::$app->mailer->send('smpt', [          // Отправка при полной интеграции пользователю me@mail.com от пользвателя с ID 1
            'to'=>'me@mail.com',
            'from'=>User::findById(1),
            'message' => '111 123',
            'subject' => '222 223',
        ])

        // или

        Yii::$app->mailer->send('smpt', [          // Отправка при полной интеграции пользователю me@mail.com от системного мыла
            'to'=>'me@mail.com',
            'message' => '111 123',
            'subject' => '222 223',
        ])

        Yii::$app->mailer->send('smpt', [          // Отправка при полной интеграции пользователю me@mail.com от системного мыла
            'to'=>'me@mail.com',
            'template' => '@template_mail_path/template.php',
            'message' => '111 123',
            'subject' => '222 223',
        ])

        Yii::$app->mailer->send('smpt', [          // Отправка при полной интеграции пользователю me@mail.com от системного мыла
            'to'=>'me@mail.com',
            'template' => 'template',
            'params' => ['user'=>$User, 'post'=>$Post, 'anotherParam'=>true],
            'message' => '111 123',
            'subject' => '222 223',
        ])

        Yii::$app->mailer->send([                // Отправляем писмо с шаблоном test. При этом в шаблоне будут доступные все параметры переданные сообщением в переменной $message
            'to'=>User::findById(1),
            'message' => '111 123',
            'subject' => '222 223',
            'template' => 'test',
        ], 'default')

    ```


