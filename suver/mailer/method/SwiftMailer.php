<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace suver\mailer\method;

use Yii;
use yii\base\InvalidConfigException;
use yii\mail\BaseMailer;

class SwiftMailer extends BaseMailer implements \suver\mailer\method\MethodInterface
{
    private $_swiftMailer;

    private $_transport = [];



    public function params($params)
    {
        return $params;
    }

    public function setTransport($transport=false)
    {
        if($transport AND $transport['class'] AND $transport['class'] == 'Swift_SmtpTransport')
        {
            $host = !empty($transport['host']) ? $transport['host'] : 'localhost' ;
            $port = !empty($transport['port']) ? $transport['port'] : '25' ;
            $encryption = !empty($transport['encryption']) ? $transport['encryption'] : null ;
            $username = !empty($transport['username']) ? $transport['username'] : false ;
            $password = !empty($transport['password']) ? $transport['password'] : false ;

            $_transport = \Swift_SmtpTransport::newInstance($host, $port, $encryption);
            if($username)
            {
                $_transport->setUsername($username);
            }

            if($password)
            {
                $_transport->setPassword($password);
            }
        }
        else if($transport AND $transport['class'] AND $transport['class'] == 'Swift_SendmailTransport')
        {
            $command = !empty($transport['command']) ? $transport['command'] : '/usr/sbin/exim -bs' ;
            $_transport = \Swift_SendmailTransport::newInstance($command);
        }
        else
        {
            $_transport = \Swift_MailTransport::newInstance();
        }

        $this->_transport = $_transport;
    }

    public function getTransport()
    {
        if(empty($this->_transport))
        {
            $this->_transport = $this->setTransport();
        }

        return $this->_transport;
    }

    public function sendMessage($params)
    {
        $params = $this->params($params);
        $mailer = \Swift_Mailer::newInstance($this->getTransport());

        $message = \Swift_Message::newInstance('Wonderful Subject');
        $message->setFrom($params['from']);
        $message->setTo($params['to']);
        $message->setBody('Here is the message itself');

        if(isset($params['charset']))
        {
            $message->setCharset($params['charset']);
        }
        if(isset($params['subject']))
        {
            $message->setSubject($params['subject']);
        }
        if(isset($params['replyTo']))
        {
            $message->setReplyTo($params['replyTo']);
        }
        if(isset($params['cc']))
        {
            $message->setCc($params['cc']);
        }
        if(isset($params['bcc']))
        {
            $message->setBcc($params['bcc']);
        }
        if(isset($params['encoder']))
        {
            $message->setEncoder($params['encoder']);
        }
        if(isset($params['contentType']))
        {
            $message->setContentType($params['contentType']);
        }
        if(isset($params['date']))
        {
            $message->setDate($params['date']);
        }
        if(isset($params['sender']))
        {
            $message->setSender($params['sender']);
        }
        if(isset($params['returnPath']))
        {
            $message->setReturnPath($params['returnPath']);
        }
        if(isset($params['id']))
        {
            $message->setId($params['id']);
        }
        if(isset($params['message']))
        {
            $message->setBody($params['full_message'], $params['type']);
        }

        var_dump($params);

        return $mailer->send($message);
    }
}
