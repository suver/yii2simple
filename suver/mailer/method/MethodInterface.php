<?php

namespace suver\mailer\method;

/**
 * Created by PhpStorm.
 * User: suver
 * Date: 17.09.14
 * Time: 20:01
 */

interface MethodInterface {

    public function params($params);

    public function setTransport($transport);

    public function getTransport();

    public function sendMessage($params);
}