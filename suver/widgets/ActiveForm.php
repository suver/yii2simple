<?php
/**
 * Created by PhpStorm.
 * User: suver
 * Date: 21.09.14
 * Time: 19:49
 */

namespace suver\widgets;

/**
 * Class ActiveForm
 * @package suver\widgets
 *
 */


use yii;

class ActiveForm extends \yii\bootstrap\ActiveForm
{
    public function init()
    {
        if (!isset($this->fieldConfig['class'])) {
            $this->fieldConfig['class'] = ActiveField::className();
        }

        parent::init();

        $this->registerTranslations();

        $view = $this->getView();
        ActiveAsset::register($view);
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['suver/widgets/ActiveForm'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@suver/temporaryStorage/messages',
        ];
    }
}