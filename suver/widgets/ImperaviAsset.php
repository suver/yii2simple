<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace suver\widgets;

use yii;
use yii\web\AssetBundle;

/**
 * This asset bundle provides the javascript files for client validation.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ImperaviAsset extends AssetBundle
{
    public $sourcePath = '@suver/widgets/assets/imperavi';

    public $js = [
        'redactor.js',
        'lang/ru.js',
        'lang/ua.js',
        'plugins/fontcolor/fontcolor.js',
        'plugins/textdirection/textdirection.js',
        'plugins/fontsize/fontsize.js',
        'plugins/filemanager/filemanager.js',
        'plugins/imagemanager/imagemanager.js',
        'plugins/video/video.js',
        'plugins/table/table.js',
        'plugins/separate/separate.js',
    ];
    public $css = [
        'redactor.css',
        'font-awesome.min.css',
        'style.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
