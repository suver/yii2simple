if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.separate = function()
{
	return {
        init: function()
        {
            var button = this.button.add('separate', 'Separate');
            this.button.addCallback(button, this.separate.insert);
            this.button.setAwesome('separate', 'fa-ellipsis-h');
            //this.button.setInactive('separate');

            // add active state
            this.observe.addButton('snippet', 'separate');

        },
        format: function(tag, type, value)
        {
            var formatTags = ['snippet', 'pre'];
            if ($.inArray(tag, formatTags) == -1) return;

            this.block.isRemoveInline = (tag == 'snippet' || tag.search(/h[1-6]/i) != -1);

            // focus
            if (!this.utils.browser('msie')) this.$editor.focus();

            this.block.blocks = this.selection.getBlocks();

            this.block.blocksSize = this.block.blocks.length;
            this.block.type = type;
            this.block.value = value;

            this.buffer.set();
            this.selection.save();

            this.block.set(tag);

            this.selection.restore();
            this.code.sync();

        },
        insert: function()
        {
            this.separate.format('snippet');
        }
	};
};