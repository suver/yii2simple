
function suver_widget_editable($editable, $input, emptyText) {
    $editable.editable({
        emptyMessage: '<em>' + emptyText + '</em>',
        touch : true,
        toggleFontSize : true,
        closeOnEnter : true,
        event : 'dblclick',
        tinyMCE : false,
        callback: function (data) {

            if( data.content )
            {
                $input.val(data.content);
            }
            else {
                $input.val('');
            }

            data.$el.effect('blink');
        }
    });
}