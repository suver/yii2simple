<?php

namespace suver\widgets;

/**
 * Примерение фильтра маски вводимых знаков.
 * $this->input('text', ['mask'=>'a999'])
 *
 * Добавления помошника к полю пароля
 * $this->passwordInput(['helper'=>true]);
 *
 * Добавление выпадающего списка со стилизацией в select2
 * $this->dropDownList(['val1', 'val2'])
 *
 * Добавление инфо блока к полю
 * $this->input('text')
 *      ->info('text for info')
 *
 * Добавление предупреждающего сообщения к полю
 * $this->input('text')
 *      ->warning('text for warning')
 *
 * Добавление информационного и предупреждающего сообщения к полю
 * $this->input('text')
 *      ->info('text for info')
 *      ->warning('text for warning')
 *
 * Добавляем иконку в конец поля
 * $this->input('text',['icon'=>'icon-warning'])
 *
 * Добавляем $ в конце поля, по правилам TwitterBootsrap
 * $this->input('text',['after'=>'$'])
 *
 * Добавляем @ в начале поля, по правилам TwitterBootsrap
 * $this->input('text',['before'=>'@'])
 *
 *
 * Добавляет поле управления тегами
 * https://github.com/xoxco/jQuery-Tags-Input
 * $this->tags(['url_to_autocomlite'],['class'=>'ss'])
 */

use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use suver\helpers\ArrayHelper;
use yii\base\Component;
use yii\base\ErrorHandler;
use yii\base\Model;
use yii\web\JsExpression;

class ActiveField extends \yii\widgets\ActiveField
{
    public $template = "{label}\n<div class='col-sm-10'>{input}\n{info}\n{warning}\n{error}\n{hint}</div>";

    public $inputOptions = ['class' => 'form-control'];

    public $select2Options = ['class' => ''];

    public $infoOptions = ['class' => 'label label-info label-block text-left'];

    public $warningOptions = ['class' => 'label label-danger label-block text-left'];

    public $errorOptions = ['class' => 'help-block'];

    public $labelOptions = ['class' => 'control-label'];

    public $hintOptions = ['class' => 'help-block'];



    /**
     * Renders the opening tag of the field container.
     * @return string the rendering result.
     */
    public function begin()
    {
        $clientOptions = $this->getClientOptions();
        if (!empty($clientOptions)) {
            $this->form->attributes[$this->attribute] = $clientOptions;
        }

        $inputID = Html::getInputId($this->model, $this->attribute);
        $attribute = Html::getAttributeName($this->attribute);
        $options = $this->options;
        $class = isset($options['class']) ? [$options['class']] : [];
        $class[] = "field-$inputID";
        if ($this->model->isAttributeRequired($attribute)) {
            $class[] = $this->form->requiredCssClass;
        }
        if ($this->model->hasErrors($attribute)) {
            $class[] = $this->form->errorCssClass;
        }

        if(isset($this->parts['{icon}'])) {
            $class[] = 'has-feedback';
        }

        $options['class'] = implode(' ', $class);
        $tag = ArrayHelper::remove($options, 'tag', 'div');

        return Html::beginTag($tag, $options);
    }

    /**
     * Renders the whole field.
     * This method will generate the label, error tag, input tag and hint tag (if any), and
     * assemble them into HTML according to [[template]].
     * @param string|callable $content the content within the field container.
     * If null (not set), the default methods will be called to generate the label, error tag and input tag,
     * and use them as the content.
     * If a callable, it will be called to generate the content. The signature of the callable should be:
     *
     * ~~~
     * function ($field) {
     *     return $html;
     * }
     * ~~~
     *
     * @return string the rendering result
     */
    public function render($content = null)
    {
        if ($content === null) {

            if(isset($this->labelOptions['label']) AND $this->labelOptions['label'] === false) {
                $this->parts['{label}'] = '';
            }
            else {
                Html::addCssClass($this->labelOptions, 'col-sm-2');
                $this->parts['{label}'] = Html::activeLabel($this->model, $this->attribute, $this->labelOptions);
            }


            if (!isset($this->parts['{error}'])) {
                $this->parts['{error}'] = Html::tag('label', Html::error($this->model, $this->attribute), $this->errorOptions);
            }

            if(!isset($this->parts['{input}'])) {
                $this->parts['{input}']  = Html::activeTextInput($this->model, $this->attribute, $this->inputOptions);
            }

            $this->parts['{input}'] .= !empty($this->parts['{icon}']) ? $this->parts['{icon}'] : '' ;

            if(!empty($this->parts['{before}']) OR !empty($this->parts['{after}'])) {
                $inputFiled = !empty($this->parts['{before}']) ? $this->parts['{before}'] : '';
                $inputFiled .= $this->parts['{input}'];
                $inputFiled .= !empty($this->parts['{after}']) ? $this->parts['{after}'] : '';
                $this->parts['{input}'] = Html::tag('div', $inputFiled, ['class'=>'input-group']);
            }

            if (!isset($this->parts['{hint}'])) {
                $this->parts['{hint}'] = '';
            }

            if (!isset($this->parts['{warning}'])) {
                $this->parts['{warning}'] = '';
            }

            if (!isset($this->parts['{info}'])) {
                $this->parts['{info}'] = '';
            }


            $content = strtr($this->template, $this->parts);
        } elseif (!is_string($content)) {
            $content = call_user_func($content, $this);
        }

        return $this->begin() . "\n" . $content . "\n" . $this->end();
    }

    /**
     * Генерация информационного окна под полем
     *
     * @return static the field object itself
     */
    public function info($info = null, $options = [])
    {
        if ($info === false) {
            $this->parts['{info}'] = '';
            return $this;
        }

        $options = array_merge($this->infoOptions, $options);
        if ($info !== null) {
            $options['info'] = $info;
            $this->parts['{info}'] = Html::tag('span', $options['info'], $options);
        }

        return $this;
    }

    /**
     * Генерация предупреждающего окна под полем
     *
     * @return static the field object itself
     */
    public function warning($warning = null, $options = [])
    {
        if ($warning === false) {
            $this->parts['{warning}'] = '';
            return $this;
        }

        $options = array_merge($this->warningOptions, $options);
        if ($warning !== null) {
            $options['warning'] = $warning;
            $this->parts['{warning}'] = Html::tag('span', $options['warning'], $options);
        }

        return $this;
    }

    public function fieldOptions($options=[], $isInput=true)
    {
        if($isInput AND !empty($this->inputOptions['class'])) {

            Html::addCssClass($options, $this->inputOptions['class']);


            if(isset($options['before'])) {
                $this->parts['{before}'] = Html::tag('span', $options['before'], ['class'=>'input-group-addon']);
            }

            if(isset($options['after'])) {
                $this->parts['{after}'] = Html::tag('span', $options['after'], ['class'=>'input-group-addon']);
            }

            if(isset($options['icon'])) {
                $icon = Html::tag('i', '', ['class' => "icon {$options['icon']} form-control-feedback"]);
                $this->parts['{icon}'] = $icon;
            }

            if(isset($options['left-icon'])) {
                $icon = Html::tag('i', '', ['class' => "icon {$options['left-icon']} form-control-feedback"]);
                $this->parts['{icon}'] = $icon;
                $options['container'] = 'has-feedback-left';
            }
        }

        $options['value'] = isset($options['value']) ? $options['value'] : Html::getAttributeValue($this->model, $this->attribute);

        if(isset($options['tooltip'])) {
            $options['data-toggle'] = "tooltip";
            $options['data-tooltip'] = "tooltip";
            //$options['data-trigger'] = "focus";
            $options['data-placement'] = 'left';
            $options['title'] = $options['tooltip'];
            $options['data-placement'] = isset($options['placement']) ? $options['placement'] : "bottom";

            $view = $this->form->getView();
            $view->registerJs('$("[data-tooltip]").tooltip({});');
        }

        if (!array_key_exists('id', $options)) {
            $options['id'] = Html::getInputId($this->model, $this->attribute);
        }

        if (!array_key_exists('name', $options)) {
            $options['name'] = Html::getInputName($this->model, $this->attribute);
        }

        return $options;
    }

    public function input($type, $options = [])
    {
        $options = $this->fieldOptions($options);

        if(isset($options['mask']))
        {
            $options['data-mask'] = $options['mask'];
            unset($options['mask']);
        }

        return parent::input($type, $options);
    }

    public function html5DatetimeLocal($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('datetime-local', $options);
    }

    public function html5Time($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('time', $options);
    }

    public function html5Date($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('date', $options);
    }

    public function html5Month($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('month', $options);
    }

    public function html5Url($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('url', $options);
    }

    public function html5Email($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('email', $options);
    }

    public function html5Number($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('number', $options);
    }

    public function html5Week($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('week', $options);
    }

    public function html5Tel($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('tel', $options);
    }

    public function html5Search($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('search', $options);
    }

    public function html5Datetime($options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::input('datetime', $options);
    }

    public function fileInput($options = [])
    {
        $options = $this->fieldOptions($options);

        Html::addCssClass($options, 'styled');

        return parent::fileInput($options);
    }

    public function passwordInput($options = [])
    {
        $options = $this->fieldOptions($options);

        if(isset($options['helper']) AND $options['helper'] AND $this->form) {
            $options['data-password-helper'] = 'On';
            $view = $this->form->getView();
            $view->registerJs('jQuery(document).ready(function () {
                jQuery("[data-password-helper]").passField({ /*options*/ });
            });');
        }

        return parent::passwordInput($options);
    }

    public function dropDownList($items, $options = [])
    {
        $options = $this->fieldOptions($options, false);

        if(empty($options['pluginOption'])) {
            $options['pluginOption']['minimumResultsForSearch'] = '-1';
            $options['pluginOption']['width'] = '100%';
        }

        if(empty($options['data-placeholder'])) {
            $options['data-placeholder'] = isset($options['placeholder']) ? $options['placeholder'] : NULL;
            $options['data-placeholder'] = isset($options['empty']) ? $options['empty'] : $options['data-placeholder'];
            $options['data-placeholder'] = empty($options['data-placeholder']) ? Yii::t('suver/widgets/ActiveForm', "Выберите") : $options['data-placeholder'];

            if(!empty($options['data-placeholder']))
            {
                $options['prompt'] = "";
            }

        }

        $this->inputOptions = $this->select2Options;

        $pluginOption = $options['pluginOption'];

        unset($options['pluginOption']);

        $view = $this->form->getView();

        if(isset($options['editable']) AND $options['editable']) {

            $options['data-value'] = $options['value'];

            $view->registerJs('jQuery(document).ready(function () {
                $.fn.editable.defaults.mode = "inline";
            });');

            foreach($items as $key=>$item){
                $_items[] = [
                    'id' => $key, 'text' => $item
                ];
            }

            if(empty($options['data-url']) OR empty($options['editable']['url'])) {
                if (empty($this->form->action)) {
                    $action_url = Url::to('');
                } else {
                    $action_url = $this->form->action;
                }
            }

            if($options['editable'] === true) {
                $editable = '{
                    type: "select2",
                    source: ' . ArrayHelper::toJsObject($_items) . ',
                    savenochange:false,
                    showbuttons:false,
                    send:"auto",
                    name:"' . (empty($options['name']) ? $this->attribute : $options['name']) . '",
                    //pk:"' . Html::getInputId($this->model, $this->attribute) . '",
                    //url: "' . $action_url . '",
                    select2: ' . ArrayHelper::toJsObject($pluginOption) . '
                }';
            }
            else {
                $editable = ArrayHelper::toJsObject($options['editable']);
            }

            $view->registerJs('jQuery(document).ready(function () {
                $("#' . $options['id'] . '").editable(' . $editable . ');
            });');

            return Html::tag('a', '', $options);
        }

        $view->registerJs('jQuery(document).ready(function () {
            jQuery("#' . $options['id'] . '").select2(' . ArrayHelper::toJsObject($pluginOption) . ');
        });');

        //Html::addCssClass($options, 'select-full');

        unset($options['pluginOption']);

        return parent::dropDownList($items, $options);
    }

    public function hiddenInput($options = [])
    {
        return parent::hiddenInput($options);
    }

    public function textInput($options = [])
    {
        $options = $this->fieldOptions($options);

        $view = $this->form->getView();

        if(isset($options['live-transliterate']) AND $options['live-transliterate'])
        {
            $view->registerJs('jQuery(document).ready(function () {
                $("#' . $options['id'] . '").translit("watch", "#' . $options['live-transliterate'] . '");
            });');
            unset($options['live-transliterate']);
        }

        if(isset($options['editable']) AND $options['editable']) {
            return $this->editableInput($options);
        }

        return parent::textInput($options);
    }

    public function SlugInput($options = [])
    {
        $options = $this->fieldOptions($options);

        $view = $this->form->getView();

        if(isset($options['live-transliterate']) AND $options['live-transliterate'])
        {
            $view->registerJs('jQuery(document).ready(function () {
                $("#' . $options['id'] . '").translit("watch", "#' . $options['live-transliterate'] . '", true, "-");
            });');
            unset($options['live-transliterate']);
        }

        if(isset($options['editable']) AND $options['editable']) {

            $view->registerJs('jQuery(document).ready(function () {
                $.fn.editable.defaults.mode = "inline";
            });');

            $editable = ($options['editable'] === true) ? '{type: "text", inputclass: "' . $options['class'] . '"}' : ArrayHelper::toJsObject($options['editable']) ;

            $view->registerJs('jQuery(document).ready(function () {
                $("#' . $options['id'] . '").editable(' . $editable . ');
            });');

            //return Html::tag('a', $options['value'], $options);

            $options = array_merge($this->inputOptions, $options);
            $this->adjustLabelFor($options);
            $this->parts['{input}'] = Html::tag('a', $options['value'], $options);

            return $this;
        }

        return parent::textInput($options);
    }

    public function editableInput($options = [])
    {
        $options = $this->fieldOptions($options);

        $view = $this->form->getView();

        $view->registerJs('jQuery(document).ready(function () {
            suver_widget_editable( $("#' . $options['id'] . '_editable"), $("input[name=\"' . $options['name'] . '\"]"), "' . Yii::t('suver/widgets/ActiveForm', 'Укажите значние') . '");
        });');

        $options = array_merge($this->inputOptions, $options);
        $this->adjustLabelFor($options);

        $id = $options['id'];
        $name = $options['name'];

        //unset($options['id']);
        unset($options['name']);

        $this->parts['{input}'] = Html::tag('span', $options['value'], array_merge($options, array('id' => $id . '_editable'))) . Html::input('hidden', $name, $options['value'], $options);

        return $this;
    }



    public function numericInput($options = [])
    {
        $options = $this->fieldOptions($options);

        if(empty($options['pluginOption'])) {
            $options['pluginOption'] = ['numberFormat' => 'n'];
        }

        $view = $this->form->getView();
        $view->registerJs('jQuery(document).ready(function () {
            $( "#' . $options['id'] . '" ).spinner(' . yii\helpers\Json::encode($options['pluginOption']) . ');
        });');

        unset($options['pluginOption']);

        return parent::textInput($options);
    }

    public function multiSelect($items, $options = [])
    {
        $options = $this->fieldOptions($options, false);

        $options['multiple'] = true;

        if(empty($options['pluginOption'])) {
            $options['pluginOption']['buttonClass'] = 'btn btn-default';
            $options['pluginOption']['onChange'] = 'js:function(element, checked){$.uniform.update();}';
        }

        $view = $this->form->getView();

        $view->registerJs('jQuery(document).ready(function () {
            $( "#' . $options['id'] . '" ).multiselect(' . ArrayHelper::toJsObject($options['pluginOption']) . ');
            $(".multiselect-container input").uniform({ radioClass: "choice", selectAutoWidth: false });
        });');

        unset($options['pluginOption']);

        return self::listBox($items, $options);
    }

    public function singleSelect($items, $options = [])
    {
        $options = $this->fieldOptions($options, false);

        if(empty($options['pluginOption'])) {
            $options['pluginOption']['buttonClass'] = 'btn btn-default';
            $options['pluginOption']['onChange'] = 'js:function(element, checked){$.uniform.update();}';
        }

        $view = $this->form->getView();

        $view->registerJs('jQuery(document).ready(function () {
            $( "#' . $options['id'] . '" ).multiselect(' . ArrayHelper::toJsObject($options['pluginOption']) . ');
            $(".multiselect-container input").uniform({ radioClass: "choice", selectAutoWidth: false });
        });');

        unset($options['pluginOption']);

        return self::listBox($items, $options);
    }

    public function select2($items, $options = [])
    {
        $options = $this->fieldOptions($options, false);

        if(empty($options['pluginOption'])) {
            $options['pluginOption']['minimumResultsForSearch'] = '1';
            $options['pluginOption']['width'] = '100%';
            $options['pluginOption']['allowClear'] = true;
        }

        if(empty($options['data-placeholder'])) {
            $options['data-placeholder'] = isset($options['placeholder']) ? $options['placeholder'] : NULL;
            $options['data-placeholder'] = isset($options['empty']) ? $options['empty'] : $options['data-placeholder'];
            $options['data-placeholder'] = empty($options['data-placeholder']) ? Yii::t('suver/widgets/ActiveForm', "Выберите") : $options['data-placeholder'];

            if(!empty($options['data-placeholder']))
            {
                $options['prompt'] = "";
            }

        }

        $this->inputOptions = $this->select2Options;

        $view = $this->form->getView();

        $view->registerJs('jQuery(document).ready(function () {
            jQuery( "#' . $options['id'] . '" ).select2(' . ArrayHelper::toJsObject($options['pluginOption']) . ');
        });');

        unset($options['pluginOption']);

        return parent::dropDownList($items, $options);
    }

    public function multiSelect2($items, $options = [])
    {
        $options = $this->fieldOptions($options, false);

        if(empty($options['pluginOption'])) {
            $options['pluginOption']['width'] = '100%';
        }

        if(empty($options['data-placeholder'])) {
            $options['data-placeholder'] = isset($options['placeholder']) ? $options['placeholder'] : NULL;
            $options['data-placeholder'] = isset($options['empty']) ? $options['empty'] : $options['data-placeholder'];
            $options['data-placeholder'] = empty($options['data-placeholder']) ? Yii::t('suver/widgets/ActiveForm', "Выберите") : $options['data-placeholder'];

            if(!empty($options['data-placeholder']))
            {
                $options['prompt'] = "";
            }

        }

        $this->inputOptions = $this->select2Options;

        $options['multiple'] = true;

        $view = $this->form->getView();

        $view->registerJs('jQuery(document).ready(function () {
            jQuery( "#' . $options['id'] . '" ).select2(' . ArrayHelper::toJsObject($options['pluginOption']) . ');
        });');

        unset($options['pluginOption']);

        return parent::listBox($items, $options);
    }

    public function switchButton($options = [])
    {
        $options = $this->fieldOptions($options, false);

        if(empty($options['pluginOption'])) {
            //$options['pluginOption']['width'] = '100%';
        }

        if(!empty($options['on'])) {
            $options['data-on-label'] = $options['on'];
            unset($options['on']);
        }

        if(!empty($options['off'])) {
            $options['data-off-label'] = $options['off'];
            unset($options['off']);
        }

        if(!empty($options['on-class'])) {
            $options['data-on'] = $options['on-class'];
            unset($options['on-class']);
        }

        if(!empty($options['off-class'])) {
            $options['data-off'] = $options['off-class'];
            unset($options['off-class']);
        }

        $view = $this->form->getView();

        $view->registerJs('jQuery(document).ready(function () {
            jQuery( "#' . $options['id'] . '" ).bootstrapSwitch();
        });');

        unset($options['pluginOption']);

        return parent::checkbox($options, false);
    }

    public function elasticTextarea($options = [])
    {
        $options = $this->fieldOptions($options);

        //Html::addCssClass($options, 'elastic');

        $options['data-elastic'] = 'On';

        $view = $this->form->getView();
        $view->registerJs('jQuery(document).ready(function () {
            jQuery("[data-elastic]").autosize();
        });');

        return $this->textarea($options);
    }

    public function textarea($options = [])
    {
        $options = $this->fieldOptions($options);

        if(isset($options['limit']) AND is_numeric($options['limit'])) {

            $options['data-textarea-limit'] = $options['limit'];

            $view = $this->form->getView();
            $view->registerJs('jQuery(document).ready(function () {
                jQuery("[data-textarea-limit]").inputlimiter({
                    limit: ' . $options['limit'] . ',
                    boxId: "' . $options['id'] . '-limit-text",
                    boxAttach: false,
                    remText: "' . Yii::t('suver/widgets/ActiveForm', 'Осталось %n символов.'). '",
                    limitText: "' . Yii::t('suver/widgets/ActiveForm', 'Не более %n символов.'). '"
                });
            });');
        }

        $textAreaField = parent::textarea($options);

        if(isset($options['limit']) AND is_numeric($options['limit'])) {
            $textAreaField->hint('', ['id' => $options['id'] . '-limit-text']);
        }

        return $textAreaField;
    }

    public function tags($url=null, $options = [], $tagsOptions = [])
    {
        $options = $this->fieldOptions($options);

        if (!array_key_exists('id', $options)) {
            $options['id'] = Html::getInputId($this->model, $this->attribute);
        }

        $view = $this->form->getView();
        if($url) {
            $_tagsOptions['autocomplete_url'] = Url::to($url);
        }
        $_tagsOptions['width'] = '100%';
        $_tagsOptions['defaultText'] = Yii::t('suver/widgets/ActiveForm', 'Тег');

        $tagsOptions = array_merge($_tagsOptions, $tagsOptions);

        $view->registerJs('jQuery(document).ready(function () {$("#' . $options['id'] . '").tagsInput(' . yii\helpers\Json::encode($tagsOptions) . ');});');

        return parent::textInput($options);
    }

    public function radio($options = [], $enclosedByLabel = true)
    {
        $options = $this->fieldOptions($options);

        Html::addCssClass($options, 'styled');

        $view = $this->form->getView();
        $view->registerJs('jQuery(document).ready(function () {
            $(".styled").uniform({ radioClass: "choice", selectAutoWidth: false });
        });');

        return parent::radio($options, $enclosedByLabel);
    }

    public function checkbox($options = [], $enclosedByLabel = true)
    {
        $options = $this->fieldOptions($options);

        Html::addCssClass($options, 'styled');

        $view = $this->form->getView();
        $view->registerJs('jQuery(document).ready(function () {
            $(".styled").uniform({ radioClass: "choice", selectAutoWidth: false });
        });');

        return parent::checkbox($options, $enclosedByLabel);
    }

    public function listBox($items, $options = [])
    {
        $options = $this->fieldOptions($options);

        return parent::listBox($items, $options);
    }

    public function multyListBox($items, $options = [])
    {
        $options = $this->fieldOptions($options);

        $options['multiple'] = true;

        return parent::listBox($items, $options);
    }

    public function checkboxList($items, $options = [])
    {
        $options = $this->fieldOptions($options);

        if(empty($options['itemOptions'])) {
            $options['itemOptions'] = [];
        }

        if(empty($options['itemOptions']['container'])) {
            $options['itemOptions']['container'] = false;
        }

        if(empty($options['itemOptions']['labelOptions'])) {
            $options['itemOptions']['labelOptions'] = [];
        }

        if(!empty($options['labelClass'])) {
            Html::addCssClass($options['itemOptions']['labelOptions'], $options['labelClass']);
            unset($options['labelClass']);
        }

        Html::addCssClass($options['itemOptions'], 'styled');
        Html::addCssClass($options['itemOptions']['labelOptions'], 'checkbox-inline');
        Html::addCssClass($options, 'form-checkbox-list-control');

        $view = $this->form->getView();
        $view->registerJs('jQuery(document).ready(function () {
            $(".styled").uniform({ radioClass: "choice", selectAutoWidth: false });
        });');

        return parent::checkboxList($items, $options);
    }

    public function radioList($items, $options = [])
    {
        $options = $this->fieldOptions($options);

        if(empty($options['itemOptions'])) {
            $options['itemOptions'] = [];
        }

        if(empty($options['itemOptions']['container'])) {
            $options['itemOptions']['container'] = false;
        }

        if(empty($options['itemOptions']['labelOptions'])) {
            $options['itemOptions']['labelOptions'] = [];
        }

        if(!empty($options['labelClass'])) {
            Html::addCssClass($options['itemOptions']['labelOptions'], $options['labelClass']);
            unset($options['labelClass']);
        }

        Html::addCssClass($options['itemOptions'], 'styled');
        Html::addCssClass($options['itemOptions']['labelOptions'], 'radio-inline');
        Html::addCssClass($options, 'form-radio-list-control');

        $view = $this->form->getView();
        $view->registerJs('jQuery(document).ready(function () {
            $(".styled").uniform({ radioClass: "choice", selectAutoWidth: false });
        });');

        return parent::radioList($items, $options);
    }

    public function simpleWysiwyg($options = [])
    {
        $options = $this->fieldOptions($options);

        $view = $this->form->getView();
        $view->registerJs('jQuery(document).ready(function () {
            jQuery( "#' . $options['id'] . '" ).wysihtml5({});
        });');

        return $this->textarea($options);
    }

    public function imperaviWysiwyg($options = [])
    {
        $options = $this->fieldOptions($options);

        $view = $this->form->getView();

        $bundle = \suver\widgets\ImperaviAsset::register($view);

        if(isset($options['pluginOption']['lang'])) {
            $view->registerJsFile('langs/' . $options['pluginOption']['lang'] . '.js');
        }


        $options['pluginOption']['focus'] = true;
        $options['pluginOption']['buttonSource'] = true;
        $options['pluginOption']['fileUpload'] = '/upload.php';
        $options['pluginOption']['fileManagerJson'] = '/files/files.json';
        $options['pluginOption']['imageUpload'] = Url::toRoute(['/images/upload/insert']);
        $options['pluginOption']['imageManagerJson'] = Url::toRoute(['/images/list/json']);
        $options['pluginOption']['uploadImageFields'] = [
            Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken(),
            'size'=>'400x400',
            'owner' => 'Images',
            'elements' => 'Images[image]',
            'element_id' => 'image',
            'element_name' => 'Images[image]'
        ];
        $options['pluginOption']['plugins'] = [
            'fontcolor',
            'textdirection',
            'fullscreen',
            'video',
            'table',
            'filemanager',
            'imagemanager',
            'separate',
        ];

        $view->registerJs('jQuery(document).ready(function () {
            jQuery( "#' . $options['id'] . '" ).redactor(' . ArrayHelper::toJsObject($options['pluginOption']) . ');
        });');

        unset($options['pluginOption']);

        return $this->textarea($options);
    }

    public function widget($class, $config = [])
    {
        return parent::widget($class, $config);
    }


}
