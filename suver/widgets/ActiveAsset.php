<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace suver\widgets;

use yii;
use yii\web\AssetBundle;

/**
 * This asset bundle provides the javascript files for client validation.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ActiveAsset extends AssetBundle
{
    public $sourcePath = '@suver/widgets/assets';

    public $js = [
        'jquery/jquery-ui.min.js',
        'inputmask.js',
        'inputlimit.min.js',
        'uniform.min.js',
        'select2.min.js',
        'autosize.js',
        'inputlimit.min.js',
        'listbox.js',
        'multiselect.js',
        'validate.min.js',
        'tags.min.js',
        'switch.min.js',
        'uploader/plupload.full.min.js',
        'uploader/plupload.queue.min.js',
        'wysihtml5/wysihtml5.min.js',
        'wysihtml5/toolbar.js',
        'translit.js',
        //'bootstrap-wysihtml5/wysihtml5-0.3.0.js',
        //'bootstrap-wysihtml5/bootstrap-wysihtml5-0.0.2.js',
        'passfield/js/passfield.min.js',
        'jquery.editable.min.js',
        'suver.widget.js',
    ];
    public $css = [
        'suver.widget.css',
        'passfield/css/passfield.min.css',
        //'bootstrap-wysihtml5/bootstrap-wysihtml5-0.0.2.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
