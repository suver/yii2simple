<?php

namespace suver\logger\models;

use Yii;

/**
 * This is the model class for table "loggers".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $action
 * @property string $data
 * @property string $date_created
 */
class Loggers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suver_loggers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data', 'date_created'], 'required'],
            [['user_id'], 'integer'],
            [['data'], 'string'],
            [['date_created'], 'safe'],
            [['action', 'status'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('suver/loggers', 'ID'),
            'user_id' => Yii::t('suver/loggers', 'ID Пользовтаеля'),
            'action' => Yii::t('suver/loggers', 'Действие'),
            'status' => Yii::t('suver/loggers', 'Статус'),
            'data' => Yii::t('suver/loggers', 'Data'),
            'date_created' => Yii::t('suver/loggers', 'Date Created'),
        ];
    }
}
