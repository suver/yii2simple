Logger для проектов на Yii2
===========================
Добавляет возможность логирования действий проекта в БД


Installation
-------------
1. Добавьте Yii2-logger в ваш composer.json файл:
    <pre>
       {
            "require": {
                "suver/yii2-logger": "dev-master"
            }
       }
    </pre>

2. Запустите
    <pre>
      php composer.phar update
    </pre>

3. Запустите
    <pre>
    php yii migrate/up --migrationPath=suver/logger/migrations
    </pre>


4. Добавьте в файл настроек
    ```php
    'components' => [
            'logger' => [
                'class' => 'suver\logger\Logger',
            ],
        ],
    ```



Usage
-----

Для занесения информации в БД достаточно вызвать

    ```php
        Yii::$app->logger->set('dd', [1,2,3]);  // тип действия, дамп данных

        // или

        Yii::$app->logger->set('dd', [1,2,3], 2); // тип действия, дамп данных, id пользователя в системе
    ```