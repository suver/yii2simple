<?php

namespace suver\logger;

use \yii;
use \yii\base\BootstrapInterface;
use \yii\base\Application;
use suver\logger\models\Loggers;

class Logger extends \yii\base\Widget
{
    public function bootstrap($app)
    {
        $app->on(Application::EVENT_BEFORE_REQUEST, function () {
            // do something here
        });
    }

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function run()
    {
        return "Hello!";
    }

    public function test()
    {
        return true;
    }

    public function set($action, $dump, $status=NULL, $user=false)
    {
        if(!$user AND !Yii::$app->user->isGuest)
        {
            $user_id = Yii::$app->user->id;
        }
        else if(!$user)
        {
            $user_id = false;
        }

        $Loggers = new Loggers;
        $Loggers->user_id = $user_id;
        $Loggers->action = $action;
        $Loggers->status = $status;
        $Loggers->data = serialize($dump);
        $Loggers->date_created = date("Y-m-d H:i:s");
        return $Loggers->save();
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['suver/logger'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'ru-RU',
            'basePath' => '@suver/logger/messages',
        ];
    }
}
