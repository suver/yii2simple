<?php

use yii\db\Schema;

class m140916_191445_loggers extends \yii\db\Migration
{
    public function up()
    {
        // Настройки MySql таблицы
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        // Таблица для смены e-mail адресов пользователя
        $this->createTable('{{%suver_loggers}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NULL DEFAULT NULL',
            'action' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'status' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'data' => Schema::TYPE_TEXT . ' NOT NULL',
            'date_created' => Schema::TYPE_DATETIME . ' NOT NULL',
        ], $tableOptions);

        // Индексы
        $this->createIndex('user_id', '{{%suver_loggers}}', 'user_id');
        $this->createIndex('date_created', '{{%suver_loggers}}', 'date_created');
        $this->createIndex('action', '{{%suver_loggers}}', 'action');

    }

    public function down()
    {

        $this->dropTable('{{%suver_loggers}}');
    }
}
