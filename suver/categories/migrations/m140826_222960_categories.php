<?php

use yii\db\Schema;

class m140826_222960_categories extends \yii\db\Migration
{
    public function safeUp()
    {
        // Настройки MySql таблицы
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        // Таблица комментариев
        $this->createTable('{{%suver_categories}}', array(
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_STRING.' NULL DEFAULT NULL',
            'item_id' => Schema::TYPE_INTEGER.' NULL DEFAULT NULL',
            'title' => Schema::TYPE_STRING.' NULL DEFAULT NULL',
            'alias' => Schema::TYPE_STRING.' NULL DEFAULT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'image' => Schema::TYPE_STRING . '(100) NULL DEFAULT NULL',
            'status' => 'TINYINT(2) NOT NULL DEFAULT 1',
            'create_time' => Schema::TYPE_DATETIME . ' NOT NULL',
            'update_time' => Schema::TYPE_DATETIME . ' NOT NULL',
        ), $tableOptions);

        // Индексы
        $this->createIndex('item_id', '{{%suver_categories}}', 'item_id');
        $this->createIndex('title', '{{%suver_categories}}', 'title');
        $this->createIndex('alias', '{{%suver_categories}}', 'alias');
        $this->createIndex('status', '{{%suver_categories}}', 'status');
        $this->createIndex('create_time', '{{%suver_categories}}', 'create_time');
        $this->createIndex('update_time', '{{%suver_categories}}', 'update_time');

        // Связи
        //$this->addForeignKey('FK_activity_user', '{{%categories}}', 'user_id', '{{%users}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%suver_categories}}');
    }
}
