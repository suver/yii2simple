Categories для проектов на Yii2
===========================
Добавляет возможность простого добавления категорий к моделям и формам.


Installation
-------------
1. Добавьте Yii2-categories в ваш composer.json файл:
    <pre>
       {
            "require": {
                "suver/yii2-categories": "dev-master"
            }
       }
    </pre>

2. Запустите
    <pre>
      php composer.phar update
    </pre>

3. Запустите
    <pre>
    php yii migrate/up --migrationPath=suver/categories/migrations
    </pre>


4. Добавьте в файл настроек
    ```php
    'components' => [
        'activity' => [
            'class' => 'suver\categories\Category',
        ],
    ],
    ```



Usage
-----

Для занесения информации в БД достаточно вызвать

    ```php
        Yii::$app->activity->set('ТИП ДЕЙСТВИЯ', 'ОПИСАНИЕ');  // тип действия, короткий вызов. Если пользователь авторизован, то действие будет записано авторизованному пользователю

        // или

        Yii::$app->activity->set($action, 'ОПИСАНИЕ {variable}', ['variable'=>11], ID_ТЕКУЩЕГО_ПОЛЬЗОВТАЛЕЯ, ID_ПОЛЬЗОВАТЕЛЯ НАД КОТОРЫМ ПРОИЗВЕЛИ ДЕЙСТВИЕ); // тип действия, 3 последних праметра не обязательны и любой может быть опущен
    ```