<?php

namespace suver\categories\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $type
 * @property integer $item_id
 * @property string $title
 * @property string $alias
 * @property string $description
 * @property string $image
 * @property integer $status
 * @property string $create_time
 * @property string $update_time
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suver_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'status'], 'integer'],
            [['description', 'create_time', 'update_time'], 'required'],
            [['description'], 'string'],
            [['create_time', 'update_time'], 'safe'],
            [['type', 'title', 'alias'], 'string', 'max' => 255],
            [['image'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('categories', 'ID'),
            'type' => Yii::t('categories', 'Type'),
            'item_id' => Yii::t('categories', 'Item ID'),
            'title' => Yii::t('categories', 'Title'),
            'alias' => Yii::t('categories', 'Alias'),
            'description' => Yii::t('categories', 'Description'),
            'image' => Yii::t('categories', 'Image'),
            'status' => Yii::t('categories', 'Status'),
            'create_time' => Yii::t('categories', 'Create Time'),
            'update_time' => Yii::t('categories', 'Update Time'),
        ];
    }
}
