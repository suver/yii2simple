<?php
namespace suver\helpers;

use yii;

class FileHelper extends \yii\helpers\FileHelper {

    private static $params = [];

    public static function setParams($params){
        self::$params = $params;
    }

    public static function getParams(){
        return self::$params;
    }

    public static function directoryCreator($root, $path){

        self::path($path);

        if(!file_exists( $root . '/' . Yii::getAlias($path)))
        {
            $dirs = explode("/", Yii::getAlias($path));
            $_path = $root;
            foreach($dirs as $dir)
            {
                $_path = $_path . '/' . $dir;
                if(!file_exists($_path)) {
                    @mkdir($_path);
                }
            }
        }
    }

    public static function path($path, $params = []){

        $params = array_merge($params, self::getParams());

        foreach($params as $param=>$value) {
            $path = str_replace("{{$param}}", $value, $path);
        }

        return $path;
    }

}