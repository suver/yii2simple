<?php
/**
 * Created by PhpStorm.
 * User: suver
 * Date: 22.09.14
 * Time: 22:57
 */

namespace suver\helpers;

use yii;


class ArrayHelper extends \yii\helpers\ArrayHelper
{

    public static function toJsObject($value,$safe=false)
    {
        if(is_string($value))
        {
            if(strpos($value,'js:')===0 && $safe===false)
                return substr($value,3);
            else
                return "'".self::quote($value)."'";
        }
        elseif($value===null) {
            return 'null';
        }
        elseif(is_bool($value)) {
            return $value ? 'true' : 'false';
        }
        elseif(is_integer($value)) {
            return "$value";
        }
        elseif(is_float($value)) {
            if($value===-INF)
                return 'Number.NEGATIVE_INFINITY';
            elseif($value===INF)
                return 'Number.POSITIVE_INFINITY';
            else
                return str_replace(',','.',(float)$value);  // locale-independent representation
        }
        elseif($value instanceof CJavaScriptExpression) {
            return $value->__toString();
        }
        elseif(is_object($value)) {
            return self::toJsObject(get_object_vars($value), $safe);
        }
        elseif(is_array($value)) {
            $es=array();
            if(($n=count($value))>0 && array_keys($value)!==range(0,$n-1))
            {
                foreach($value as $k=>$v)
                    $es[]="'".self::quote($k)."':".self::toJsObject($v,$safe);
                return '{'.implode(',',$es).'}';
            }
            else
            {
                foreach($value as $v)
                    $es[]=self::toJsObject($v,$safe);
                return '['.implode(',',$es).']';
            }
        }
        else {
            return '';
        }
    }

    public static function quote($js,$forUrl=false)
    {
        if($forUrl)
            return strtr($js,array('%'=>'%25',"\t"=>'\t',"\n"=>'\n',"\r"=>'\r','"'=>'\"','\''=>'\\\'','\\'=>'\\\\','</'=>'<\/'));
        else
            return strtr($js,array("\t"=>'\t',"\n"=>'\n',"\r"=>'\r','"'=>'\"','\''=>'\\\'','\\'=>'\\\\','</'=>'<\/'));
    }

}