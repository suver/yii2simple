<?php
namespace suver\helpers;

use yii;

class Generator  {

    public static function randomString($length=24){
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }


}